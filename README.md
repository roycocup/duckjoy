# README for DUCKJOY website

Requirements
============

Webserver like apache2
PHP 5.3 or greater
Mysql database server


Instructions
============

1. How to create a news post

    A news post is simply a post. The only difference between a normal post and a news post is that the news post has category of "News".
    So, in order to make a news post

    1. Create a new post
    2. Add Title, Content, Image
    3. Add a "More" tag somewhere inside the content so that you delimit the excerpt that is going into the lists (not the detail)
    4. Assign a category of "News" and unchek any other category.
    5. Save, publish or update.


How to create a new Holiday Package
===================================

1. Duckjoy Offers Plugin
	1. Find and activate this plugin.
2. Add a new Holiday Package under the menu item "Compare"
	1. Complete all the fields
	2. Available dates can be deleted but leaving the departure blank

