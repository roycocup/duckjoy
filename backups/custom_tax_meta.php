<?php

public function render_company(){
		global $post; 
		$tax_name = 'company';
		$tax_title = ucwords($tax_name);
		$tax_slug = $this->post_type.'_'.$tax_name; 
		$tax = get_taxonomy($tax_name);

		//The name of the form
		$name = 'tax_input[' . $tax_name . ']';

		//Get all the terms for this taxonomy
		$terms = get_terms($tax_name,array('hide_empty' => 0));
		$postterms = get_the_terms( $post->ID,$tax_name );
		$current = ($postterms ? array_pop($postterms) : false);
		$current = ($current ? $current->term_id : 0);
		$popular = get_terms( $tax_name, array( 'orderby' => 'count', 'order' => 'DESC', 'number' => 10, 'hierarchical' => false ));
		?>
		<!-- Display tabs-->
		<ul id="<?php echo $tax_name; ?>-tabs" class="category-tabs">
			<li class="tabs"><a href="#<?php echo $tax_name; ?>-all" tabindex="3"><?php echo $tax->labels->all_items; ?></a></li>
			<li class="hide-if-no-js"><a href="#<?php echo $tax_name; ?>-pop" tabindex="3"><?php _e( 'Most Used' ); ?></a></li>
		</ul>
		<!-- Display taxonomy terms -->
		<div id="<?php echo $tax_name; ?>-all" class="tabs-panel">
			<ul id="<?php echo $tax_name; ?>checklist" class="list:<?php echo $tax_name?> categorychecklist form-no-clear">
				<?php   foreach($terms as $term){
					$id = $tax_name.'-'.$term->term_id;
					echo "<li id='$id'><label class='selectit'>";
					echo "<input type='radio' id='in-$id' name='{$name}'".checked($current,$term->term_id,false)."value='$term->term_id' /> $term->name<br />";
					echo "</label></li>";
				}?>
			</ul>
		</div>
		<!-- Display popular taxonomy terms -->
		<div id="<?php echo $taxonomy; ?>-pop" class="tabs-panel" style="display: none;">
			<ul id="<?php echo $taxonomy; ?>checklist-pop" class="categorychecklist form-no-clear" >
				<?php   foreach($popular as $term){
					$id = 'popular-'.$taxonomy.'-'.$term->term_id;
					echo "<li id='$id'><label class='selectit'>";
					echo "<input type='radio' id='in-$id'".checked($current,$term->term_id,false)."value='$term->term_id' />$term->name<br />";
					echo "</label></li>";
				}?>
			</ul>
		</div>

		<?php
	}

