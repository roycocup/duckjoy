<?php

public function render_company_name(){
		global $post, $wpdb; 
		$meta_name = 'company_id';
		$meta_title = 'Company Name';
		$meta_slug = $this->post_type.'_'.$meta_name; 
		// Add an nonce field so we can check for it later.
		//wp_nonce_field($this->post_type.'_'.$meta_name, $meta_name.'_nonce');
		// Use get_post_meta to retrieve an existing value from the database.
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		$companies = $wpdb->get_results('select * from wp_duck_company'); 
		?>
		<select name="<?php echo $meta_name; ?>" >
			<?php
				foreach($companies as $company){
					$selected = ($company->company_id == $saved_value)? true : false;
					if ($selected){
						echo '<option value="'.$company->company_id.'" selected>'.$company->name.'</option>';	
					} else {
						echo '<option value="'.$company->company_id.'">'.$company->name.'</option>';
					}
					
				}
			?>
		</select>

		<?php
	}

?>