# Notes


### To change the post_type

- The post single template filename has to start with single-"post_type"; 

- Must change all the post types in the database
`select * from wp_posts t1 where t1.post_type like '%compare%';
update wp_posts set post_type = 'compares' where post_type = 'compare';`

- Make sure all taxonomies and metaboxes are in sync with the new post type
- The paginagion should work fine as long as the Page permalink and this post_type don't collide
- To register the new post type dont use any rewrite or slug