INFO
====

- These discounts are only available from 1st November 2011 to 30th April 2012 to adults travelling in a group of ten or more. 
- These discounts are not available on web prices - please call the groups team on 0871 231 4824 for the latest pricing. 
- All members of the group must travel on the same date, for the same duration, and to the same accommodation. For big groups, it’s possible for people to travel from different airports. 
- Group discounts can’t be used alongside any other offers, apart from those already on the system. They’re also subject to availability, can be changed at any time, and are subject to normal booking conditions. 
- If someone cancels, normal cancellation charges will apply and this could result in the loss or reduction of discounts.
- http://www.thomson.co.uk/editorial/groups/great-group-travel-deals.html