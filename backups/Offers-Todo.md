TODO
====

### Offers Frontend
1. Detail	
	1. Need to have the info boxes moved to its own table. Can't be metainfo anymore.
	2. Where to submit the form to... orders
	3. Tabs need revisiting - Be done like original
2. List
	1. http://sabramedia.com/blog/how-to-add-custom-fields-to-custom-taxonomies
	2. Filters - A bunch of things here
	3. Make transports a taxonomy
	4. Are taxonomies searchable
	5. http://codex.wordpress.org/Translating_WordPress
	6. Make transports a taxonomy
3. Info
4. Search
	1. http://wordpress.org/support/topic/customising-search-query
	2. http://wordpress.stackexchange.com/questions/7651/custom-search-by-post-data-and-post-metadata

#Offers Main


##Backend TODO

- name:"name of the offer"- meta
- offer_code:"offer code"- Meta
- company:"company that provides this - possibly logo and info in the future"- Tax
- *** transport:"str with bus, plane, train, boat"- change to taxonomy ***
- length:"1-30"- meta
- start:"datetime"- meta
- end:"datetime"- meta
- stars:"1-3"- meta
- itinerary:"str with cities in order and comma separated - cron job will read them and populate a table later"- meta?
- ***info:"fare description, day-by-day, visa, precautions - all in 4 blocks of text"- meta*** Problems here because of chinese copy/paste - own tables.
- price_per_single:"for a single room - can be null"- meta
- price_per_double:"for a double room - can be null" - meta
- price_per_single_student:"for a triple room - can be null" - meta
- price_per_double_student:"for a quad room - can be null" - meta
- price_per_children:"price for on child" - meta
- taxi_fare:"£25 to be included in the order booking" - meta
- status:"active, inactive, archived" - meta
- meeting points: "Several options for text"
- availability:"availability table - needs to write to another table - with departure date, return date, status, prices." - availability
- *** Make Pickup venues and time metabox - like availability ***
- **reviews:"on the orders table for review from the customer"- orders**
- **members_bought:"orders table"- orders**
- **rating:"on the orders table" - orders**
- **make nonces work for submission***


##Notes
- Will not validate the offer backend form so we can draft it, but need to show message saying that 
fields are missing and not show the offer because of that
- reinstate offer status. Or prevent it from publishing and just drafting - http://wordpress.org/support/topic/how-to-display-the-settings-updated-message?replies=4
- List is provided via shortcode (not template) - Problem for filtering? 



##Frontend Pages

###Offer search
- No autocomplete for now - Needs to be with dropdowns
- Search has no problem finding cities
- Search has regex problems on countries

###Offer list
- This needs styling

###Offer detail
- Styling
- Rooms & Price form

###Offer info
- http://stackoverflow.com/questions/8766776/trying-to-get-multiple-fields-validated-in-wordpress-html-form-using-javascript






