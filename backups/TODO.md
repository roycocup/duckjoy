### Order
- Validation of passenger info
- Registration
- Login
- general options
	- Payment off
	- Email for paypal
	- Pages in news
	- View Orders
- Orders backup
	- Save an orders
	- Put it through paypal
- Filters

### Site
- Homepage
- Flash messages (http://baylorrae.com/add-flash-messages-to-your-site/)
- Translate cookies
- Translations http://weblogtoolscollection.com/archives/2007/08/27/localizing-a-wordpress-plugin-using-poedit/
	- http://stackoverflow.com/questions/16976996/where-i-must-put-po-mo-wordpress-languages-file
- Visa page
- T&C Page not show in mmenu
- validation 
	- http://codex.wordpress.org/Data_Validation
	- http://stackoverflow.com/questions/8766776/trying-to-get-multiple-fields-validated-in-wordpress-html-form-using-javascript
- Mockups for 
	- order confirmation page
	- orders backend
- Paypal Api
- Search for News and Guides -  not working
- About page - Divs floated
- Flights page 
- Compare search style
- Compare search destinations autocomplete
- Compare search logic of 1 + 2 + 3 + 4
- compare search latest and erliest
- Group - All of it
- Orders dashboard - all of it
- Dashboard - all of it
- Log in - All of it
- Registration - All of it
- Emails markup - All of it
- Emails send to translate
- Javascript Pagination 
- About - contact us link


