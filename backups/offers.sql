-- table duckjoy.wp_duck_pickup_locations
CREATE TABLE `wp_duck_pickup_locations` (
  `pickup_code` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `location` varchar(250) COLLATE utf8_bin NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`pickup_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- Create Table: company

CREATE TABLE wp_duck_company
(
	`company_id` INT NOT NULL AUTO_INCREMENT
	,PRIMARY KEY (company_id)
	,`name` VARCHAR(250) NOT NULL 
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



-- Create Table: availability

CREATE TABLE `wp_duck_availability` (
  `availability_code` int(10) unsigned NOT NULL,
  `offer_id` int(11) NOT NULL,
  `departure` datetime NOT NULL,
  `return` datetime NOT NULL,
  `status` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`availability_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- Create Table: info

CREATE TABLE wp_duck_info
(
	`itinerary_id` INT NOT NULL AUTO_INCREMENT
	,PRIMARY KEY (itinerary_id)
	,`fares_description` VARCHAR(250)  NULL 
	,`day_by_day_id` VARCHAR(250)  NULL 
	,`visa` VARCHAR(250)  NULL 
	,`precautions` VARCHAR(250)  NULL 
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- Dumping structure for table duckjoy.wp_duck_itinerary

CREATE TABLE `wp_duck_itinerary` (
  `post_id` int(11) NOT NULL,
  `place_name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `origin` int(1) NOT NULL,
  `destination` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;




-- Create Table: routes

-- CREATE TABLE wp_duck_routes
-- (
-- 	`route_id` INT NOT NULL AUTO_INCREMENT
-- 	,PRIMARY KEY (route_id)
-- 	,`origin` VARCHAR(250)  NULL 
-- 	,`destination` VARCHAR(250)  NULL 
-- )
-- ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



-- Create Table: offers

CREATE TABLE wp_duck_offers
(
	`offer_id` INT NOT NULL AUTO_INCREMENT
	,PRIMARY KEY (offer_id)
	,`length` INT  NULL 
	,`stars` INT  NULL 
	,`transport` VARCHAR(250)  NULL 
	,`route_id` INT  NULL 
	,`price_single` INT  NULL 
	,`price_double` INT  NULL 
	,`price_triple` INT  NULL 
	,`price_quad` INT  NULL 
	,`offer_code` VARCHAR(250)  NULL 
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



-- Create Table: route_cities

-- CREATE TABLE wp_duck_route_cities
-- (
-- 	`route_id` INT NOT NULL 
-- 	,`city_id` INT NOT NULL 
-- 	,`order` INT  NULL 
-- )
-- ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



-- Create Table: countries

CREATE TABLE wp_duck_countries
(
	`country_id` INT NOT NULL AUTO_INCREMENT
	,PRIMARY KEY (country_id)
	,`name` VARCHAR(250)  NULL 
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- Dumping structure for table duckjoy.wp_duck_cities
CREATE TABLE `wp_duck_cities` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `origin` tinyint(1) NOT NULL,
  `destination` tinyint(1) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



-- Create Table: company_offers

CREATE TABLE wp_duck_company_offers
(
	`company_id` INT NOT NULL 
	,`offer_id` INT NOT NULL 
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Create Table: reviews

CREATE TABLE wp_duck_reviews
(
	`review_id` INT NOT NULL AUTO_INCREMENT
	,PRIMARY KEY (review_id)
	,`user_id` INT  NULL 
	,`rating` INT  NULL 
	,`route_id` INT  NULL 
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Create Table: Members

CREATE TABLE wp_duck_members
(
	`member_id` INT NOT NULL AUTO_INCREMENT
	,PRIMARY KEY (member_id)
	,`name` VARCHAR(250)  NULL 
	,`age` INT  NULL 
	,`email` VARCHAR(250)  NULL 
	,`primary` BIT  NULL 
	,`party_id` VARCHAR(250)  NULL 
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;






