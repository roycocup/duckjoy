
<?php
/**
 * Template Name: Hotels
 * 
 */

get_header(); ?>

<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>

<style>
	#page{background-color: white; border: none;}
	#primary{margin:0 auto; padding: 30px;padding-bottom: 135px; background-color: white; width: 100%; margin: 0; padding: 0;}
	#login, #login a, #login a:hover{color: black;}
	#searchbox input{border:2px dashed rgba(210,121,39,1); font-size: 18px;}

	
	#masthead.site-header .navbar {top:-5px; background-color: white; top:10px; height: 38px;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {color: #d57c08; background-color: white; padding-top: 0; padding-bottom: 0;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a:hover {background-color: white;border-bottom:8px solid #d57c08;}
	.current_page_item a {border-bottom:8px solid #d57c08 !important;}
	.nav-menu li.page_item:last-child a{border-bottom-width: 8px; border-bottom-style: solid; border-bottom-color: rgba(62,124,78,1);}
	
	#masthead.site-header .navbar .main-navigation .nav-menu .current_page_item a {color: #d57c08; }
	.last-menu-item{padding-left: 0}
	.visa-menu{padding-left: 0}
	

 #login  a {color: #d57c08 !important;}
 
 
 #searchbox input {
	margin-top: 8px !important;
	float:left;
	border: 2px dashed rgba(210,121,39,1) !important;
	color:#000;
}

body {
display: block;
margin: 8px;
}

#masthead{background-color: rgba(255,255,255,1);
height: 130px;
width: 1064px;
}

#magnifier_glass{margin:5px 0 0 0; float:left;}

.menu-logo{width:300px !important; height:40px !important;margin-top:-5px;}
.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_hotel.jpg) no-repeat center 0 !important;  }
.menu-logo a:hover{border:none !important; display:block;}

#main {
width: 1064px;
height: auto;
background: url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/hotelbg.jpg) repeat-y;
float:left;

}

#primary {

background-color:inherit !important;

}

#left-side {
height: auto;
width: 525px;
margin-top:100px;
margin-left: 45px;
float: left;
overflow: hidden;
background-color: rgb(251,190,40);
}

#side {
width: 385px;
margin-top:310px;
float: left;
margin-left: 90px;
overflow: hidden;
margin-bottom:20px;

}


.address {
text-align: right;
margin-left:0;
font-size: 12px;
margin-top:110px;
color:#fff; font-family: "Times New Roman", Times, serif;
float:right;
line-height:30px;
}

 #page {
padding-top:0 !important;

}

#login{font-size:16px !important; color:#d57c08 !important;}
#masthead.site-header .navbar .main-navigation .nav-menu li {
display: inline;
float: left !important;
}



.iframe{border-style:none;  }



</style>

<!-- style via js that needs to override css -->
<script>
	jQuery(document).ready(function($){
		$('#login, #login a').css('color','#3d8150');
		var imgsrc = '<?php echo get_stylesheet_directory_uri(); ?>/img/hotel_magnifier.jpg';
		$('#magnifier_glass a').html('<img src="'+imgsrc+'" alt="">');
	});
</script>




	<div id="primary" class="site-content clearfix">
		<div id="content" role="main">
        
        
        <div id="left-side">
        	<div class="content-side-wrapper">
			<!-- <script type='text/javascript' src='http://brands.datahc.com/SearchBox/143768'></script> -->
			<iframe class="iframe" src="<?php echo get_stylesheet_directory_uri(); ?>/hotel_widget.html" height="510" width="700" scrolling="no" seamless="seamless"></iframe>
           </div>
       </div>
       
       
       
       
       <div id="side">
	<div class="side-innerwrapper">
		<div id="bus">
			<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/hotelgraphicduck.png" alt="">
		</div>

		<div class="clearfix spacer"></div>

		<div class="clearfix address">
			Duckjoy Limited 2A Compton House, Guildford, GU1 4TX 
			<br>Tel: +44 (0)7410 429595
			<br>@2013-2015 大脚鸭旅游网 DUCKJOY LTD | ALL rights reserved
		</div>
	</div>
</div>
       
       
       
		</div><!-- #content -->
	</div><!-- #primary -->




