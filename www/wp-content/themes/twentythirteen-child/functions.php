<?php

//login
function custom_login_css() {
	echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/login/login.css" />';
}
add_action('login_head', 'custom_login_css');



//lang
add_action( 'after_setup_theme', 'setup' );
function setup() {
	load_theme_textdomain( 'duckjoy', get_stylesheet_directory() . '/lang' );
}


function favicon_link() {
	echo '<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />' . "\n";
}
add_action( 'wp_head', 'favicon_link' );




function modify_admin_bar( $wp_admin_bar ){
	$wp_admin_bar->remove_menu( 'search' );
}
add_action( 'admin_bar_menu', 'modify_admin_bar' );



function format_date($date){
	return date('Y-m-d', strtotime($date));
}

function format_js_date($date){
	return date('F d, Y h:i:s', strtotime($date)); 
}


if ( !function_exists( 'debug_log' ) ) {
	function debug_log( $msg, $status = 'DEBUG', $file = 'debug_log.txt' ) {
		$location = 
		$msg = gmdate( 'Y-m-d H:i:s' ) . ' - ' . $status . ' - ' .print_r( $msg, TRUE ) . "\n";
		file_put_contents( $file , $msg, FILE_APPEND);
	}
}

if ( !function_exists( 'get_stars' ) ) {
	function get_stars($number){
		$str = '';
		for($i = 1; $i <= $number; $i++){
			$str .= '&#9733; ';
		}
		return $str;
	}
}


function setFlashMessage($type, $msg){
	$_SESSION['messages'][$type] = $msg;
}

/**
*
* Returns false if there are NO messages
* and true if there are messages
*
**/
function sessionHasMessages(){
	if (empty($_SESSION['messages'])) {
		return false; 
	}
	else {
		return true;
	}
}


function flashMessagesDisplay(){
	if (!sessionHasMessages()) return false; 
	foreach($_SESSION['messages'] as $type => $message){
		echo "<div class='flashmessage {$type}'>$message</div>";
	}
	echo "destroying the session";
	unset($_SESSION['messages']);	
}



function getPagination($total_pages, $current_page, $show_each_side = 3){

	$prev = '<a href="#" class="page-prev"><</a>';
	$next = '<a href="#" class="page-next">></a>';
	$left = '';
	$right = '';
	$cur = "<a href='#'><span class='page-cur' style='text-decoration:none; color:black;'>".$current_page."</span></a>";

	
	for ($i = 1; $i <= $current_page-1; $i++){
		$left .= "<a href='#'><span class='page-number' style='text-decoration:none; color:black;'>".$i.'</span></a>';
	}

	for ($i = $current_page+1; $i <= ($current_page+$show_each_side) && $i <= $total_pages; $i++){
		$right .= "<a href='#'><span class='page-number' style='text-decoration:none; color:black;'>".$i.'</span></a>';
	}


	return array('left'=>$left, 'cur'=>$cur, 'right'=>$right, 'prev'=>$prev, 'next'=>$next);
}



// Disable Admin Bar for everyone but administrators
if (!function_exists('df_disable_admin_bar')) {

	function df_disable_admin_bar() {
		
		if (!current_user_can('manage_options')) {
			
			// for the admin page
			remove_action('admin_footer', 'wp_admin_bar_render', 1000);
			// for the front-end
			remove_action('wp_footer', 'wp_admin_bar_render', 1000);
			
			// css override for the admin page
			function remove_admin_bar_style_backend() { 
				echo '<style>body.admin-bar #wpcontent, body.admin-bar #adminmenu { padding-top: 0px !important; }</style>';
			}	  
			add_filter('admin_head','remove_admin_bar_style_backend');
			
			// css override for the frontend
			function remove_admin_bar_style_frontend() {
				echo '<style type="text/css" media="screen">
				html { margin-top: 0px !important; }
				* html body { margin-top: 0px !important; }
			</style>';
		}
		add_filter('wp_head','remove_admin_bar_style_frontend', 99);
		
	}
}
}
add_action('init','df_disable_admin_bar');


function duck_truncate($string, $chars = 50, $terminator = ' …') {
    $cutPos = $chars - mb_strlen($terminator);
    $boundaryPos = mb_strrpos(mb_substr($string, 0, mb_strpos($string, ' ', $cutPos)), ' ');
    return mb_substr($string, 0, $boundaryPos === false ? $cutPos : $boundaryPos) . $terminator;
}


