<?php
/**
 * The Template for displaying all DETAILS for offers.
 * NOT PAGES
 *
 */
?>
<?php get_header(); ?>
<?php

if (!empty($_POST)){

	if ($_POST['next_page'] == 'compare_passenger_info'){
		//proceed with the order info about passengers
		include (get_stylesheet_directory().'/offer_passenger_info.php');	
		exit();
	}

	if (($_POST['next_page'] == 'compare_detail')){
		include (get_stylesheet_directory()."/offer_detail.php");	
		exit();
	}

}


if (empty($_POST['next_page']) ){
	//detail page
	include (get_stylesheet_directory()."/offer_detail.php");
	exit();
} else {
	//not found
	include( get_query_template( '404' ) );
	exit();	
}
?>

<?php get_footer(); ?>





