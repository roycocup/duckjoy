<?php
/**
 * Template Name: Home
 * 
 */

get_header(); ?>
<!--  big slider -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.slides.min.js"></script>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/slider.css" rel="stylesheet" type="text/css" /> 
<!-- recomendations small js on left -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/xiaobiantuijian.js"></script>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/xiaobiantuijian.css" rel="stylesheet" type="text/css" />

<script>
	// logo alteration
	jQuery(document).ready(function($){
		$('.current-menu-item a')
		.html('<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/duckjoy_home_logo.png" style="max-width:210px;">')
		.css({'padding':'20px 0 10px 0 '});
	});
</script>
<style>
	.site-content{padding:0 0 40px 0; }
	#content{overflow: auto; height: 2000px;}
		.bigslider-wrapper{height: 500px; width: 944px; display: block; margin: 0 auto; padding: 30px 0;margin-bottom: 50px; }
			#bigslider {height: 500px;width: 944px;position: absolute;border-bottom-width: medium;border-bottom-style: none;border-bottom-color: rgba(204,204,204,1);}
				.slidesjs-pagination li a { background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/pagination.png');}
		.content-beyond-slider{background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/home-bg.png'); background-repeat: repeat-y; overflow: auto;}
		
	#left-side{height: 760px;width: 630px;position: absolute;margin-top: 680px;margin-left: 40px;border-right-width: medium;border-right-style: solid;border-right-color: rgba(204,204,204,1);border: 1px solid red;}
		.recomendations-wrapper{float: left; margin-bottom: 30px; margin-left: 40px; }
			#sliderFrame {width:600px; height: 400px; background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/xiaobiantuijianbg.png'); background-repeat: no-repeat;}
			#slider {background: black url(loading.gif) no-repeat 50% 50%;}
			.loading {background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/loading.gif');}
		.news-pull{position: absolute; width: 590px; margin-top: 465px;padding-left: 40px;}
			.news-title{position: absolute; height: 40px; width: 600px; background-color: #e5e5e5;color: #f75959;text-align: left;vertical-align: middle;}
				a:visited, a:hover, a:link{ text-decoration: none; color: inherit}
				.news-title a .h3{padding:0;margin: 0; padding-top: 2px; padding-left: 10px; font-size: 24px;}
				.news-title a .sub-news-title{position: relative; left: 450px;}
			.news-articles {position: relative; top: 23px; left: -40px;}
				.news-articles .news ul li{padding-left:10px; padding-top: 5px;list-style-type: none; display: block; min-height: 28px;width: 325px;font-size: 16px;border-bottom-width: thin;border-bottom-style: solid;border-bottom-color: rgb(204,204,204);}
				.news-articles .pic{position:relative; left:473px; top:-160px; height: 150px; width: 170px; background-repeat: no-repeat; background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/tree3.jpg');}

	img{padding: 0; margin: 0;}
	#right-side{ float: right; width: 370px; border: 1px solid green;}
		#weibo{float:right; height: 350px; width: 275px; border-width: 4px; border-color: #f75959; border-style: dotted; margin-right: 40px;}
		.toplinks{float: right; padding-right:10px; width: 275px; margin-right: 40px;}
			.toplinks .header {font-size: 24px;color: #f75959;text-align: left;margin:20px 0; width: 275px; height: 40px; background-color: #e5e5e5;color: #f75959;text-align: left;vertical-align: middle;}
			.toplinks table{width: 275px; height:310px;}
			.toplinks td{background-color: #ccc;}
			#top_picture_1, #top_picture_2, #top_picture_3, #top_picture_4 {max-width: 137px; max-height: 102px; } 
			#top_picture_5 {max-width: 137px; max-height: 214px;}
			
	<!--Header Css-->			
	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo {width:228px !important; height:70px !important; background:#fff !important; margin:0 20px 0 20px !important; padding:0 !important;}
	.menu-logo a{width:228px !important; height:70px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo('stylesheet_directory'); ?>/img/duckjoylogo.jpg) no-repeat center center !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left; padding-top:22px;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 18px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
	#page {max-width:1064px !important;width: 100%;background-color: #f75959;padding-top: 20px;margin: 0 auto;border-left: 20px solid #f75959;border-right: 20px solid #f75959;}

</style>

<div id="primary" class="site-content">
	<div id="content" role="main">
		<?php while ( have_posts() ) : the_post(); ?>


			<div class="bigslider-wrapper">
				<div id="bigslider">
					<div id="slides"> 
						<a href="www.bbc.co.uk"><img src="<?php the_field('slider_image_1'); ?>" alt=""></a>
						<a href="www.bbc.co.uk"><img src="<?php the_field('slider_image_2'); ?>" alt=""></a>
						<a href="www.bbc.co.uk"><img src="<?php the_field('slider_image_3'); ?>" alt=""></a>
						<a href="www.bbc.co.uk"><img src="<?php the_field('slider_image_4'); ?>" alt=""></a>
					</div>
				</div>	
			</div>
			
			<div class="content-beyond-slider">
				
				<div class="left-side">
						
					<div class="recomendations-wrapper">
						<div id="recommendationtitle"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/recommendation_bg3.png"></div><!--end of recommtitle-->
						<div id="recommendationslider">
							<div id="sliderFrame">
								<div id="ribbon"></div>
								<div id="slider">
									<a href="<?php the_field('image_1_link'); ?>">
										<img src="<?php the_field('image_1'); ?>" alt="#htmlcaption1" />
									</a>
									<a href="<?php the_field('image_2_link'); ?>">
										<img src="<?php the_field('image_2'); ?>" alt="#htmlcaption2" />
									</a>
									<a href="<?php the_field('image_3_link'); ?>">
										<img src="<?php the_field('image_3'); ?>" alt="#htmlcaption3" />
									</a>
									<a href="<?php the_field('image_4_link'); ?>">
										<img src="<?php the_field('image_4'); ?>" alt="#htmlcaption4" />
									</a>
								</div>
								
								<style>
									.top{ height: 240px; overflow: hidden;}
								</style>

								
								<div id="htmlcaption1" style="display: none;font-family: Comic Sans MS, cursive;">
									<div class="top">
										<span class="xiaobiantitle"><?php the_field('rec_title_image_1'); ?></span> <br /><br /><?php the_field('rec_text_image_1'); ?>	
									</div>
									<div class="bottom">
										<a href="<?php the_field('rec_image_1_link'); ?>">继续阅读</a>.	
									</div>
								</div>
								<div id="htmlcaption2" style="display: none;">
									<div class="top">
										<span class="xiaobiantitle"><?php the_field('rec_title_image_2'); ?></span> <br /><br /><?php the_field('rec_text_image_2'); ?>
									</div>
									<div class="bottom">
										<a href="<?php the_field('rec_image_2_link'); ?>">继续阅读</a>.	
									</div>
								</div>
								<div id="htmlcaption3" style="display: none;">
									<div class="top">
										<span class="xiaobiantitle"><?php the_field('rec_title_image_3'); ?></span> <br /><br /><?php the_field('rec_text_image_3'); ?>
									</div>
									<div class="bottom">
										<a href="<?php the_field('rec_image_3_link'); ?>">继续阅读</a>.
									</div>
								</div>
								<div id="htmlcaption4" style="display: none;">
									<div class="top">
										<span class="xiaobiantitle"><?php the_field('rec_title_image_4'); ?></span> <br /><br /><?php the_field('rec_text_image_4'); ?>
									</div>
									<div class="bottom">
										<a href="<?php the_field('rec_image_4_link'); ?>">继续阅读</a>.
									</div>
								</div>

								
								
								<!--thumbnails-->
								<div id="thumbs">
									<div class="thumb"><img src="<?php the_field('image_1'); ?>" /></div>
									<div class="thumb"><img src="<?php the_field('image_2'); ?>" /></div>
									<div class="thumb"><img src="<?php the_field('image_3'); ?>" /></div>
									<div class="thumb"><img src="<?php the_field('image_4'); ?>" /></div>
								</div>
							</div>
						</div><!--end of recommslider-->
					</div> <!-- recomendations-wrapper -->
					

					<?php 
						$args = array(
							'numberposts' => 7,
							'offset' => 0,
							'category' => get_cat_ID('News'),
							'orderby' => 'post_date',
							'order' => 'DESC',
							'include' => '',
							'exclude' => '',
							'meta_key' => '',
							'meta_value' => '',
							'post_type' => 'post',
							'post_status' => 'publish',
							'suppress_filters' => true 
							);
						$recent_news_posts = wp_get_recent_posts( $args, OBJECT );
					?>

					<div class="news-pull">
						<div class="news-title">
							<a href="/news"><span class='h3'>旅游快讯</span></a>
							<a href="/news"><span class="sub-news-title">更多</span></a>
							</div>
						<div class="news-articles">
							<div class="news">
								<ul>
								<?php foreach ($recent_news_posts as $key => $news) :?>
									<li><a href="<?php echo get_permalink($news->ID); ?>"><?php echo $news->post_title; ?></a></li>
								<?php endforeach;?>
								</ul>
							</div>
							<div class="pic"></div>
						</div>
					</div>

				</div> <!-- left-side -->


				<div class="right-side">
					
					<div id="weibo">
						<iframe width="260" height="340" class="share_self"  frameborder="0" scrolling="no" 
						src="http://widget.weibo.com/weiboshow/index.php?language=&width=260&height=340&fansRow=1&ptype=1&speed=100&skin=7&isTitle=0&noborder=0&isWeibo=1&isFans=0&uid=3202270263&verifier=eb7f11be&dpc=1"></iframe>
					</div>

					<div class="toplinks">
						<div class="header">&nbsp;团购</div>
						<?php $id = get_the_ID();?>
						   
						<div id="pics_container">
								<div class="pic1"><a href="<?php the_field('top_link_1', $id); ?>"><img id="top_picture_1" src="<?php the_field('top_picture_1', $id); ?>" alt=""></a></div>
								<div class="pic2"><a href="<?php the_field('top_link_2', $id); ?>"><img id="top_picture_5" src="<?php the_field('top_picture_2', $id); ?>" alt=""></a></div>
								<div class="pic3"><a href="<?php the_field('top_link_5', $id); ?>"><img id="top_picture_2" src="<?php the_field('top_picture_5', $id); ?>" alt=""></a></div>
								<div class="pic4"><a href="<?php the_field('top_link_3', $id); ?>"><img id="top_picture_3" src="<?php the_field('top_picture_3', $id); ?>" alt=""></a></div>
								 <div class="pic5"><a href="<?php the_field('top_link_4', $id); ?>"><img id="top_picture_4" src="<?php the_field('top_picture_4', $id); ?>" alt=""></a></div>
						</div>
					</div>

				</div> <!-- right-side -->

			</div> <!-- content-beyond-slider -->

			<style>
			.middleline{
				width: 944px;
				position: absolute;
				margin-top: 40px;
				background-color: white;
				background-image: -webkit-repeating-linear-gradient(45deg, transparent, transparent 5px, rgba(204,204,204,.5) 5px, rgba(204,204,204,.5) 7px);
				background-image: -moz-repeating-linear-gradient(45deg, transparent, transparent 5px, rgba(204,204,204,.5) 5px, rgba(204,204,204,.5) 7px);
				background-image: -ms-repeating-linear-gradient(45deg, transparent, transparent 5px, rgba(204,204,204,.5) 5px, rgba(204,204,204,.5) 7px);
				background-image: -o-repeating-linear-gradient(45deg, transparent, transparent 5px, rgba(204,204,204,.5) 5px, rgba(204,204,204,.5) 7px);
				background-image: repeating-linear-gradient(45deg, transparent, transparent 5px, rgba(204,204,204,.5) 5px, rgba(204,204,204,.5) 7px);
				height: 15px;
				margin-left: 40px;
				color: rgba(153,153,153,1);
				text-align: center;
				vertical-align: middle;
				letter-spacing: 15px;
				padding-bottom: 5px;
				font-size: 12px;
			}
			.middlebanner{
				height: 91px;
				width: 944px;
				position: absolute;
				margin-top: 65px;
				margin-left: 40px;
				background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/middlebanner.jpg');
			}
			.whitebackground{ height: 180px; width: 944px; background-color: white;}
			</style>
			<div class="whitebackground">
				<div class="middleline"></div>
				<div class="middlebanner"></div>
			</div>

			<div class="content-beyond-slider content-beyond-slider2">

				<div class="left-side-bottom">
					<style>
						.guides-pull{position: absolute; width: 590px; padding-left: 40px;}
							.guides-title{position: absolute; height: 40px; width: 600px; background-color: #e5e5e5;color: #f75959;text-align: left;vertical-align: middle;}
								a:visited, a:hover, a:link{ text-decoration: none; color: inherit}
								.guides-title a .h3{padding:0;margin: 0; padding-top: 2px; padding-left: 10px; font-size: 24px;}
								.guides-title a .sub-guides-title{position: relative; left: 380px;}
							.guides-articles {position: relative; top: 23px; left: -40px;}
								.guides-articles .guides ul li{padding-left:10px; padding-top: 5px;list-style-type: none; display: block; min-height: 28px;width: 300px;font-size: 16px;border-bottom-width: thin;border-bottom-style: solid;border-bottom-color: rgb(204,204,204);}

						.bottomlinks{float: right; margin-right: 40px; border-width: 2px;border-color: #CCCCCC;border-style: dashed; padding:10px; width: 300px;}
							.bottomlinks .header {font-size: 24px;color: rgb(98,98,98);text-align: left;margin: 0 0 0 0;width: 275px;height: 40px;background-color:inherit;color: #f75959;text-align: right;vertical-align: middle;}
							.bottomlinks table{width: 275px; height:310px;}
							.bottomlinks td{background-color: #ccc;}
							#top_picture_1, #top_picture_2, #top_picture_3, #top_picture_4 {max-width: 137px; max-height: 102px; } 
							#top_picture_5 {max-width: 137px; max-height: 214px;}
						

						.guides_container li{padding:0 0 15px 0 !important; margin:0 0 15px 0 !important; float:left !important; width:598px !important;}
						.guides_container li + li{border-bottom:solid 1px #ccc;}
						#guides-pic1{height: 180px;width: 230px;background-color: #009966; float:left;}
						#guides-pic1 img{width:100%; height:100%; float:left;}
						#guides-text1{width: 345px; float:left; margin:0 0 0 20px;}
						#guides-text1 h4{margin:0 0 15px 0; color:#f75959;}
						.guides_container{margin:30px 0 20px 0 !important; float:left !important; clear:both !important; color:#333; }
						.guide_post_content1{font-size:12px;}

						.post_meta_info1{padding:0 0 15px 0;}
						.post_meta_info1 span { color:#333;}

						.guides-articles .guides ul li {list-style-type: none;font-size: 12px !important;}
						.guides-articles .guides ul li:last-child{ border-bottom:0;}
									
					</style>

					<?php 
							$args = array(
								'numberposts' => 2,
								'offset' => 0,
								'category' => get_cat_ID('Guides'),
								'orderby' => 'post_date',
								'order' => 'DESC',
								'post_type' => 'post',
								'post_status' => 'publish',
								'suppress_filters' => true 
								);
							$recent_guides_posts = wp_get_recent_posts( $args, OBJECT );
						?>

						<div class="guides-pull">
							<div class="guides-title">
								<a href="/guides"><span class='h3'>新脚度 （游记）</span></a>
								<a href="/guides"><span class="sub-guides-title">更多</span></a>
							</div>
							<div class="guides-articles">
								<div class="guides">
									<ul class="guides_container">
										<?php foreach ($recent_guides_posts as $key => $guide) :?>
											<?php $views = get_post_custom_values('views', $guide->ID); ?>
											<li>
												<div id="guides-pic1">
													<a href="<?php echo get_permalink($guide->ID); ?>"><img src="<?php echo the_field('primary_image', $guide->ID); ?>" /></a>
												</div>
												<div id="guides-text1">
													<h4><a href="<?php echo get_permalink($guide->ID); ?>"><?php echo $guide->post_title ?></a></h4>
													<div class="post_meta_info1">
														<span class="meta_info_1">作者：<?php echo the_field('author'); ?></span>
														<span class="meta_info_2"><?php echo " 发布时间：". format_date($guide->post_date); ?></span>
														<span class="meta_info_3"><?php if(function_exists('the_views')) { echo " 阅读："; echo "{$views[0]} 次";} ?></span>
													</div>

													<div class="guide_post_content1">
														<?php echo mb_strimwidth($guide->post_excerpt, 0, 200); ?>
														<a href="<?php echo get_permalink($guide->ID); ?>"><?php _e("...Continues", 'duckjoy');?></a>
													</div>
												</div>
											</li>

										<?php endforeach;?>
									</ul>
								</div>
							</div>
						</div>
					</div>

				<div class="right-side-bottom">

					<div class="bottomlinks">
						<div class="header" style="color:rgb(98,98,98);">&nbsp;当季好去处</div>
						<?php $id = get_the_ID();?>
							<div id="pics_container1">
								 <div class="pic1"><a href="<?php the_field('bottom_link_1', $id); ?>"><img id="top_picture_1" src="<?php echo the_field('bottom_picture_1', $id) ?>" alt=""></a></div>
								 <div class="pic2"><a href="<?php the_field('bottom_link_2', $id); ?>"><img id="top_picture_5" src="<?php echo the_field('bottom_picture_4', $id) ?>" alt=""></a></div>
								 <div class="pic3"><a href="<?php the_field('bottom_link_3', $id); ?>"><img id="top_picture_2" src="<?php echo the_field('bottom_picture_2', $id) ?>" alt=""></a></div>
								 <div class="pic4"><a href="<?php the_field('bottom_link_4', $id); ?>"><img id="top_picture_3" src="<?php echo the_field('bottom_picture_3', $id) ?>" alt=""></a></div>								
							</div>
						
					</div>

				</div> <!-- right-side-bottom -->

			</div> <!-- content-beyond-slider -->

		<div class="middleline" ></div>

		<?php endwhile; // end of the loop. ?>
		<?php wp_reset_postdata(); ?>
	</div><!-- #content -->
</div><!-- #primary -->

<!-- SlidesJS Required: Initialize SlidesJS with a jQuery doc ready -->
<script>
	jQuery(function($) {
		$('#slides').slidesjs({
			width: 1200,
			height: 600,
			play: {
				active: true,
				auto: true,
				interval: 8000,
				swap: true
			}
		});
	});
</script>
<!-- End SlidesJS Required -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>