<?php // Template Name: Tickets ?>

<?php get_header('tickets'); ?>

				<div id="topleft">

					<script>
						function fb_login_if_not(){};
						function fb_register(){};
					</script>
					<div id="searchtitle">Duckjoy-机票搜索</div>


					<div id="searchwrap" style="height:800px; margin-top:-100px;">
						<section class="">
							<div id="page" class="type_home">
								<div class="wrap">
									<div class="constraint">
										<div class="head">
											<div class="links">
												<div class="menus">
												</div>
											</div>
										</div>

									<section class="main">
								<div class="inner">
									<div id="multiform" class="flights">

										

										
											
												
													<div id="flights" class="content">
														
	


<form id="flightform" class="form flights" method="get" action="http://tickets.duckjoy.com" onSubmit="return dohop.validate();">

	

	<input type="hidden" name="d1" id="d1" value="290114"/>
	<input type="hidden" name="d2" id="d2" value=""/>

	<div class="flightform" style="height:355px;">
			<div class="cell from" style="margin-top:-20px;">
				<div class="padding">
					<div class="form txt clearable complete">
						<label for="departPlace">自
						
						</label>
						<input type="text" placeholder="输入机场或城市名的第一个字母" class="text" name="a1" id="departPlace" value="" >
						<div class="clearer" onclick="dohop.locations.setOrigin(''); $('#departPlace').focus()"></div>
						
					</div>
					<div class="arrow"></div>
				</div>
			</div>
			<div class="cell to">
				<div class="padding">
					<div class="form txt clearable complete" style="margin-top:10px;">
						<label for="arrivePlace">至</label>
						<input type="text" placeholder="输入机场或城市名的第一个字母" class="text" name="a2" id="arrivePlace" value="">
						<div class="clearer" onclick="dohop.locations.setDestination(''); $('#arrivePlace').focus()"></div>
						
					</div>
				</div>
		</div>
        <div class="cell option" style="margin-top:10px;margin-left:0px;">
				<div class="form chk left">
					<input type="radio" name="return" value="1" id="check_returndate">
					<label for="check_returndate">回程</label>
				</div>
				<div class="form chk left">
					<input type="radio" name="return" value="0" id="check_oneway" checked>
					<label for="check_oneway">单程</label>
				</div>
			</div>
            
		<div class="row lower" style="width:600px; margin-top:45px;">
			
			<div class="cell depart" >
				<div class="padding">
					<div class="form txt date">
						<label for="departDate">出发</label>
						<div class="calendar-input-wrap" style="width:210px;z-index:2;">
							<input type="text" placeholder="选择启程日期" id="departDate" readonly value="">
							<div class="calendar-icon"></div>
							<div class="calendar-pointer"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="cell return" style="width:240px; margin-left:70px; " >
				<div class="padding">
					<div class="form txt date">
						<label for="returnDate" style="margin-left:0px;">回程</label>
						<div class="calendar-input-wrap" >
							<input type="text" placeholder="选择回程日期" id="returnDate" readonly value="">
							<div class="calendar-icon"></div>
							<div class="calendar-pointer"></div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="cell sbutton" style="width:435px;margin-top:65px;margin-right:165px;">
				<a class="button orrange big fill" style="background-color: rgb(159,204,242); color:#4d4d4d;" onclick="$('form.flights input[type=submit]').click(); return false;">搜索</a>
			</div>
			
				
					
					<div class="clearfix"></div>
					<label id="compareLabel"><input style="margin-top:55px;"type="checkbox" id="compare_all_hotels_checkbox" unchecked/> 比一比更划算(与其他网站比较)</label>
					
				
			
		</div>
	</div>

	<input type="submit" class="hidden-submit" tabindex="-1">
</form>

		
			
			<div style="display: none;" id="clicktripz_sites"></div>
			<div style="display:none">
				<input type="text" name="ct_travelers" id="ct_travelers" value="1"/>
				<input type="radio" name="ct_oneway" id="ct_oneway" value="1" checked="checked"/>
				<input type="text" name="ct_a1" id="ct_a1" value="LUN"/>
				<input type="text" name="ct_a2" id="ct_a2" value="CDG"/>
				<input type="text" name="ct_d1" id="ct_d1" value="01/29/2014"/>
				<input type="text" name="ct_d2" id="ct_d2" value=""/>
			</div>
			
		

		

	
	<!--
		<div style="display:none">
			<input type="text" name="ct_travelers" id="ct_travelers" value="1"/>
			<input type="radio" name="ct_oneway" id="ct_oneway" value="1" checked="checked"/>
			<input type="text" name="ct_a1" id="ct_a1" value="LUN"/>
			<input type="text" name="ct_a2" id="ct_a2" value="CDG"/>
			<input type="text" name="ct_d1" id="ct_d1" value="01/29/2014"/>
			<input type="text" name="ct_d2" id="ct_d2" value=""/>
		</div>
	-->
	

													</div>
												
											
										
									</div>
									<div class="clear"></div>
								</div>

								
								<div id="dialog">
									<div id="dialog_title"></div>
									<p>
										<span id="dialog_text"></span>
									</p>
								</div>
								

								
			

							</section>
						

										</div>
									</div>
								</div>
						</div><!-- /searchwrap -->
						<div id="usersettingsdrp">
							<div class="options country">
								<div class="form txt drp">
									<label for="settings_country">
										<span class="flag" style="background-image: url(http://static.dohop.com/img/flags24/GB.png);"></span>
										<span class="icon"></span>
										<span class="selection">您的居住地</span>
									</label>
									<div class="select-like">
										<span class="value"></span>
										<select name="" id="settings_country"></select>
									</div>
								</div>
							</div>
							<div class="options currency">
								<div class="form txt drp">
									<label for="settings_currency">
										<span class="icon"></span>
										<span class="selection">货币</span>
									</label>
									<div class="select-like">
										<span class="value"></span>
										<select name="" id="settings_currency"></select>
									</div>
								</div>
							</div>
							<a class="button orrange big fill"><b>Apply</b></a>
						</div>
						<div id="wizard"></div>
						<div class="overlay wizard-overlay" onClick="dohop.user.closewizard()"></div>


						<script type="text/javascript" src='http://static.dohop.com/js/packages/flightform_script_package.js?v=40200'></script>
						<script type="text/javascript" src="http://static.dohop.com/templates/gerlach_form_template_package-zh.js?v=40200"></script>
						<script type="text/javascript">
							(function(){
								if (window.top == window || !window.top.postMessage)
									return;

								var size = 0;
								setInterval(function(){
									var newSize = Math.max.apply(Math, 
										[$(document).height()].concat(
											$('.popup, ul.ui-autocomplete').map(function(){
												return $(this).offset().top + $(this).outerHeight();
											}).get()
											)
										);
									if (newSize != size) {
										window.top.postMessage('dohop_iframe_height:' + newSize, '*');
										size = newSize;
									}
								}, 100);
							})();

						</script>




						<script type="text/javascript" src="http://static.dohop.com/js/gerlach.js?v=40200"></script>

						<script type="text/javascript">
							var _gaq = _gaq || [];
							_gaq.push(['_setAccount','UA-284809-1']);

							_gaq.push(['setDomainName', 'dohop.com']);
							_gaq.push(['setAllowLinker', true]);

							_gaq.push(['_setCustomVar', 1, 'aff_wlname', 'Duckjoy_flights', 2]);

							_gaq.push(['_trackPageview']);


							(function() {
								var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
								ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
								var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
							})();
						</script>


						<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
						<script>
							$(document).ready(function(){
								$('#nav1 ul li a, #nav2 ul li a').hover(function() {
									$(this).addClass('underline');
								}, function() {
									$(this).removeClass('underline');
								});
								$('img[name="magnifier"]').click(function() {
									
								});
							});
						</script>

					</div><!--end of topleft-->


					<!--begin of topright-->

					<div id="topright">



					</div> <!--enf of topright-->
					<div class="clear"></div> 
					<!--begin of footertext-->
					<div id="footertext">

						<div id="righttext">
							Duckjoy Limited 2A Compton House, Guildford, GU1 4TX 
							<br /><br />

							Tel: +44 (0)7410 429595 
							<br /><br />

							@2013-2015 大脚鸭旅游网 DUCKJOY LTD | ALL rights reserved </div><!--end of righttext-->
						</div><!--end of footertext-->

					</div><!--end of main--> 

				</div><!--end of container-->
			</div><!--end of wrapper-->

		</body>
		</html>
