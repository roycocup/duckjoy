<?php
/**
 * Template Name: News
 * 
 */
?>
<?php get_header(); ?>

<style>
	#primary {overflow: auto;}
	#content{ float:left; width:670px; padding: 0 30px; border-right: 3px solid #ccc; margin: 30px 0;}
	
	.advertising{ width:350px; float: right;}
		.advertising ul {padding: 0; margin: 0; text-align: right;}
			.advertising li{ list-style: none; border:1px solid rgba(0,0,0,0);}
				.advertising li img{ max-width: 285px; max-height: 200px; margin-top: 30px; margin-right: 35px;}

	.grey-bar{ background-color: rgba(204,204,204,1); color: white; padding: 0 10px; margin-bottom: 20px; height: 32px; font-size: 20px; }

	.news-table li .post-date{float: right; padding-right: 10px;}
	.news-table li a:hover{ text-decoration: none;color: rgb(154,38,37); }
	.news-table li a:link{ color: rgb(51,51,51);text-decoration: none; }
	.news-table li a:visited { color: rgb(51,51,51);text-decoration: none;}
	
	
	<!--New Css-->
	
	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo{width:300px !important; height:40px !important;}
	.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}


	
	.grey-bar {
background-color: rgba(204,204,204,1);
color: white;
padding: 0 10px;
margin-bottom: 20px;
font-size: 24px;
line-height:37px;
height:auto !important;
}

.news-table ul{padding:0 !important; margin:0 !important;}
.news-table ul li {
padding-top:0 !important;
text-indent: 2em !important;
list-style: none;
font-family: "Verdana,宋体";
font-size: 14px !important;
color: #333333;
text-align: left;
height:30px !important;
background-image: none;
background-repeat: no-repeat;
background-position: 3px 50%;
border-bottom-width: 1px;
border-bottom-style: none;
border-bottom-color: #6AA70B;
-moz-border-radius: 10px 10px / 10px 10px;
border-radius: 10px 10px / 10px 10px;
padding-bottom:0 !important;
line-height:30px;
}



</style>
<script>
	// logo alteration
	jQuery(document).ready(function($){
		$("div .menu-item :contains('duckjoy')")
		.html('<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo_common.jpg" style="max-width:210px;">')
		.css({'padding':'20px 0 10px 0 '});
	});
</script>
<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/news-zixun.css" rel="stylesheet" type="text/css" />

	<div id="primary" class="site-content">

		<div class="advertising">
			<ul>
				<li><a href="http://<?php the_field('link_for_ad_1');?>" target="_blank" ><img src="<?php the_field('ad_1');?>" alt="" class="advert"></a></li>
				<li><a href="http://<?php the_field('link_for_ad_2');?>" target="_blank" ><img src="<?php the_field('ad_2');?>" alt="" class="advert"></a></li>
				<li><a href="http://<?php the_field('link_for_ad_3');?>" target="_blank" ><img src="<?php the_field('ad_3');?>" alt="" class="advert"></a></li>
				<li><a href="http://<?php the_field('link_for_ad_4');?>" target="_blank" ><img src="<?php the_field('ad_4');?>" alt="" class="advert"></a></li>
			</ul>
		</div>


		<?php
		//page content
		$post = get_page(get_the_id()); 
		$title = apply_filters('the_title', $post->post_title); 
		$content = apply_filters('the_content', $post->post_content); 
	
		//posts listing
		$category_name = 'News';
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$posts_per_page = 30;
		$args = array(
			'post_type' => array('post'),
			'posts_per_page' => $posts_per_page,
			'orderby'=>'date',
			'order'=>'DESC',
			'paged' => $paged,
			'cat' => get_cat_ID($category_name),
		);
		$posts = get_posts($args)
		?>

		<div id="content" role="main">
			<div class='grey-bar'>旅游资讯</div>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="news-table" id="tab">
					<ul>
						<?php foreach ($posts as $post) : ?>
							<li>
								<?php $post_date = get_the_date('F j,Y'); ?>
								<a href="<?php the_permalink(); ?>"><?php the_title() ?></a> 
								<span class="post-date"> <?php echo format_date($post_date); ?> </span>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>

			<?php 
				$postsInCat = get_term_by('name',$category_name,'category');
				$total_posts = $postsInCat->count;
				//$total_posts = count(get_posts(array('cat'=>get_cat_ID($category_name), 'posts_per_page'=>300000)));
				$pages_to_show = ceil($total_posts / $posts_per_page);
				$args = array(
					'base'         => '/'.strtolower($category_name).'/%_%',
					'format'       => '/page/%#%',
					'total'        => $pages_to_show,
					'current'      => max( 1, get_query_var('paged') ),
					'show_all'     => False,
					'end_size'     => 2,
					'mid_size'     => 2,
					'prev_next'    => True,
					'prev_text'    => __('« ', 'duckjoy'),
					'next_text'    => __(' »', 'duckjoy'),
					'type'         => 'plain',
					'add_args'     => False,
					'add_fragment' => ''
				);
				echo paginate_links($args); 
			?>

		</div><!-- #content -->
	</div><!-- #primary -->

	<script type="text/javascript">
		var Ptr=document.getElementById("tab").getElementsByTagName("li");
		function $() {
			for (i=1;i<Ptr.length+1;i++) {
				Ptr[i-1].className = (i%2>0)?"t1":"t2";
			}
		}
		window.onload=$;
		for(var i=0;i<Ptr.length;i++) {
			Ptr[i].onmouseover=function(){
				this.tmpClass=this.className;
				this.className = "t3";
			};
			Ptr[i].onmouseout=function(){
				this.className=this.tmpClass;
			};
		}
	</script>


<?php get_footer(); ?>