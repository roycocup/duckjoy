<?php 
/*
* Template Name: Register
*/
?>

<?php get_header(); ?>

<style>
	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo{width:300px !important; height:40px !important;}
	.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}



	#main {width: 100%;background-color: white;float: left; min-height: 400px; font-size: 16px;}

	#right {width:285px;margin-top:30px;float:right; margin-right:40px;}
	#right a, #right a:hover, #right a:link, #right a:visited{ text-decoration: none; color: black;}

	#left {width:630px;margin-top:30px;margin-left:40px;float:left; border-right:solid 3px #cccccc; padding:0 30px 0 0;}

	.column-1{ float: left; text-align: right;}
	.column-2{ float: left; margin-left: 10px;}
	.column-2 input[type='submit'] { background-color: rgb(229,100,86); border-radius: 5px; color:white; box-shadow: none; border:none; height:40px; } 

	.lwa-modal h4, .lwa-modal p{font: inherit;}

	.flashmessages{ float:left; width:945px; margin:40px; padding:20px 40px; background-color: rgb(239,215,217); color: rgb(168,77,62); min-height: 20px;}

</style>


<?php 
	$show = true;
	if (!empty($_POST)){
		if (!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['cpassword']) ) {

			//validation

			//email already exists
			if(email_exists( $_POST['email'] )){
				$_POST['messages'][] = __("Email already exists.", 'duckjoy');	
			}


			//passwords must match
			if ($_POST['password'] !== $_POST['cpassword']){
				$_POST['messages'][] = __("Passwords do not match.", 'duckjoy');	
			}
			//passwords must be bigger than 6 characters
			if (strlen($_POST['password']) < 6){
				$_POST['messages'][] = __("Password should be at least 6 characters long.", 'duckjoy');	
			}	

			//email valid
			if (!is_email($_POST['email']) ){
				$_POST['messages'][] = __("That email is not valid.", 'duckjoy');		
			}

			if (empty($_POST['messages'])){
				//This is a new user and member
				$username = trim($_POST['email']);
				$email = trim($_POST['email']);
				$password = trim($_POST['password']);
				wp_create_user( $username, $password, $email );
				$show = false;
				$_POST['messages'][] =  __("You are now registered.", 'duckjoy');
			}
		} else {
			$_POST['messages'][] =  __("Please fill all fields to register", 'duckjoy');
		}
	}
?>


<div id="primary" class="site-content">
	<div id="content" role="main">
		
		<?php if (!empty($_POST['messages'])) :?>
			<div class="flashmessages">
				<?php foreach ($_POST['messages'] as $message): ?>
					<li><?php echo $message; ?></li>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<?php if ($show): ?>
			<div id="left">
				<form name="loginform" id="loginform" action="" method="post">
					<div class="column-1">
						<p><?php _e("Email", 'duckjoy');?></p>
						<p><?php _e("Password", 'duckjoy');?></p>
						<p><?php _e("Confirm", 'duckjoy');?></p>
					</div>
					<div class="column-2">
						<p><input type="text" name="email" size="40"></p>
						<p><input type="password" name="password" size="40"></p>
						<p><input type="password" name="cpassword" size="40"></p>
						<input type="hidden" name="redirect_to" value="http://duckjoy.machine/log-in/">
						<input type="submit" name="wp-submit" value="<?php _e("Register", 'duckjoy');?>"><br>
					</div>
				</form>
			</div>




			<div id="right">
			<?php if(!is_user_logged_in()) : ?>
				<a href="/log-in"><?php _e("> Login", 'duckjoy');?></a>
			<?php else: ?>
				<a href="<?php echo wp_logout_url() ?>"><?php esc_html_e( 'Logout', 'duckjoy') ?></a>
			<?php endif; ?>
			</div>
		<?php endif; ?>
		



	</div><!-- #content -->
</div><!-- #primary -->



		<?php get_footer(); ?>

