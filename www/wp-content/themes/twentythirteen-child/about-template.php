<?php
/**
 * Template Name: About
 * 
 */

get_header(); ?>

<?php //wp_enqueue_style( 'duck_news', get_bloginfo('stylesheet_directory')."/news.css", array('')); ?>
<!-- <link rel="stylesheet" src="<?php echo get_bloginfo('stylesheet_directory') . "/news.css"; ?>"> -->
<style>
	#main{ float:left; height:auto; background-image:url('<?php echo get_bloginfo('stylesheet_directory'); ?>/img/aboutusbg4.jpg');}
	
	#contactus {
font-size: 24px;
font-family: "Arial Rounded MT Bold";
color: rgb(255,255,255);
margin-top: 49px;
margin-left: 145px;
}

#contactus a {
color: rgb(255,255,255);
text-decoration: none;
}

 #text1 {
font-size: 12px;
color: rgb(51,51,51);
height: 100px;
width: 365px;
margin-top: 657px;
margin-left: 205px;
line-height: 20px;
text-align: justify;
}

 #text2 {
font-size: 12px;
color: rgb(51,51,51);
height: 100px;
width: 360px;
margin-top: 89px;
margin-left: 430px;
line-height: 20px;
text-align: justify;
}

#text3 p {
font-size: 16px;
color: rgb(51,51,51);
height: 400px;
width: 660px;
margin-top: 815px;
margin-left: 183px;
line-height: 34px;
text-align: justify;
}

.bottombluetext{
	font-size:16px;
	color:rgb(33,70,133);}


#login{font-size:16px !important; color:#fff !important;}
#magnifier_glass{float:left;}
.menu-logo{width:300px !important; height:40px !important;}
.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
.menu-logo a:hover{border:none !important; display:block;}
#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
#searchbox input {
height: 22px !important;
width: 145px !important;
float:left;
border: 2px #fff !important;
color:#000 !important;
background:none; margin:0;
}

</style>
	<div id="primary" class="site-content clearfix">
		<div id="content" role="main">
			
            
             <div id="contactus"><a href="/contact-us">Contact Us</a></div>
   <div id="text1">Duckjoy是由spring －一个爱旅游爱策划的留学生创立而成的。Duckjoy的概念起初诞生在一个常见的 “英式”下雨天。Spring Yu （创始人）刚结束了一大批论文的肆虐， 旅行是一件令人向往的事情但出发前的策划却总是让人烦恼不已！这一天刚从论文的狂风暴雨中走出的Spring却又陷入了策划假期旅行－订机票，订酒店，比价格，比好评等等一系列的摧残中。他顿时萌生一念，要是有个集旅游信息和预定于一体的网站，具有一目了然的比较功能该多省心省事！</div>
   <div id="text2">这个可爱的名字源于约克遍地是鸭的奇景。Spring发现这里的鸭子不仅能飞能游能走，可谓是水陆空畅行无阻。而且随处可见鸭群在马路上不紧不慢的穿行，高傲的抬着头看着两旁为他们让路的人群和车流。Spring希望旅行者们也能向约克的鸭子般在旅途中畅行无阻大脚走天下，于是2013年年底Duckjoy 大脚鸭 在约克成立了！</div>
   <div id="text3">
     <p><span class="bottombluetext">Spring Yu (创始人):</span>大脚鸭将成为每一位走出国门的旅行者的领路人，每一位爱旅行的“鸭友”都可以在此找到最适合自己的旅行方式，确保大家的旅途畅行无阻！<br />
<span class="bottombluetext">Ya Ya (小编): </span>脚鸭网将能在出行前为您带来关于目的地的有用信息，同时也希望您能成为我们的一员，在此记录与分享每一步精彩的旅行足迹！ <br />
<span class="bottombluetext">Lei He (网站管理):</span>在鸭友的宝贵意见和建议下，我们将不断的改善更新大脚鸭网的功能及运用，使旅行前繁琐的准备过程成为一种享受！<br />
<span class="bottombluetext">Laracat (网站设计):</span>个性与追求带给鸭友们不一样的视觉享受！</p>
    
   </div>
            
            
		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>