jQuery(document).ready(function() {

	/* Chinese initialisation for the jQuery UI date picker plugin. */
	/* Written by Ressol (ressol@gmail.com). */
	jQuery(function($){
		$.datepicker.regional['zh-TW'] = {
			closeText: '關閉',
			prevText: '&#x3c;上月',
			nextText: '下月&#x3e;',
			currentText: '今天',
			monthNames: ['一月','二月','三月','四月','五月','六月',
			'七月','八月','九月','十月','十一月','十二月'],
			monthNamesShort: ['一月','二月','三月','四月','五月','六月',
			'七月','八月','九月','十月','十一月','十二月'],
			dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
			dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
			dayNamesMin: ['日','一','二','三','四','五','六'],
			weekHeader: '周',
			dateFormat: 'yy-mm-dd',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: true,
			yearSuffix: '年'};
		$.datepicker.setDefaults($.datepicker.regional['zh-TW']);
	});


	//general datepicker for all class datepicker
	jQuery('.datepicker').datepicker({
		dateFormat : 'yy-mm-dd',
	});

	jQuery('.datepickerdob').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-90:+0",
	});

	
	//general date and time picker for class datetimepicker
	jQuery('.datetimepicker').datetimepicker({
		dateFormat : 'yy-mm-dd'
	});

	//general TIME picker for class datetimepicker
	jQuery('.timepicker').timepicker({
		dateFormat : 'yy-mm-dd'
	});
	

	

	//availability add function
	jQuery('#add_availability').click(function(){
		var time_now = new Date().getTime();
		var previous_dates ='<tr><td><input type="text" class="datepicker" name="availability_departure['+time_now+']"></td>';
		previous_dates = previous_dates + '<td><input type="text" class="datepicker" name="availability_return['+time_now+']"></td>';
		previous_dates = previous_dates + '<td><input type="checkbox" name="status['+time_now+']" checked></td></tr>';
		jQuery('#availability_table > tbody').append(previous_dates);
		jQuery('.datepicker').datepicker({dateFormat : 'yy-mm-dd'});
	});


	//pickup location and time add function
	jQuery('#add_location').click(function(){
		var time_now = new Date().getTime();
		var previous_dates ='<tr><td><input type="text" size="50" name="pickup_location['+time_now+']"></td>';
		previous_dates = previous_dates + '<td><input type="text" class="timepicker" name="pickup_time['+time_now+']"></td></tr>';
		jQuery('#pickup_table > tbody').append(previous_dates);
		jQuery('.timepicker').timepicker({dateFormat : 'yy-mm-dd'});
	});	

});