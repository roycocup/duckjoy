
// Set maxlength value for your required fields
var maxlength = 255;
/*
* You can pass three parameters this function
* Example : ValidateRequiredField(phone,"Telephone must be filled out!", "number");
* For string format no need to pass any strFormat.
*/
function ValidateRequiredField(field,alerttxt,strFormat) {
	with (field) {
		if (value == null|| value == "") {
			field.style.background= "grey";
			alert(alerttxt);return false;
		} else if (value.length > maxlength ) {
			field.style.background= "grey";
			alert('Maxlenth should be not more than 255 charactor');return false;
		} else if (strFormat == 'number' && isNaN(value) ) {
			field.style.background= "grey";
			alert(field.name + ' is not a number, Please put in Numric format');return false;
		} else {return true;}
	}
}

/*
* Using the function you can validate the email functions
* Example: ValidateEmailAddress(email,"Email is not in Valid format!")
* Return true or false
*/
function ValidateEmailAddress(field, alerttxt) {
	with (field) {
		apos=value.indexOf("@");
		dotpos=value.lastIndexOf(".");
		if (apos < 1 || dotpos-apos < 2)
			{alert(alerttxt);return false;}
		else {return true;}
	}
}

/*
* Using the function you can validate the checkbox in the form
* Example: ValidateCheckBox(agreement,"Agreement is not checked!")
* Return true or false
*/
function ValidateCheckBox(field,alerttxt) {
	with (field) {
		if (!field.checked == 1) {
			alert(alerttxt);return false;
		} else {return true;}
	}
}

/*
* Using the function you can validate the checkbox in the form
* Example: ValidateRequiredField(website,"Website name is required!")
* Return true or false
*/
function ValidateWebAddress(field,alerttext) {
	with(field) {
		
		var companyUrl = value;
		
		var RegExp = /^(([w]+:)?//)?(([dw]|%[a-fA-fd]{2,2})+(:([dw]|%[a-fA-fd]{2,2})+)?@)?([dw][-dw]{0,253}[dw].)+[w]{2,4}(:[d]+)?(/([-+_~.dw]|%[a-fA-fd]{2,2})*)*(?(&?([-+_~.dw]|%[a-fA-fd]{2,2})=?)*)?(#([-+_~.dw]|%[a-fA-fd]{2,2})*)?$/;
		
		if(RegExp.test(companyUrl)) {
			return true;
		} else {
			alert(alerttext);
			return false;
		}
	}
}

function ValidateCompleteForm(thisform) {
	with (thisform) {
		
		if (ValidateRequiredField(full_name,"First name is required!")== false) {full_name.focus();return false;}
		
		if (ValidateRequiredField(email_address,"Email must be filled out!")== false) {email_address.focus();return false;}
		
		if (ValidateEmailAddress(email_address,"Email is not in Valid format!")== false) {email_address.focus();return false;}
		
		if (ValidateRequiredField(website_name,"Website name is required!")== false) {website_name.focus();return false;}
		
		if (ValidateWebAddress(website,"Website Address is not incorrect format!")== false) {agreement.focus();return false;}
		
		if (ValidateRequiredField(mobile,"Mobile must be filled out!", "number")== false) {phone.focus();return false;}
		
		if (ValidateCheckBox(sex,"sex is not checked!")== false) {sex.focus();return false;}
		
	}
}
