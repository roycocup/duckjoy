
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>

<?php get_header(); ?>

<script>
jQuery(document).ready(function($) {
	var time = '<?php echo format_js_date(get_post_meta( $post->ID, 'end_date', true )); ?>';
	setInterval(function(){
		str = countdown(time);
		$('.remain').html(str);
	}, 1000);

	//availability depending on expiry date
	check_available();
	function check_available(){
		var post_time = new Date(time);
		var now = new Date();
		console.log(now, post_time);		
		if (post_time < now){
			$('input[type="submit"]').attr('disabled','disabled').css({'background-color':'#ccc'});
		}else {
			$('input[type="submit"]').removeAttr('disabled').css({'background-color':'rgba(104,187,109,1)'});	
		}
	}

});


function countdown(end){

	var end  = new Date(end);
	var now = new Date();
	var _second = 1000;
	var _minute = _second * 60;
	var _hour = _minute * 60;
	var _day = _hour * 24;
	var timer;
	var distance = end - now;

	
	if (distance < 0) {
		clearInterval(timer);
		return '本次团购已结束';
	}
	var days = Math.floor(distance / _day);
	var hours = Math.floor((distance % _day) / _hour);
	var minutes = Math.floor((distance % _hour) / _minute);
	var seconds = Math.floor((distance % _minute) / _second);

	str = Math.abs(days) + '天 ';
	str += Math.abs(hours) + '时 ';
	str += Math.abs(minutes) + '分 ';
	str += Math.abs(seconds) + '秒';

	return str;
	

}

</script>

<style>

	#content{overflow: auto;}
	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo{width:300px !important; height:40px !important;}
	.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
	li.group-menu a{background-color: white; color: #f75959 !important;}

	#left-side{float: left; width:670px;}
	.content-side-wrapper{ width: 633px; padding-left: 40px; margin-top: 30px;}
		article{margin: 0; padding: 0;}
		.header{margin: 0; padding: 0;}
			.title{margin: 0; padding: 0; color: #87bf7f; margin-bottom: 10px;}
			.thumbnail{margin: 0; padding: 0; border:2px dashed #87bf7f; margin-bottom: 40px;}
				.thumbnail img{ max-width: 600px; padding: 5px;}
	
	#side{ float: right; width: 290px; border: 5px solid #ccc; margin-top: 30px; margin-right: 40px;}
		#orderinfo{margin: 10px;}
			form{}
			table, tr, td{border:none;}
			tr.bottom-border{ border-bottom:1px solid #ccc;}
			.attachment-post-thumbnail{border:3px dashed white; padding:5px;}
			.text{font-size: 14px;}
			.red{color: #f75959;}
			.striketrough{text-decoration:line-through}
			button, input[name="orderbutton"]{margin: 0; padding: 0; background-color: rgba(104,187,109,1); color: black; height: 30px; width: 263px; font-size: 18px; border: none;}
			button, input[name="orderbutton"]{text-align: center; margin-top:10px; color: white;}
			button,input[type="submit"]{background: rgba(104,187,109,1); text-decoration: none;}
	
	#help{float: right; clear: right; width: 290px; height: 100px; border: 1px solid #ccc; margin-top: 30px; margin-right: 40px; }	
		#help .help-wrapper { margin-top: 20px;  }
			#help .help-wrapper .title{font-size: 34px; float: left; padding-left: 5px}
			#help .help-wrapper .sub-text{float: right; font-size: 14px; padding-right: 30px;}

	.flashmessages{ float:left; width:945px; margin:40px; padding:20px 40px; background-color: rgb(239,215,217); color: rgb(168,77,62); min-height: 20px;}
</style>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

	<?php if (!empty($_POST['messages'])) :?>
		<div class="flashmessages">
		<?php foreach ($_POST['messages'] as $message): ?>
			<li><?php echo $message; ?></li>
		<?php endforeach; ?>
		</div>
	<?php endif; ?>

		<div id="left-side">
			<div class="content-side-wrapper">
				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php 
					$all_meta = get_post_custom(get_the_ID()); 
					$taxonomies['company'] = wp_get_post_terms(get_the_ID(), 'company');
					require_once(WP_PLUGIN_DIR."/duckjoy_group/models/groups.php");
					require_once(WP_PLUGIN_DIR."/duckjoy_group/models/itinerary.php");
					require_once(WP_PLUGIN_DIR."/duckjoy_orders/models/orders.php");
					$model = new Groups_model();
					$itinerary_model = new Itineraty_model();
					$orders_model = new Orders_model();
					$origin_and_destination = $itinerary_model->get_origin_and_destination_for_post_id(get_the_ID());
					$availability = $model->get_availability_for_post_id(get_the_ID());
					$all_meta = get_post_custom(get_the_ID());
					$prices = unserialize(unserialize($all_meta['prices'][0]));
					$best_price = $prices['double'];
					$used_to_be_price = get_field('used_to_be_price'); 
					$diff = abs($best_price - $used_to_be_price);

					?>
					<?php $prices = unserialize(unserialize($all_meta['prices'][0])); ?>

					<article id="post-<?php the_ID(); ?>">

						<header class="header">
							<h3 class="title"><?php the_title(); ?></h3>

							<div class="thumbnail">
								<img src="<?php the_field('detail_image'); ?>" alt="">
							</div>
							

						</header>

						<div class="entry-content">
							<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
						</div>

						<div id="tabs">
							<ul>
								<li class="nav1"><a href="#tab1"><?php _e("Day-by-Day", 'duckjoy');?></a></li>
								<li class="nav2"><a href="#tab2"><?php _e("Visa", 'duckjoy');?></a></li>
								<li class="nav3"><a href="#tab3"><?php _e("Fare Description", 'duckjoy');?></a></li>
								<li class="nav4 last"><a href="#tab4"><?php _e("Precautions",'duckjoy');?></a></li>
							</ul>

							<div id="tab1" class="tab">
								<?php echo the_field('day_by_day'); ?>
							</div>

							<div id="tab2" class="tab">
								<?php echo the_field('visa'); ?>
							</div>

							<div id="tab3" class="tab">
								<?php echo the_field('fare_description'); ?>
							</div>

							<div id="tab4" class="tab">
								<?php echo the_field('precautions'); ?>
							</div>
						</div> <!-- tabs -->

					</article>

				<?php endwhile; ?>

				<div id="sharebutton"></div>
				<div id="articlelinks"></div>
				<div id="comments"></div>


				<!-- UY BEGIN -->
				<div id="uyan_frame"></div>
				<script type="text/javascript" src="http://v2.uyan.cc/code/uyan.js"></script>
				<!-- UY END -->
			</div> <!-- content-side-wrapper -->
		</div> <!-- left-side -->

		<div id="side">
			<div id="orderinfo">
				<form action="<?php get_permalink(); ?>" method="post" name="orderinfo">
					<?php $nonce = wp_create_nonce( 'orderinfo_'.$post->ID ); ?>
					<input type="hidden" name="nonce" value="<?php echo $nonce; ?>" />
					<input type="hidden" name="offer_code" value="<?php echo $all_meta['offer_code'][0]; ?>" />
					<input type="hidden" name="company" value="<?php echo $taxonomies['company'][0]->name; ?>" />
					<input type="hidden" name="route_name" value="<?php echo $post->post_title; ?>" />
					<input type="hidden" name="post_id" value="<?php echo get_the_ID(); ?>" />
					<input type="hidden" name="stars" value="<?php echo $all_meta['stars'][0]; ?>" />
					<input type="hidden" name="group_detail" value="true" />
					<table>
						<tbody>
							<tr class="bottom-border">
								<td colspan="3"><?php _e("Offer Code: ", 'duckjoy')?><?php echo $all_meta['offer_code'][0]; ?></td>
							</tr>
							<tr class="bottom-border">
								<td colspan="3"><?php _e("Origin: ", 'duckjoy')?><?php echo $origin_and_destination['origin']->place_name; ?></td>
							</tr>
							<tr class="bottom-border">
								<td colspan="3"><?php _e("Destination: ", 'duckjoy')?><?php echo $origin_and_destination['destination']->place_name; ?></td>
							</tr>
							<!-- <tr class="bottom-border">
								<td colspan="3"><?php _e("Travel Company: ", 'duckjoy')?> <?php echo $taxonomies['company'][0]->name; ?></td>
							</tr> -->
							<tr class="bottom-border">
								<td colspan="3"><?php _e("Deal Ends: ", 'duckjoy')?><span class="remain"></span></td>
							</tr>
							<tr class="bottom-border">
								<td colspan="3"><?php _e("Travel Date：", 'duckjoy')?>
									<input type="hidden" name="traveldates" value="<?php echo $availability[0]->availability_code;?>">
									<?php echo date('Y-m-d',strtotime($availability[0]->departure));?>
								</td>
							</tr>
							<tr>
								<td>
									<?php _e("Minimum number:", 'duckjoy');?><?php echo the_field('minimum_number_of_buyers');?>人
								</td>
							</tr>
							<tr>
								<td> 
									<?php $many_bought = $orders_model->getBoughtOrdersForPostId($post->ID); ?>
									已有<span class="red"><?php echo (int)$many_bought; ?></span>人预订
								</td>
							</tr>

							<tr class="bottom-border">
								<td colspan="3">现价/原价: <span class="red"><?php echo $best_price;?>镑</span>/<span class='striketrough'><?php echo $used_to_be_price; ?>镑<span></td>
							</tr>
						</tbody>
					</table>

					<table>
						<thead>
							<!--<td>成人：&nbsp;儿童：&nbsp;学生：</td>-->
							<td>房型</td>
							<td>数量</td>
							<td><?php _e("Adults", 'duckjoy');?></td>
							<td><?php _e("Children", 'duckjoy');?></td>
						</thead>
						<tr>
							<!-- 双人间 -->
							<td><?php _e("Double Room", 'duckjoy');?></td>
							<td>
								<select name="doubles_number" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['doubles_number']) && $i == $_POST['doubles_number']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected; ?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<td>
								<select name="doubles_adult" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['doubles_adult']) && $i == $_POST['doubles_adult']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected; ?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<!-- double children -->
							<td>
								<select name="doubles_children" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['doubles_children']) && $i == $_POST['doubles_children']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected;?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
						</tr>
						<tr>
							<td><?php _e("Single Room", 'duckjoy');?></td>
							<td>
								<select name="singles_number" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['singles_number']) && $i == $_POST['singles_number']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected; ?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<td>
								<select name="singles_adult" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['singles_adult']) && $i == $_POST['singles_adult']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected;?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<td>
								<select name="singles_children" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['singles_children']) && $i == $_POST['singles_children']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected;?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
						</tr>
					</table>
					
					<div class="text">
						<?php _e("Rooms in total：", 'duckjoy');?><span id="total_rooms">0</span>
					</div>
					<div class="text">
						<?php _e("Price in total：", 'duckjoy');?><span id="total_price">0</span>镑
					</div>
					<div>
					<?php if (get_option('dj_orders_buy_on')): ?>
						<input type="submit" name="orderbutton" value="说走就走">
					<?php else: ?>
						<input type="button" name="orderbutton" value="<?php echo _e('Buying is not available', 'duckjoy'); ?> " disabled="true">
					<?php endif; ?>
					</div>		
				</form>
			</div>
		</div> <!-- #side -->

		<div id="help">
			<div class="help-wrapper">
				<?php $upload_dir = wp_upload_dir(); ?>
				<a href="<?php echo $upload_dir['baseurl']; ?>/2013/12/help.pdf"><span class="red title">新手速成</span></a>
				<div class="sub-text">
					第一次报名吗?<br>
					赶紧戳我吧！
				</div>
			</div>
		</div>

		<div class="clearfix spacer"></div>

		<div class="clearfix"></div>

	</div><!-- #content -->
</div><!-- #primary -->



<script>
	jQuery(document).ready(function(){
		calculate_prices();

		$('.prices').change(function(){
			calculate_prices();
		});

		function calculate_prices(){

			var doubles_number = parseInt($('select[name=doubles_number]').val());
			var singles_number = parseInt($('select[name=singles_number]').val());

			var doubles_adult = {
				number : $('select[name=doubles_adult]').val(),
				price : <?php echo $prices['double']; ?> 
			};
			doubles_adult.multiply = parseInt(doubles_adult.number * doubles_adult.price);
			var doubles_children = {
				number : $('select[name=doubles_children]').val(),
				price : <?php echo $prices['children']; ?>
			};
			doubles_children.multiply = parseInt(doubles_children.number * doubles_children.price);
			// var doubles_student = {
			// 	number : $('select[name=doubles_student]').val(),
			// 	price : <?php echo $prices['double_student']; ?>
			// };
			// doubles_student.multiply = parseInt(doubles_student.number * doubles_student.price);
			var singles_adult = {
				number : $('select[name=singles_adult]').val(),
				price : <?php echo $prices['single']; ?>
			};
			singles_adult.multiply = parseInt(singles_adult.number * singles_adult.price);
			var singles_children = {
				number : $('select[name=singles_children]').val(),
				price : <?php echo $prices['children']; ?>
			};
			singles_children.multiply = parseInt(singles_children.number * singles_children.price);
			// var singles_student = {
			// 	number : $('select[name=singles_student]').val(),
			// 	price : <?php echo $prices['single_student']; ?>
			// };
			// singles_student.multiply = parseInt(singles_student.number * singles_student.price);

			var total_price = doubles_adult.multiply 
			+ doubles_children.multiply 
			// + doubles_student.multiply 
			+ singles_adult.multiply 
			+ singles_children.multiply ;
			// + singles_student.multiply

		var total_rooms = doubles_number +  singles_number;
		
		$('#total_rooms').html(total_rooms);
		$('#total_price').html(total_price);

	}
});
</script>

<script>
// TABS
$(function() {
	$( "#tabs" ).tabs({
		fx: { 
			opacity: 'toggle' 
		}
	});

});
</script>

<!-- Organic Tabs -->
<script type='text/javascript'>
	// jQuery(document).ready(function($) {
	// 	jQuery(function() {
	// 		jQuery("#tabs").organicTabs({
	// 			"speed": 200
	// 		});
	// 	});
	// });
</script>

<script type="text/javascript" src="http://v2.uyan.cc/code/uyan.js"></script>

<?php get_footer(); ?>
