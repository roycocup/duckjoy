<?php
/**
 * Template Name: Compare
 * Compare page template
 */
get_header(); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>

<style>
	#page{background-color: white; border: none;}
	#primary{margin:0 auto; padding: 30px;padding-bottom: 135px; background-color: white; width: 100%; margin: 0; padding: 0;}
	#content{background-image: url('<?php echo get_stylesheet_directory_uri()."/img/green-bg.jpg"?>'); background-repeat:repeat-y;}
	#login, #login a, #login a:hover{color: black;}
	#searchbox input{border:2px dashed #3d8150; font-size: 18px;}

	
	#masthead.site-header .navbar {top:-5px; background-color: white; top:10px; height: 38px;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {color: #3d8150; background-color: white; padding-top: 0; padding-bottom: 0;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a:hover {background-color: white;border-bottom:8px solid #3d8150;}
	.current_page_item a {border-bottom:10px solid #3d8150;}
	.nav-menu li.page_item:last-child a{border-bottom-width: 8px; border-bottom-style: solid; border-bottom-color: rgba(62,124,78,1);}
	
	
	
	.menu-logo{width:300px !important; height:37px !important;  padding:0 !important;}
	.menu-logo a{width:300px !important; height:37px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo('stylesheet_directory'); ?>/img/logo_compare.jpg) no-repeat center center !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left; }
	#magnifier_glass{float:left;}
	#masthead.site-header .site-branding #login {float: right;padding-right: 20px;font-size: 16px;}
	#masthead.site-header .navbar .main-navigation .nav-menu .current_page_item a {background-color: white;color:#3d8150 !important;}
	.last-menu-item{padding-left: 0}
	.visa-menu{padding-left: 0}
	
</style>

<!-- style via js that needs to override css -->
<script>
	jQuery(document).ready(function($){
		$('#login, #login a').css('color','#3d8150');
		var imgsrc = '<?php echo get_stylesheet_directory_uri(); ?>/img/green_magnifier.jpg';
		$('#magnifier_glass a').html('<img src="'+imgsrc+'" alt="">');
	});
</script>

<?php 
//Flash messages
if (!empty($_POST['messages'])) {
	foreach ($_POST['messages'] as $message){
		echo '<h3 style="color:red">'.$message.'</h3>';
	}
}
?>


<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

			<?php the_content(); ?>
			
	</div><!-- #content -->
</div><!-- #primary -->






