
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>

<?php //wp_enqueue_style( 'comparetabs-css', get_stylesheet_directory_uri()."/css/comparetab.css" ); ?>
<?php //wp_enqueue_script( 'comparetabs-js', get_stylesheet_directory_uri()."/js/comparetab.js", 'jquery' ); ?>

<style>
	#page{background-color: white; border: none; padding:0; }
	#primary{margin:0 auto; padding: 30px;padding-bottom: 135px; background-color: white; width: 100%; margin: 0; padding: 0;}
	#content{background-image: url('<?php echo get_stylesheet_directory_uri()."/img/green-bg.jpg"?>'); background-repeat:repeat-y; overflow: auto;}
	#login, #login a, #login a:hover{color: black; margin-top: -50px;}
	#searchbox input{border:2px dashed #3d8150; font-size: 18px;}

	.site-branding{margin-top: -20px;}
	#masthead.site-header .navbar {top:-5px; background-color: white; height: 38px;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {color: #3d8150; background-color: white; padding-top: 0; padding-bottom: 0;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a:hover {background-color: white;border-bottom:8px solid #3d8150;}
	.current_page_item a {border-bottom:8px solid #3d8150;}
	.last-menu-item a{border-bottom:8px solid #3d8150;}
	.nav-menu li.page_item:last-child a{border-bottom-width: 8px; border-bottom-style: solid; border-bottom-color: rgba(62,124,78,1);}
	
	.menu-logo{width:300px !important; height:37px !important;  padding:0 !important;}
	.menu-logo a{width:300px !important; height:37px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo('stylesheet_directory'); ?>/img/logo_compare.jpg) no-repeat center center !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left; }
	#magnifier_glass{float:left; background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/green_magnifier.jpg');}
	#masthead.site-header .site-branding #login {float: right;padding-right: 20px;font-size: 16px;}
	#masthead.site-header .navbar .main-navigation .nav-menu .current_page_item a {background-color: white;color:#3d8150 !important;}
	.last-menu-item{padding-left: 0}
	.visa-menu{padding-left: 0}


	#left-side{float: left; width:693px;}
	.content-side-wrapper{ background-color: white; width: 633px; margin: 30px auto; padding: 40px 15px;}
		article{margin: 0; padding: 0;}
		.entry-header{margin: 0; padding: 0;}
			.entry-title{margin: 0; padding: 0; color: #87bf7f; margin-bottom: 10px;}
			.entry-thumbnail{margin: 0; padding: 0; border:2px dashed #87bf7f; margin-bottom: 40px;}
			.entry-content{}
	
	#side{ float: left; width: 370px; }
		#orderinfo{ background-color: white; margin:20px 40px 10px 40px;}
			form{ margin: 30px 10px; padding-bottom: 20px;}
			table, tr, td{border:none;}
			tr.bottom-border{ border-bottom:1px solid #ccc;}
			.text{font-size: 14px;}
			.attachment-post-thumbnail{border:3px dashed white; padding:5px;}
			.image{padding: 5px; max-width: 600px;}
			button, input[name="orderbutton"]{margin: 0; padding: 0; background-color: rgba(104,187,109,1); color: black; height: 30px; width: 263px; font-size: 18px; border: none;}
			button, input[name="orderbutton"]{text-align: center; margin-top:10px; color: white;}
			button,input[type="submit"]{background: rgba(104,187,109,1); text-decoration: none;}
			button:hover,input[type="submit"]:hover{background: rgba(104,190,11,1)}

	#help{float: right; clear: right; width: 290px; height: 100px; border: 1px solid #ccc; margin-top: 10px; margin-right: 40px; background-color: white; }
		#help .help-wrapper { margin-top: 20px;  }
			#help .help-wrapper .title{font-size: 34px; float: left; padding-left: 5px}
			#help .help-wrapper .sub-text{float: right; font-size: 14px; padding-right: 30px;}

			
	.address{width: 420px;height: 68px;float:right; text-align: right;font-size: 12px;margin-top: 15px; color: white; margin-right: 10px; }
	.spacer{height: 100px;}

	footer{visibility: hidden;}

	.flashmessages{ float:left; width:990px; margin:40px; padding:20px 40px; background-color: rgb(239,215,217); color: rgb(168,77,62); min-height: 20px;}
</style>
<!-- style via js that needs to override css -->
<script>
	jQuery(document).ready(function($){
		$('#login, #login a').css('color','#3d8150');
		//to fix the 140 cut out problem
		$('#uyan_loginot + div').css('height', '70px');
	});
</script>
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
		
		<?php if (!empty($_POST['messages'])) :?>
			<div class="flashmessages">
			<?php foreach ($_POST['messages'] as $message): ?>
				<li><?php echo $message; ?></li>
			<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<div id="left-side">
			<div class="content-side-wrapper">
				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php $all_meta = get_post_custom(get_the_ID()); 
					$taxonomies['company'] = wp_get_post_terms(get_the_ID(), 'company');
					require_once(WP_PLUGIN_DIR."/duckjoy_offers/models/offers.php");
					$model = new Offers_model();
					$availability = $model->get_availability_for_post_id(get_the_ID());
					?>
					<?php $prices = unserialize(unserialize($all_meta['prices'][0])); ?>

					<article id="post-<?php the_ID(); ?>">

						<header class="entry-header">
							<h3 class="entry-title"><?php the_title(); ?></h3>

							<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
								<div class="entry-thumbnail">
									<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>
									<img class="image" src="<?php echo $url; ?>" alt="">
								</div>
							<?php endif; ?>

						</header>

						<div class="entry-content">
							<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
						</div>

						<div id="tabs">
							<ul>
								<li class="nav1"><a href="#tab1"><?php _e("Day-by-Day", 'duckjoy');?></a></li>
								<li class="nav2"><a href="#tab2"><?php _e("Visa", 'duckjoy');?></a></li>
								<li class="nav3"><a href="#tab3"><?php _e("Fare Description", 'duckjoy');?></a></li>
								<li class="nav4 last"><a href="#tab4"><?php _e("Precautions", 'duckjoy');?></a></li>
							</ul>

							<div id="tab1" class="tab">
								<?php echo the_field('day_by_day'); ?>
							</div>

							<div id="tab2" class="tab">
								<?php echo the_field('visa'); ?>
							</div>

							<div id="tab3" class="tab">
								<?php echo the_field('fare_description'); ?>
							</div>

							<div id="tab4" class="tab">
								<?php echo the_field('precautions'); ?>
							</div>
						</div> <!-- tabs -->

					</article>

				<?php endwhile; ?>

				<div id="sharebutton"></div>
				<div id="articlelinks"></div>
				<div id="comments"></div>


				<!-- UY BEGIN -->
				<div id="uyan_frame"></div>
				<script type="text/javascript" src="http://v2.uyan.cc/code/uyan.js"></script>
				<!-- UY END -->
			</div> <!-- content-side-wrapper -->
		</div> <!-- left-side -->

		<div id="side">
			<div id="orderinfo">
				<form action="<?php get_permalink(); ?>" method="post" name="orderinfo">
					<?php $nonce = wp_create_nonce( 'orderinfo_'.$post->ID ); ?>
					<input type="hidden" name="nonce" value="<?php echo $nonce; ?>" />
					<input type="hidden" name="offer_code" value="<?php echo $all_meta['offer_code'][0]; ?>" />
					<input type="hidden" name="company" value="<?php echo $taxonomies['company'][0]->name; ?>" />
					<input type="hidden" name="route_name" value="<?php echo $post->post_title; ?>" />
					<input type="hidden" name="post_id" value="<?php echo get_the_ID(); ?>" />
					<input type="hidden" name="stars" value="<?php echo $all_meta['stars'][0]; ?>" />
					<table>
						<tbody>
							<tr class="bottom-border">
								<td colspan="3"><?php _e("Offer Code: ", 'duckjoy')?><?php echo $all_meta['offer_code'][0]; ?></td>
							</tr>
							<tr class="bottom-border">
								<td colspan="3"><?php _e("Travel Company: ", 'duckjoy')?> <?php echo $taxonomies['company'][0]->name; ?></td>
							</tr>
							<tr class="bottom-border">
								<td colspan="3"><?php _e("Travel Dates：", 'duckjoy')?>
									<label for="traveldates"></label>
									<select name="traveldates" id="traveldates">
										<?php foreach($availability as $k => $v):?>
											<option tag="<?php echo $v->status;?>" value="<?php echo $v->availability_code;?>"><?php echo date('Y-m-d',strtotime($v->departure));?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr class="bottom-border">
								<td colspan="3"><?php _e("Availability： ", 'duckjoy')?><span class="available-text"><?php _e("Available", 'duckjoy');?></span></td>
							</tr>
							<tr>
								<td><?php _e("Price：", 'duckjoy'); ?></td>
								<td colspan="2"><?php echo $prices['double'] ?>镑/<?php _e('adult', 'duckjoy');?></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2"><?php echo $prices['double_student'] ?>镑/<?php _e('student', 'duckjoy');?></td>
							</tr>
							<tr class="bottom-border">
								<td></td>
								<td colspan="2"><?php echo $prices['children'] ?>镑/<?php _e('child', 'duckjoy');?></td>
							</tr>
						</tbody>
					</table>

					<table>
						<thead>
							<!--<td>成人：&nbsp;儿童：&nbsp;学生：</td>-->
							<td>房型</td>
							<td>数量</td>
							<td><?php _e("Adults", 'duckjoy');?></td>
							<td><?php _e("Children", 'duckjoy');?></td>
							<td><?php _e("Students", 'duckjoy');?></td>
						</thead>
						<tr>
							<!-- 双人间 -->
							<td><?php _e("Double Room", 'duckjoy');?></td>
							<td>
								<select name="doubles_number" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['doubles_number']) && $i == $_POST['doubles_number']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected; ?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<td>
								<select name="doubles_adult" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['doubles_adult']) && $i == $_POST['doubles_adult']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected; ?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<!-- double children -->
							<td>
								<select name="doubles_children" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['doubles_children']) && $i == $_POST['doubles_children']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected;?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<td>
								<select name="doubles_student" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['doubles_student']) && $i == $_POST['doubles_student']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected;?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
						</tr>
						<tr>
							<td><?php _e("Single Room", 'duckjoy');?></td>
							<td>
								<select name="singles_number" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['singles_number']) && $i == $_POST['singles_number']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected; ?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<td>
								<select name="singles_adult" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['singles_adult']) && $i == $_POST['singles_adult']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected;?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<td>
								<select name="singles_children" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['singles_children']) && $i == $_POST['singles_children']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected;?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
							<td>
								<select name="singles_student" class="prices">
									<?php for ($i=0; $i <= 8; $i++) :?>
										<?php $selected = (isset($_POST['singles_student']) && $i == $_POST['singles_student']) ? 'selected' : '' ; ?>
										<option value="<?php echo $i; ?>" <?php echo $selected;?> ><?php echo $i; ?></option>
									<?php endfor;?>
								</select>
							</td>
						</tr>
					</table>
					<div class="text">
						<?php _e("Rooms in total：", 'duckjoy');?><span id="total_rooms">0</span>
					</div>
					<div class="text">
						<?php _e("Price in total：", 'duckjoy');?><span id="total_price">0</span>镑
					</div>
					<div>
					<?php if (get_option('dj_orders_buy_on')): ?>
						<input type="submit" name="orderbutton" value="说走就走">
					<?php else: ?>
						<input type="button" name="orderbutton" value="<?php echo _e('Buying is not available', 'duckjoy'); ?> " disabled="true">
					<?php endif; ?>
					</div>		
				</form>
			</div>
		</div> <!-- #side -->

		<div class="clearfix spacer"></div>

		<div id="help">
			<div class="help-wrapper">
				<?php $upload_dir = wp_upload_dir(); ?>
				<a href="<?php echo $upload_dir['baseurl']; ?>/2013/12/help.pdf"><span class="red title">新手速成</span></a>
				<div class="sub-text">
					第一次报名吗?<br>
					赶紧戳我吧！
				</div>
			</div>
		</div>

		<div class="clearfix spacer"></div>

		<div class="clearfix address">
			Duckjoy Limited 2A Compton House, Guildford, GU1 4TX
			<br/>Tel: +44 (0)7410 429595
			<br/>@2013-2015 大脚鸭旅游网 | ALL rights reserved
		</div>

		<div class="clearfix"></div>

	</div><!-- #content -->
</div><!-- #primary -->


<script>
	//Syncing the available dates with the available words
	jQuery(document).ready(function($){
		check_available();
		$('#traveldates').change(function(){
			check_available();
		});
		function check_available(){
			var traveldates_select = $('#traveldates');
			var available = $('option:selected', traveldates_select).attr('tag');

			if (parseInt(available) === 0 || available === undefined){
				$('.available-text').css('color', 'red').html('已满'); //unavailable
				$('input[type="submit"]').attr('disabled','disabled').css({'background-color':'#ccc'});
			}else {
				$('.available-text').css('color', '').html('未满'); //available
				$('input[type="submit"]').removeAttr('disabled').css({'background-color':'rgba(104,187,109,1)'});	
			}
		}
	});
</script>


<script>
	jQuery(document).ready(function(){
		calculate_prices();

		$('.prices').change(function(){
			calculate_prices();
		});

		function calculate_prices(){

			var doubles_number = parseFloat($('select[name=doubles_number]').val());
			var singles_number = parseFloat($('select[name=singles_number]').val());

			var doubles_adult = {
				number : $('select[name=doubles_adult]').val(),
				price : <?php echo $prices['double']; ?> 
			};
			doubles_adult.multiply = parseFloat(doubles_adult.number * doubles_adult.price);
			var doubles_children = {
				number : $('select[name=doubles_children]').val(),
				price : <?php echo $prices['children']; ?>
			};
			doubles_children.multiply = parseFloat(doubles_children.number * doubles_children.price);
			var doubles_student = {
				number : $('select[name=doubles_student]').val(),
				price : <?php echo $prices['double_student']; ?>
			};
			doubles_student.multiply = parseFloat(doubles_student.number * doubles_student.price);
			var singles_adult = {
				number : $('select[name=singles_adult]').val(),
				price : <?php echo $prices['single']; ?>
			};
			singles_adult.multiply = parseFloat(singles_adult.number * singles_adult.price);
			var singles_children = {
				number : $('select[name=singles_children]').val(),
				price : <?php echo $prices['children']; ?>
			};
			singles_children.multiply = parseFloat(singles_children.number * singles_children.price);
			var singles_student = {
				number : $('select[name=singles_student]').val(),
				price : <?php echo $prices['single_student']; ?>
			};
			singles_student.multiply = parseFloat(singles_student.number * singles_student.price);

			var total_price = doubles_adult.multiply 
			+ doubles_children.multiply 
			+ doubles_student.multiply 
			+ singles_adult.multiply 
			+ singles_children.multiply 
			+ singles_student.multiply;

		// var total_rooms = parseInt(doubles_adult.number) 
		// + parseInt(doubles_student.number)
		// + parseInt(singles_adult.number) 
		// + parseInt(singles_student.number);

		var total_rooms = doubles_number +  singles_number;
		
		$('#total_rooms').html(total_rooms);
		$('#total_price').html(total_price);

	}
});
</script>

<script>
// TABS
$(function() {
	$( "#tabs" ).tabs({
		fx: { 
			opacity: 'toggle' 
		}
	});

});
</script>

<!-- Organic Tabs -->
<script type='text/javascript'>
	// jQuery(document).ready(function($) {
	// 	jQuery(function() {
	// 		jQuery("#tabs").organicTabs({
	// 			"speed": 200
	// 		});
	// 	});
	// });
</script>

<script type="text/javascript" src="http://v2.uyan.cc/code/uyan.js"></script>


