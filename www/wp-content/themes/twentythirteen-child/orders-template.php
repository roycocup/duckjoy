<?php
/**
 * Template Name: Orders
 * 
 */
?>
<?php get_header(); ?>


<style>
	/*#masthead.site-header .navbar .main-navigation .nav-menu li:last-child a{background-color: white; color: #f75959;}*/
	
	#content{overflow: auto;}

	#left-side{float: left; width:655px; border-right: 1px solid #ccc;}
	.content-side-wrapper{ background-color: white; width: 633px; margin: 0 auto; padding: 0;}
		article{margin: 0; padding: 0;}
		.entry-header{margin: 0; padding: 0;}
			.entry-title{margin: 0; padding: 0; color: #87bf7f; margin-bottom: 10px;}
			.entry-thumbnail{margin: 0; padding: 0; border:2px dashed #87bf7f; margin-bottom: 40px;}
			.entry-content{}
	.blue-bar{height: 15px; background-color: rgb(18,142,226); color: white;}
	
	#right-side{ float: right; width: 370px; padding:20px;}
		.innerwrapper{ border: 1px solid #ccc;}
	
	td{}
	form{ margin: 30px 10px; padding-bottom: 20px;}
	
	.address{width: 420px;height: 68px;position: absolute;text-align: right;margin-left: 620px;font-size: 12px;margin-top: 15px; color: white;}
	.spacer{height: 100px;}
	.icon{ max-height: 20px;}

	.label{color: #14a7ea;}
	.label-small{ font-size: 16px;}
	.label-medium{ font-size: 18px;}
	.label-big{ font-size: 20px;}
	
</style>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				
				<?php the_content(); ?>
				
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_footer(); ?>