<?php
/**
 * Single Post Template: News Post Template
 */
?>
<?php get_header(); ?>

<style>
	#primary {overflow: auto;}
	#content{ float:left; width:670px; padding: 0 30px; border-right: 3px solid #ccc; margin: 30px 0;}

	.advertising{ width:350px; float: right;}
		.advertising ul {padding: 0; margin: 0; text-align: right;}
			.advertising li{ list-style: none; border:1px solid rgba(0,0,0,0);}
				.advertising li img{ max-width: 285px; max-height: 200px; margin-top: 30px; margin-right: 35px;}

	.article-title{font-size: 24px; color: rgb(102,191,127);}
	.article-header{font-size: 12px; color: #999999; line-height: 20px; margin: 20px 0;}
	.article-body{font-size: 14px;color: #666666;line-height: 23px;}


	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo{width:300px !important; height:40px !important;}
	.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
	li.news-menu a{background-color: white; color: #f75959 !important;}

	.sub-spacer{margin-left: 20px;}

</style>
	<div id="primary" class="content-area">

		<div class="advertising">
			<ul>
				<?php $news_page_id = '10'; ?>
				<li><a href="http://<?php the_field('link_for_ad_1', $news_page_id);?>" target="_blank" ><img src="<?php the_field('ad_1', $news_page_id);?>" alt="" class="advert"></a></li>
				<li><a href="http://<?php the_field('link_for_ad_2', $news_page_id);?>" target="_blank" ><img src="<?php the_field('ad_2', $news_page_id);?>" alt="" class="advert"></a></li>
				<li><a href="http://<?php the_field('link_for_ad_3', $news_page_id);?>" target="_blank" ><img src="<?php the_field('ad_3', $news_page_id);?>" alt="" class="advert"></a></li>
				<li><a href="http://<?php the_field('link_for_ad_4', $news_page_id);?>" target="_blank" ><img src="<?php the_field('ad_4', $news_page_id);?>" alt="" class="advert"></a></li>
			</ul>
		</div>

		<div id="content" class="site-content" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="article-title"><?php the_title() ?></div>

				<div class="article-header">
					<?php echo "来源： "; echo the_field('author'); ?>
					<?php echo "<span class='sub-spacer'>发布时间： </span>"; echo format_date($post->post_date); ?>
					<?php echo "<span class='sub-spacer'>阅读： </span>"; ?><?php if(function_exists('the_views')) { the_views(); } ?> <?php _e("次");?>	
				</div>

				<div class="article-body">
					<?php echo wpautop($post->post_content); ?>
				</div>
				
			<?php endwhile; ?>
			

			<div id="sharebutton"></div>
			<div id="articlelinks"></div>
			<div id="comments"></div>
			<div id="uyan_frame"></div>
			<script type="text/javascript" src="http://v2.uyan.cc/code/uyan.js"></script>

		</div><!-- #content -->
	</div><!-- #primary -->



<?php get_sidebar(); ?>
<?php get_footer(); ?>