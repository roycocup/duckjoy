<?php
/**
 * Template name: Visa
 *
 */

get_header(); ?>


<style>
	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo a{width:228px !important; height:91px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo('stylesheet_directory'); ?>/img/logo_common.jpg) no-repeat center center !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	.menu-logo{width: 228px !important;height: 91px !important;margin: 13px 32px 0 32px !important;padding: 0px 0 0 0 !important;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left; padding-top:43px;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 17px 13px 17px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
	#masthead.site-header .site-branding { height:40px;}
	
	#navbar { height:91px; }
	.last-menu-item{padding-left: 0px;}
	.visa-menu{padding-left: 0px;}
</style>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php global $post; ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					<header class="entry-header">
						<h1 class="entry-title"><?php echo $post->post_content; ?></h1>
					</header><!-- .entry-header -->

					<footer class="entry-meta">
						&nbsp;
						<?php //edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->

				</article><!-- #post -->

				<?php //comments_template(); ?>
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>