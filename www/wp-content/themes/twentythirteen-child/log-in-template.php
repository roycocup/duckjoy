<?php
/**
 * Template Name: Log-in
 * 
 */

get_header(); ?>

<style>
#login{font-size:16px !important; color:#fff !important;}
#magnifier_glass{float:left;}
.menu-logo{width:300px !important; height:40px !important;}
.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
.menu-logo a:hover{border:none !important; display:block;}
#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}



#main {width: 100%;background-color: white;float: left; min-height: 400px; font-size: 16px;}

.right {width:285px;margin-top:30px;float:right; margin-right:40px;}
.right a, #right a:hover, #right a:link, #right a:visited{ text-decoration: none; color: black;}

.left {width:630px;margin-top:30px;margin-left:40px;float:left; border-right:solid 3px #cccccc; padding:0 30px 0 0;}

.column-1{ float: left; text-align: right;}
.column-2{ float: left; margin-left: 10px;}
.column-2 input[type='submit'] { background-color: rgb(229,100,86); border-radius: 5px; color:white; box-shadow: none; border:none; height:40px; } 

.lwa-modal h4, .lwa-modal p{font: inherit;}


</style>

	<div id="primary" class="site-content">
		<div id="content" role="main">
		
		<?php login_with_ajax(); ?>
			
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>