
<style>

	input[type='submit'].bootstrap-blue {
		background-color: hsl(206, 100%, 30%) !important;
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#0090ff", endColorstr="#005699");
		background-image: -khtml-gradient(linear, left top, left bottom, from(#0090ff), to(#005699));
		background-image: -moz-linear-gradient(top, #0090ff, #005699);
		background-image: -ms-linear-gradient(top, #0090ff, #005699);
		background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #0090ff), color-stop(100%, #005699));
		background-image: -webkit-linear-gradient(top, #0090ff, #005699);
		background-image: -o-linear-gradient(top, #0090ff, #005699);
		background-image: linear-gradient(#0090ff, #005699);
		border-color: #005699 #005699 hsl(206, 100%, 25%);
		color: #fff !important;
		text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.33);
		-webkit-font-smoothing: antialiased;
		padding:5px 10px;
	} 

	input[type='submit'].bootstrap-grey {
		background-color: hsl(201, 0%, 86%) !important;
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#122122122", endColorstr="#dbdbdb");
		background-image: -khtml-gradient(linear, left top, left bottom, from(#122122122), to(#dbdbdb));
		background-image: -moz-linear-gradient(top, #122122122, #dbdbdb);
		background-image: -ms-linear-gradient(top, #122122122, #dbdbdb);
		background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #122122122), color-stop(100%, #dbdbdb));
		background-image: -webkit-linear-gradient(top, #122122122, #dbdbdb);
		background-image: -o-linear-gradient(top, #122122122, #dbdbdb);
		background-image: linear-gradient(#122122122, #dbdbdb);
		border-color: #dbdbdb #dbdbdb hsl(201, 0%, 79%);
		color: #333 !important;
		text-shadow: 0 1px 1px rgba(255, 255, 255, 0.46);
		-webkit-font-smoothing: antialiased;
		padding:5px 10px;
	}

	li.group-menu a{background-color: white; color: #f75959 !important;}
	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo{width:300px !important; height:40px !important;}
	.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
	#content{overflow:visible; background:#fff; float:left; width:100%;}
	
	
	.breadcrumbs-bar {color: #14a7ea;width: 1000px;text-align: center;}


	#left-side{float: left; width:600px; border-right: 1px solid #ccc;}
		#left-side .content-side-wrapper{ background-color: white; margin: 0 auto; padding: 0;}
		#left-side .blue-bar{height: 15px; background-color: rgb(18,142,226); color: white;}
		#left-side .blue-text{color: #14a7ea; padding-right: 5px;}
		#left-side table{width: 600px; font-size: 12px; border: none;}
		#left-side table tr{ border: none;}
		#left-side form{ margin: 30px 10px; padding-bottom: 20px;}
	
	
	#right-side{ float: right; width: 370px; padding:20px;}
		#right-side .innerwrapper{ border: 1px solid #ccc;}
		#right-side .label{color: #14a7ea; font-size: 18px;}
	
	
	.address{width: 420px;height: 68px;position: absolute;text-align: right;margin-left: 620px;font-size: 12px;margin-top: 15px; color: white;}
	.spacer{height: 100px;}
	.icon{ max-height: 20px;}
	



	button, input.buy{margin: 0; padding: 0; background-color: rgba(104,187,109,1); color: black; height: 30px; width: 263px; font-size: 18px; border: none;}
	button, input.buy{text-align: center; margin-top:10px; color: white;}
	button,input.buy{background: rgba(104,187,109,1); text-decoration: none;}
	button:hover,input.buy:hover{background: rgba(104,187,109,1)}
	
	
	/*New Css*/
	
	.breadcrumbs-bar{margin:0 30px !important; width:964px !important;}

#left-side {
float:left;
width: 620px;
border-right: 1px solid #ccc;
margin:0 0 0 30px !important;
padding:0 20px 0 0 !important;
}
#left-side form {
margin:0 !important;
padding-bottom: 20px;
}

.right-side2 {
float: right;
width: 370px;
padding:20px 30px 20px 20px !important;
}

#right-side .innerwrapper {
border: 1px solid #ccc;
padding: 0 0 20px 20px !important;
}

.evidence{padding:15px 0;}

#orderinfo {
background-color: white;
margin: 20px 0 20px 80px !important;
}

input{-webkit-border-radius: 2px;
-moz-border-radius: 2px;
border-radius: 2px; border:solid 1px #d2d2d2;} 
textarea{-webkit-border-radius: 2px;
-moz-border-radius: 2px;
border-radius: 2px; border:solid 1px #d2d2d2;} 

.font_grey td{color:#606060;}




input[type='submit'].bootstrap-blue {
color: #fff !important;
padding:0 !important;
background:url(<?php bloginfo('stylesheet_directory'); ?>/img/button1.png) no-repeat 0 0 !important; width:189px !important; height:33px !important; text-align:center; border:none !important; font-size:16px !important;
}


input[type='submit'].bootstrap-grey {
color: #979797 !important;
padding:0 !important;
background:url(<?php bloginfo('stylesheet_directory'); ?>/img/button2.png) no-repeat 0 0 !important; width:69px !important; height:33px !important; text-align:center; border:none !important;
}

.text_right{text-align:right;}

#left-side h3, h4{color:#14a7ea;}

#left-side .font_grey{color:#606060;}

.on-hover-show-help{position:relative; }

.on-hover-show-help-content{background:#fff; border:solid 1px #c7c7c7; box-shadow: 0px 1px 5px #c7c7c7; padding:10px 15px; -webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px; display:none; position:absolute; z-index:1; width:250px; left: -123px;
top: 36px;}
.on-hover-show-help-content:before{background:url(<?php bloginfo('stylesheet_directory'); ?>/img/arrow_top.png) no-repeat 100px top; height:17px; width:100%; content:' '; position: absolute;
top: -17px;}

.on-hover-show-help:hover .on-hover-show-help-content{display:block;}

.on-hover-show-help-content h3{color:#2b333c !important; border-bottom:solid 1px #3698dc !important; padding:0 0 15px 0 !important; margin:0 0 15px 0 !important;}
.on-hover-show-help-content p{color:#271e1e !important;  padding:00 !important; margin:0 !important;}
.on-hover-show-help-content p a{color:#271e1e !important;}


.content-side-wrapper td{border-top:none;}
.innerwrapper td{border-top:none;}
	
	
.flashmessages{ float:left; width:945px; margin:40px; padding:20px 40px; background-color: rgb(239,215,217); color: rgb(168,77,62); min-height: 20px;}
</style>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

	<?php if (!empty($_POST['messages'])) :?>
		<div class="flashmessages">
		<?php foreach ($_POST['messages'] as $message): ?>
			<li><?php echo $message; ?></li>
		<?php endforeach; ?>
		</div>
	<?php endif; ?>

	<?php 
		$title = (!empty($_POST['primary']['title']))? $_POST['primary']['title'] : '';
		$surname = (!empty($_POST['primary']['surname']))? $_POST['primary']['surname'] : '';
		$first_name = (!empty($_POST['primary']['first_name']))? $_POST['primary']['first_name'] : '';
		$dob['year'] = (!empty($_POST['primary']['dob']['year']))? $_POST['primary']['dob']['year'] : '';
		$dob['month'] = (!empty($_POST['primary']['dob']['month']))? $_POST['primary']['dob']['month'] : '';
		$dob['day'] = (!empty($_POST['primary']['dob']['day']))? $_POST['primary']['dob']['day'] : '';
		$type_passenger = (!empty($_POST['primary']['type_passenger']))? $_POST['primary']['type_passenger'] : '';
		$country = (!empty($_POST['primary']['country']))? $_POST['primary']['country'] : '';
		$country_phone_code = (!empty($_POST['primary']['country_phone_code']))? $_POST['primary']['country_phone_code'] : '+';
		$email = (!empty($_POST['primary']['email']))? $_POST['primary']['email'] : '';
		$confirm = (!empty($_POST['primary']['confirm']))? $_POST['primary']['confirm'] : '';
		$phone = (!empty($_POST['primary']['phone']))? $_POST['primary']['phone'] : '';
		$gender = (!empty($_POST['primary']['gender']))? $_POST['primary']['gender'] : '';
		$post_code = (!empty($_POST['primary']['post_code']))? $_POST['primary']['post_code'] : '';
		$address1 = (!empty($_POST['primary']['address1']))? $_POST['primary']['address1'] : '';
		$address2 = (!empty($_POST['primary']['address2']))? $_POST['primary']['address2'] : '';
		


		$pickup = (!empty($_POST['pickup']))? $_POST['pickup'] : 0;
		$visa = (!empty($_POST['visa']))? $_POST['visa'] : false;
		$notes = (!empty($_POST['notes']))? $_POST['notes'] : false;
		$couples = (!empty($_POST['couples']))? $_POST['couples'] : 0;
		
	?>
    
    <form action="<?php get_permalink(); ?>" method="POST">
    <input type="hidden" value="<?php echo wp_create_nonce('group_passenger_info');?>" name="nonce">
	
	<div>
		<table width="800" border="0" class="breadcrumbs-bar">
			<tr>
				<td><?php _e("Order Information", 'duckjoy');?></td>
				<td><?php _e("Passenger Info", 'duckjoy');?></td>
				<td><?php _e("Payment", 'duckjoy');?></td>
				<td><?php _e("Confirm Payment", 'duckjoy');?></td>
			</tr>
			<tr>
				<td colspan="4"><img src="<?php bloginfo('stylesheet_directory');  ?>/img/progressbar.jpg" /></td>
			</tr>
		</table>
	</div> <!-- breadcrumbs bar -->

	<hr>

	<div id="right-side" class="right-side2">
		<div class="innerwrapper">
			<table>
				<tr>
					<td colspan="3"><span class='label'><strong><?php echo $_SESSION['group_form_vars']['route_name']; ?></strong></span></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="font_grey">
					<td><?php _e("Stars:", 'duckjoy');?></td>
					<td>&nbsp;</td>
					<td><?php echo get_stars($_SESSION['group_form_vars']['stars']); ?></td>
				</tr>
				<tr class="font_grey">
					<td><?php _e("Departure date:", 'duckjoy');?></td>
					<td>&nbsp;</td>
					<td><?php echo date('Y-m-d', strtotime($_SESSION['group_form_vars']['departure_date'])); ?></td>
				</tr>
				<tr class="font_grey">
					<td><?php _e("Return date:", 'duckjoy');?></td>
					<td>&nbsp;</td>
					<td><?php echo date('Y-m-d', strtotime($_SESSION['group_form_vars']['return_date'])); ?></td>
				</tr>
				<tr class="font_grey">
					<td ><?php _e("Booking Details:", 'duckjoy');?></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="font_grey">
					<td><?php _e("Num Adults", 'duckjoy');?></td>
					<td></td>
					<td><?php echo $_SESSION['group_form_vars']['doubles_adult']+$_SESSION['group_form_vars']['singles_adult']; ?></td>
				</tr>
				<tr class="font_grey">
					<td><?php _e("Num Children", 'duckjoy');?></td>
					<td></td>
					<td><?php echo $_SESSION['group_form_vars']['doubles_children']+$_SESSION['group_form_vars']['doubles_children']; ?></td>
				</tr>
				<tr class="font_grey">
					<td><?php _e("Num Students", 'duckjoy');?></td>
					<td></td>
					<td><?php echo $_SESSION['group_form_vars']['doubles_student']+$_SESSION['group_form_vars']['singles_student']; ?></td>
				</tr>
				<tr class="font_grey">
					<td><?php _e("Num Rooms", 'duckjoy');?></td>
					<td></td>
					<td><?php echo $_SESSION['group_form_vars']['total_rooms']; ?></td>
				</tr>
				<tr class="font_grey">
					<td ><?php _e("Total Price", 'duckjoy');?></td>
					<td></td>
					<td><?php echo $_SESSION['group_form_vars']['total_price']; ?>镑</td>
				</tr>
			</table>
			<input type="submit" class="buy" name="group_passenger_info" value="<?php _e("Buy", 'duckjoy');?>">
		</div>
	</div>

	<div id="left-side">
		<div class="content-side-wrapper">
			
				<input type="hidden" name="source" value="group">
				<div class="blue-text">
					<table>
						<tr>
							<td colspan="3"><h3><?php _e("Primary Passenger", 'duckjoy');?></h3></td>
						</tr>
						<?php if(!is_user_logged_in()) : ?>
							<tr>
								<td class="text_right"><?php _e("Password", 'duckjoy');?></td>
								<td><input type="password" name="primary[password]"></td>
								<td><?php _e("Confirm Password", 'duckjoy');?></td>
								<td><input type="password" name="primary[cpassword]"></td>
								<td>
                                	<a href="javascript:void(0)" name="icon-pass" class="on-hover-show-help">
                                	<img class="icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/question_icon.jpg" alt="" />
                                    <div class="on-hover-show-help-content">
										<h3><?php _e("Password", 'duckjoy');?></h3>
										<p><?php _e("We need your password so you become a member of this site.", 'duckjoy');?></p>
                                    </div>
                                    </a>
                               </td>
							</tr>
						<?php endif; ?>

						<tr>
							
							<td class="text_right"><?php _e("Surname", 'duckjoy');?></td>
							<td><input name="primary[surname]" type="text" class="required" value="<?php echo $surname; ?>"></td>
							<td><?php _e("Name", 'duckjoy');?></td>
							<td><input name="primary[first_name]" type="text" class="required" value="<?php echo $first_name; ?>"></td>
						</tr>
						<tr>
							<td class="text_right"><?php _e("Date of Birth", 'duckjoy');?></td>
							<?php $days = range(1,31); $months = range(1,12); $years = array_reverse(range(1910, date('Y', time()))); ?>
							<td>
							<select name="primary[dob][year]" id="" class="required">
								<?php foreach ($years as $value) :?>
									<option value="<?php echo $value; ?>" <?php selected($dob['year'], $value, 'selected'); ?> ><?php echo $value; ?>年</option>
								<?php endforeach;?>
							</select>
							</td>
							<td>
							<select name="primary[dob][month]" id="" class="required">
								<?php foreach ($months as $value) :?>
									<option value="<?php echo $value; ?>" <?php selected($dob['month'], $value, 'selected'); ?> ><?php echo $value; ?>月</option>
								<?php endforeach;?>
							</select>
							</td>
							<td>
							<select name="primary[dob][day]" id="" class="required">
								<?php foreach ($days as $value) :?>
									<option value="<?php echo $value; ?>" <?php selected($dob['day'], $value, 'selected'); ?> ><?php echo $value; ?>日</option>
								<?php endforeach;?>
							</select>
							</td>
							<td>
                            	<a href="javascript:void(0)" name="icon-dob" class="on-hover-show-help">
                                		<img class="icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/question_icon.jpg" alt="" />
                                        <div class="on-hover-show-help-content">
                                    	<h3><?php _e("Date of Birth", 'duckjoy');?></h3>
                                        <p><?php _e("We need to know your date of birth in order to book the hotel for you.", 'duckjoy');?></p>
                                    </div>
                                 </a>
                           </td>
						</tr>
							<td class="text_right"><?php _e("Email", 'duckjoy');?></td>
							<td><input type="text" name="primary[email]" class="required" value="<?php echo $email; ?>"></td>
							<td><?php _e("Confirm Email", 'duckjoy');?></td>
							<td><input type="text" name="primary[confirm]" class="required" value="<?php echo $confirm; ?>"></td>
							<td>
                            	<a href="javascript:void(0)" name="icon-email" class="on-hover-show-help">
                                		<img class="icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/question_icon.jpg" alt="" />
                                        <div class="on-hover-show-help-content">
                                    	<h3><?php _e("email pop title", 'duckjoy');?></h3>
                                        <p><?php _e("email pop text.", 'duckjoy');?></p>
                                    </div>
                                 </a>
                           </td>
						</tr>
						<tr>
							<td class="text_right"><?php _e("Passenger Type", 'duckjoy');?></td>
							<td>
							<select name="primary[type_passenger]" id="primary[type]" class="required">
								<option value="adult" <?php selected($type_passenger, 'adult','selected'); ?> ><?php _e("Adult", 'duckjoy'); ?></option>
								<option value="student" <?php selected($type_passenger, 'student','selected'); ?> ><?php _e("Student", 'duckjoy'); ?></option>
								<option value="child"  <?php selected($type_passenger, 'child','selected'); ?> ><?php _e("Child", 'duckjoy'); ?></option>
							</select>
							</td>
							<td><?php _e("Gender", 'duckjoy');?></td>
							<td>
							<select name="primary[gender]" id="" name="primary[gender]">
								<option value="male"><?php _e("Male", 'duckjoy'); ?></option>
								<option value="female"><?php _e("Female", 'duckjoy'); ?></option>
							</select>
							</td>
						<tr>
							<td class="text_right"><?php _e("Mobile", 'duckjoy');?></td>
								<td colspan="3">
                                		<input type="text" name="primary[country_phone_code]" size="4" value="<?php echo $country_phone_code; ?>">
                                		<input type="text" name="primary[phone]" size="25" value="<?php echo $phone; ?>">
                                
								<span class="questionmark">
                                	<a href="javascript:void(0)" name="icon-phone" class="on-hover-show-help">
                                    	<img class="icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/question_icon.jpg" alt="" />
                                        <div class="on-hover-show-help-content">
                                    	<h3><?php _e("Contact Number", 'duckjoy');?></h3>
                                        <p><?php _e("We will only use this if we need to get in touch to help manage your account - never for marketing.", 'duckjoy');?></p>
                                    </div>
                                        </a>
                               </span>
                                        </td>
						</tr>
						<tr>
							<td class="text_right"><?php _e("Address1", 'duckjoy');?></td>
							<td colspan="3"><input type="text" name="primary[address1]" size="50" value="<?php echo $address1; ?>"></td>
						</tr>
						<tr class="text_right">
							<td class="text_right"><?php _e("Address2", 'duckjoy');?></td>
							<td colspan="3"><input type="text" name="primary[address2]" size="50" value="<?php echo $address2; ?>"></td>
						</tr>
						<tr>
							<td class="text_right"><?php _e("Post Code", 'duckjoy');?></td>
							<td><input type="text" name="primary[post_code]" size="10" value="<?php echo $post_code; ?>"></td>
							<td><?php _e("Country", 'duckjoy');?></td> 
							<td><input type="text" name="primary[country]" placeholder="UK" value="<?php echo $country; ?>"></td>
						</tr>
					</table>
				</div> <!-- primary-passenger -->

				
				<!-- passengers -->
				<?php 
					$total_people = $_SESSION['group_form_vars']['doubles_adult'] +
						$_SESSION['group_form_vars']['doubles_student'] + 
						$_SESSION['group_form_vars']['doubles_children'] + 
						$_SESSION['group_form_vars']['singles_adult'] + 
						$_SESSION['group_form_vars']['singles_student'] +   
						$_SESSION['group_form_vars']['singles_children'];

					$locations = $_SESSION['group_form_vars']['locations'];
				?>
				<?php if ($total_people > 1) : ?>
					<?php 
						if (isset ($_POST['passenger']) && count($_POST['passenger']) > 0){
							foreach ($_POST['passenger'] as $k => $v) {
								$passenger[$k] = $_POST['passenger'][$k]; 	
							}
						} else {
							for ($i = 2; $i <= $total_people; $i++){
								$passenger[$i] = array('gender'=>'', 'type_passenger'=>'', 'dob'=>'', 'name'=>'');
							}
						} 
					?>
				<div class="passengers">
					<div>
						<h3><?php _e("Other Passengers", 'duckjoy');?></h3>
					</div>
					
					<div>
						<table>
							<thead class="blue-bar">
								<td></td>
								<td></td>
								<td>性别</td>
								<td>旅客类型</td>
								<td>姓名</td>
								<td>出生日期</td>
							</thead>
								<?php for ($i = 2; $i <= $total_people; $i++):?>
									<tr>
										<td><?php echo __("Passenger ", 'duckjoy').$i;?></td>
										<td></td>
										<td>
											<select name="passenger[<?php echo $i; ?>][gender]" id="">
												<option value="<?php _e("male", 'duckjoy');?>" class="required" <?php selected($passenger[$i]['gender'], 'male', 'selected'); ?> ><?php _e("Male", 'duckjoy');?></option>
												<option value="<?php _e("female", 'duckjoy');?>" class="required" <?php selected($passenger[$i]['gender'], 'female', 'selected'); ?> ><?php _e("Female", 'duckjoy');?></option>
											</select>
										</td>
										<td>
											<select name="passenger[<?php echo $i; ?>][type_passenger]" class="required">
												<option value="adult" <?php selected($passenger[$i]['type_passenger'], 'adult', 'selected'); ?> ><?php _e("Adult", 'duckjoy'); ?></option>
												<option value="student" <?php selected($passenger[$i]['type_passenger'], 'student', 'selected'); ?>><?php _e("Student", 'duckjoy'); ?></option>
												<option value="child" <?php selected($passenger[$i]['type_passenger'], 'child', 'selected'); ?>><?php _e("Child", 'duckjoy'); ?></option>
											</select>
										</td>
										<td><input name="passenger[<?php echo $i; ?>][name]"type="text" value="<?php echo $passenger[$i]['name']; ?>"></td>
										<td><input class="datepickerdob" name="passenger[<?php echo $i; ?>][dob]" type="text" value="<?php echo $passenger[$i]['dob']; ?>"></td>
									</tr>
								<?php endfor; ?>
						</table>
					</div>
				</div> <!-- passengers -->
				<?php endif; ?>
				
				<?php if ($total_people > 1) : ?>
                
                
				<div>
					<h4><?php _e("How many couples traveling", 'duckjoy');?></h4>
					<select name="couples">
						<?php for($i = 0; $i <= 8; $i++): ?>
							<option value="<?php echo $i ?>" <?php selected($couples, $i, 'selected'); ?> ><?php echo $i ?></option>
						<?php endfor; ?>
					</select>
					<a href="javascript:void(0)" name="icon-couples" class="on-hover-show-help">
                    <img class="icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/question_icon.jpg" alt="" />
                    <div class="on-hover-show-help-content">
						<h3><?php _e("Couples popup title");?></h3>
						<p><?php _e("couples popup text");?></p>
					</div>
                    </a>
				</div>
                
                
			<?php endif; ?>

				<div>
					<h4><?php _e("Meeting Time and Venue", 'duckjoy');?></h4>
					<select name="pickup" class="required">
						<option value="0"><?php _e("Please select a meetup point", 'duckjoy');?></option>
						<?php foreach ($locations as $key => $value) : ?>
							<option value="<?php echo $value->pickup_code ?>" <?php selected($pickup, $value->pickup_code, 'selected'); ?> ><?php echo $value->location." @ ". date('H:i', strtotime($value->time)); ?></option>	
						<?php endforeach; ?>
					</select>
				</div>

				<div class="font_grey">
					<h3><?php _e("Visa", 'duckjoy');?></h3>
					<input type="radio" name="visa" class="required" value="has_visa" <?php checked($visa, 'has_visa', 'checked'); ?> > <?php _e("Yes I have Visa", 'duckjoy');?><br/>
					<input type="radio" name="visa" class="required" value="no_visa" <?php checked($visa, 'no_visa', 'checked'); ?> > <?php _e("I need Haloexpress to help me with visa", 'duckjoy');?> 
				</div>

				<div>
					<h3><?php _e("Taxi", 'duckjoy');?></h3>
					<p>
						<?php //_e("If you need taxi to pick you up from the Airport or Hotel, please get in touch with us via phone 018454221321", 'duckjoy');?>
						<?php //_e("or email", 'duckjoy');?> 
						<!-- <a href="" mailto="info@duckjoy.com">info@duckjoy.com</a>  -->
						<?php _e("We'll arrange that for you in advance with a reasonable fee", 'duckjoy');?> 
					</p>
				</div>

				<div>
					<h3><?php _e("Other Requirements", 'duckjoy');?></h3>
					<textarea name="notes" id="" cols="80" rows="5"><?php echo $notes; ?></textarea>
				</div>

				<div class="evidence">
					<input type="checkbox" name="t&amp;c" class="required"> 我已阅读并同意该网站的条款<a href="/t&amp;c">条款</a>
					<!-- <input type="checkbox" name="t&c" class="required"> <?php _e("I read and agree with the", 'duckjoy');?> <a href="/t&c"><?php _e(" terms and conditions ", 'duckjoy');?></a><?php //_e("of this site ", 'duckjoy');?> -->
				</div>

				<div>
					<input type="submit" class="bootstrap-blue" value="<?php _e("submit",'duckjoy');?>" name="group_passenger_info">
					<input type="submit" class="bootstrap-grey" value="<?php _e("back",'duckjoy');?>" onclick="javascript:window.history.back(); return false;">
				</div>
			</form>
		</div> <!-- content-side-wrapper -->
	</div> <!-- left-side -->
	
	

	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>




