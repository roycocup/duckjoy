<?php
/**
 * Template Name: Guides
 * 
 */

get_header(); ?>

<style>
	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo{width:300px !important; height:40px !important;}
	.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
	


	.entry-title{margin:0 !important;}
	#main {width: 100%;background-color: white;float: left;}
	
	#topleft ul li{list-style:none; border-bottom:solid 2px #cccccc; margin:0 0 20px 0; padding:0 0 20px 0; float:left; width:600px;}
	#topleft {width:630px;margin-top:30px;margin-left:40px;float:left; border-right:solid 3px #cccccc; padding:0 30px 0 0;}
	#topleft ul{margin:0 !important; padding:0 !important;}
	#topleft ul li .img {float: left;height: 150px;width: 180px;border: 2px dashed #CCC;}
	#topleft ul li .img img {margin-top: 4px;margin-left: 4px; width:168px; height:138px;}
	#topleft ul li .gongluetitle {height: 30px;width: 410px;position: absolute;color: rgba(102,192,106,1);text-align: left;vertical-align: middle;font-family: "Yuppy SC Regular";margin-left: 195px;font-size: 18px;}
	#topleft ul li .gonglueauthor {height: 20px;width: 410px;position: absolute;text-align: left;margin-top: 25px;vertical-align: middle;padding-top: 2px;margin-left: 195px;font-size: 12px;color: #999999;line-height: 20px;}
	#topleft ul li .gonglueinfo {height: 120px;width: 410px;position: absolute;text-align: justify;margin-top: 60px;vertical-align: middle;margin-left: 195px;font-size: 14px;color: #666666;line-height: 23px;}
	#topleft ul li .gonglueinfo a{color: rgba(102,192,106,1)}
	#topleft ul li .gongluetitle a {color: rgba(102,192,106,1);text-decoration:none;}
	

	#topright {float:right; width:225px; margin: 30px 100px 50px 0; }
	#topright ul{margin:0 !important; padding:0 !important;}
	#topright ul li{list-style:none; margin:0 0 20px 0;}
	#topright ul li img{max-width: 285px; max-height: 200px;}

	.sub-spacer{margin-left: 20px;}

</style>

<div id="primary" class="site-content">
	<div id="content" role="main">
		
		<div id="topleft">

			<?php
				//page content
			$post = get_page(get_the_id()); 
			$title = apply_filters('the_title', $post->post_title); 
			$content = apply_filters('the_content', $post->post_content); 
			?>

			<h1 class='entry-title'> <?php //echo $title; ?> </h1>
			<div class="entry-content"><?php //echo $content; ?></div>

			<?php
				//posts listing
			$category_name = 'Guides';
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$posts_per_page = 10;
			$args = array(
				'post_type' => array('post'),
				'posts_per_page' => $posts_per_page,
				'orderby'=>'date',
				'order'=>'DESC',
				'paged' => $paged,
				'cat' => get_cat_ID($category_name),
				);
			$posts = get_posts($args)
			?>
			<ul>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php foreach ($posts as $post) : ?>
						<?php //the date is only populated once if its exactly the same...ask wordpress... ?>
						<?php $post_date = get_the_date('Y-m-d'); ?>

						<li>
							<div class="img"><a href="<?php echo the_permalink(); ?>"><img src="<?php echo the_field('primary_image'); ?>"></a></div>
							<div class="gongluetitle"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></div>
							<div class="gonglueauthor">
								<div class="subtitle">
									<?php echo "作者："; echo the_field('author'); ?> 
									<?php if(function_exists('the_views')) { echo "<span class='sub-spacer'>阅读： </span>"; the_views();  echo "次";} ?>
									<?php echo "<span class='sub-spacer'>发布时间： </span>". format_date($post_date); ?>
								</div>
							</div>
							<div class="gonglueinfo">
								<?php echo mb_strimwidth(get_the_excerpt(), 0, 200); ?>
								<a href="<?php echo the_permalink(); ?>"><?php _e("...Continues", 'duckjoy');?></a>
							</div>
						</li>

					<?php endforeach; ?>
				<?php endwhile; ?>
			</ul>
			<?php 
			$postsInCat = get_term_by('name',$category_name,'category');
			$total_posts = $postsInCat->count;
			$pages_to_show = ceil($total_posts / $posts_per_page);
			$args = array(
				'base'         => '/'.strtolower($category_name).'/%_%',
				'format'       => '/page/%#%',
				'total'        => $pages_to_show,
				'current'      => max( 1, get_query_var('paged') ),
				'show_all'     => False,
				'end_size'     => 2,
				'mid_size'     => 2,
				'prev_next'    => True,
				'prev_text'    => __('« Previous', 'duckjoy'),
				'next_text'    => __('Next »', 'duckjoy'),
				'type'         => 'plain',
				'add_args'     => False,
				'add_fragment' => ''
				);
			echo paginate_links($args); 
			?>


		</div><!--topleft-->


		<div id="topright">
			<ul>
				<li><a href="http://<?php echo the_field('link_for_ad_1', 12); ?>"><img src="<?php echo the_field('ad_1', 12); ?>"></a></li>
				<li><a href="http://<?php echo the_field('link_for_ad_2', 12); ?>"><img src="<?php echo the_field('ad_2', 12); ?>"></a></li>
				<li><a href="http://<?php echo the_field('link_for_ad_3', 12); ?>"><img src="<?php echo the_field('ad_3', 12); ?>"></a></li>
				<li><a href="http://<?php echo the_field('link_for_ad_4', 12); ?>"><img src="<?php echo the_field('ad_4', 12); ?>"></a></li>
			</ul>
		</div>



	</div><!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>