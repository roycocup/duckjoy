<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php //get_sidebar( 'main' ); ?>

			<img id="footer" class="" src="<?php bloginfo('stylesheet_directory'); ?>/img/footer.png" alt="footer" />

			<div id="footer-info">
				<div id="footer-address">
					Duckjoy Limited <br/>
					2A Compton House, Guildford, GU1 4TX <br>
					Tel: +44 (0)1483 495584<br>
				</div>

				<div id="footer-images">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footerlogo1.png" alt="">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footerlogo2.png" alt="">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footerlogo3.png" alt="">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footerlogo4.png" alt="">
				</div>

				<div id="footer-other-info">
					<a href="/tc">Terms & Conditions</a> and <a href="#"></a>Privacy Policy</a><br>
					Monday - Thursday 9am-9pm Friday - Sunday 10am-6pm <br>
					@2013-2015 大脚鸭旅游网 DUCKJOY LTD | ALL rights reserved<br>
				</div>
			</div>
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>