<?php 
/*
* Template Name: User Dashboard
*/
?>

<?php get_header(); ?>
<?php wp_enqueue_script('jquery-ui-dialog');  ?>

<style>
	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo{width:300px !important; height:40px !important;}
	.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
	#main {width: 100%;background-color: white;float: left; min-height: 400px; font-size: 16px;}


	.title{color:rgb(9,67,166); padding-bottom:30px; margin-top:-10px; border-bottom: 2px solid #ccc; }

	#right {width:285px;margin-top:30px;float:right; margin-right:40px; border: 2px solid #ccc; padding: 10px;}
	#right a, #right a:hover, #right a:link, #right a:visited{ text-decoration: none; color: black;}

	#left {width:630px;margin-top:30px;margin-left:40px;float:left; border-right:solid 3px #cccccc; padding:0 30px 0 0;}

	.column-1{ float: left; text-align: right;}
	.column-2{ float: left; margin-left: 10px;}
	.column-2 input[type='submit'] { background-color: rgb(229,100,86); border-radius: 5px; color:white; box-shadow: none; border:none; height:40px; } 

	.lwa-modal h4, .lwa-modal p{font: inherit;}

	.flashmessages{ float:left; width:945px; margin:40px; padding:20px 40px; background-color: rgb(239,215,217); color: rgb(168,77,62); min-height: 20px;}

	.order{border-bottom: 1px solid #ccc; color: #888;}
	td, table {border: none;}
	
	.order_title{color:rgb(23,172,232); margin-top: 10px;}
	.buttons{float: right;}
	.buttons input{background-color:rgb(23,172,232); color: white; border: none; margin-left: 5px; font-size: 14px;}
	.buttons input:disabled{background-color: #ccc;}

	.rate-pop{display: none;}
	.refund-pop{display: none;}
</style>

<script>
	jQuery(document).ready(function($) {
		
		$('.rate').click(function(event) {
			var order_id = $(this).attr('order_id');
			$('#rate_'+order_id).dialog({
				modal: true,
				width: 800,
    			height: 500,
			});
		});


		$('.refund').click(function(event) {
			var order_id = $(this).attr('order_id');
			$('#refund_'+order_id).dialog({
				modal: true,
				 width: 800,
    			height: 500,
			});
		});

		if ($('.flashmessages').html() !== undefined ){
			setTimeout(function() {
				$('.flashmessages').fadeOut('slow');
			}, 3000);
		}else {
			console.log('nothing there');
		}

	});
		
</script>


<?php 
$orders_model = new Orders_model();
$members_model = new Members_model();
$reviews_model = new Reviews_model();
$emails = new Emails_model();
$user = wp_get_current_user();
$uname = $user->data->display_name;
$email = $user->data->user_email;
$user_id = $user->data->ID;

$member_exists = false;
$member_id = $members_model->getMemberByEmail($email);
if ($member_id){
	$member_exists = true;	
	$orders = $orders_model->getOrdersForMemberId($member_id);
}

?>


<?php  
	if (!empty($_POST)) {
		$messages = array();
		if (!empty($_POST['num_rate']) && !empty($_POST['review']) && !empty($_POST['order_id']) ){
			$id = $reviews_model->save($_POST['order_id'], $user_id, $_POST['num_rate'], $_POST['review']);

			$details = $orders_model->getOrderDetails($_POST['order_id']);
			$primary = $details['primary'];
			$offer = $details['offer'];
			$order = $details['order'];
			$data = array(
				'name' 			=> $primary->surname." ".$primary->first_name,
				'route_name'	=> $offer->post_title,
				'price'			=> $order->price,
				'transaction_code' => $order->paypal_transaction_id,
				'order_id'		=> $order->order_id,
				'order_code'	=> $order->order_code,
				'review'		=> $_POST['review'],
				'rate'			=> $_POST['num_rate'],
			);
			$emails->sendmail($primary->email, __('Review Submitted', 'duckjoy'), Emails_model::TEMPLATE_REVIEW_SUBMITTED, $data);
			$messages[] = __('Your review and rate were submitted.', 'duckjoy');
		}

		if (!empty($_POST['refund'])){
			$details = $orders_model->getOrderDetails($_POST['order_id']);
			$primary = $details['primary'];
			$offer = $details['offer'];
			$order = $details['order'];
			$data = array(
				'name' 			=> $primary->surname." ".$primary->first_name,
				'route_name'	=> $offer->post_title,
				'price'			=> $order->price,
				'transaction_code' => $order->paypal_transaction_id,
				'order_id'		=> $order->order_id,
				'order_code'	=> $order->order_code,
				'explanation'	=> $_POST['refund'],
			);
			//send email
			$emails->sendmail($primary->email, __('Refund Request', 'duckjoy'), Emails_model::TEMPLATE_REFUND_REQUEST_CLIENT, $data);
			$emails->sendmail(get_bloginfo('admin_email'), __('Refund Request', 'duckjoy'), Emails_model::TEMPLATE_REFUND_REQUEST_AGENCY, $data);
			$orders_model->setRefundStatus($order->order_id, 'requested');
			$messages[] = __('A refund has been requested', 'duckjoy');
		}

	}

?>


<div id="primary" class="site-content">
	<div id="content" role="main">
		
		<?php if (!empty($messages)) :?>
			<div class="flashmessages">
				<?php foreach ($messages as $message): ?>
					<li><?php echo $message; ?></li>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		
			
		
			<div id="left">
				<h3 class="title"><?php _e("Booking History", 'duckjoy');?></h3>

				<?php if($member_exists): ?>
					<?php foreach ($orders as $key => $order) :?>
						<?php if ($order->status == Orders_model::STATUS_PENDING) continue; ?>
						<div id="<?php echo $key ?>" class="order">
							<?php $order = $orders_model->getOrderDetails($order->order_id); ?>
							<div class="order_title"><?php echo $order['offer']->post_title; ?></div>
							<div class="buttons">
								<?php 
									$disabled = '';
									$thirty_days_ago = date('Y-m-d', strtotime("-30 days"));
									$bought = $order['order']->return_date;
									if (strtotime($thirty_days_ago) > strtotime($bought)){
										$disabled = 'disabled';
									}
								?>
								<input type="button" <?php echo $disabled; ?> class="rate" order_id="<?php echo $order['order']->order_id; ?>" value="<?php _e("rate", 'duckjoy');?>" >
								<input type="button" <?php echo $disabled; ?> class="refund" order_id="<?php echo $order['order']->order_id; ?>" value="<?php _e("Refund", 'duckjoy');?>" >
							</div>
							<div>
								<table>
									<tr>
										<td><?php _e("Booking Details", 'duckjoy');?></td>
										<td><?php _e("Order number", 'duckjoy');?></td>
										<td><?php echo $order['order']->order_code; ?></td>
										<td><?php _e("Total", 'duckjoy');?> <?php echo $order['order']->price; ?></td>
									</tr>
									<tr>
										<td></td>
										<td><?php _e("Departure", 'duckjoy');?></td>
										<td><?php echo format_date($order['order']->departure_date); ?></td>
									</tr>
									<tr>
										<td></td>
										<td><?php _e("Return", 'duckjoy');?></td>
										<td><?php echo format_date($order['order']->return_date); ?></td>
									</tr>
									<?php  
										$num_adults = $order['order']->doubles_adult + $order['order']->singles_adult;
										$num_students = $order['order']->doubles_student + $order['order']->singles_student;
										$num_children = $order['order']->doubles_children + $order['order']->singles_children;
										$pass_str = '';
										$pass_str .= ($num_adults)?$num_adults.__(" adults", 'duckjoy'):'';
										$pass_str .= ($num_students)?"+".$num_students.__(" students", 'duckjoy'):'';
										$pass_str .= ($num_children)?"+".$num_children.__(" children", 'duckjoy'):'';
									?>
									<tr>
										<td></td>
										<td><?php _e("Passengers", 'duckjoy');?></td>
										<td><?php echo $pass_str; ?></td>
									</tr>
									<?php
										$pass_str = '';
										$pass_str .= ($order['order']->doubles_number)?$order['order']->doubles_number.__(" Double", 'duckjoy'):'';
										$pass_str .= ($order['order']->singles_number)?"+".$order['order']->singles_number.__(" Single", 'duckjoy'):'';
									?>
									<tr>
										<td></td>
										<td><?php _e("Rooms", 'duckjoy');?></td>
										<td><?php echo $pass_str; ?></td>
									</tr>
									<tr>
										<td><?php _e("Book & Status", 'duckjoy');?></td>
										<td><?php echo $order['order']->modified; ?></td>
										<td><?php if ($order['order']->status == Orders_model::STATUS_CONFIRMED) 
											_e("confirmed", 'duckjoy') ;
										else 
											_e("unconfirmed", 'duckjoy') ; ?></td>
									</tr>
								</table>
							</div>
						</div>


						<div id="rate_<?php echo $order['order']->order_id; ?>" class="rate-pop">
							<?php  $old_review = $reviews_model->getReviewsForOrderByUserId($order['order']->order_id,$user_id); ?>
							<form action="" name="rate" method="post">
								<h3><?php _e("Rate this trip", 'duckjoy');?></h3>
								<div class="rate">
									<p><?php _e("Please give a rating from 1 to 10", 'duckjoy');?></p>
									<?php for($i = 1; $i <= 10; $i++): ?>
										<?php echo $i ?> <input type="radio" name="num_rate" value="<?php echo $i; ?>" <?php if(!empty($old_review->rate))checked($old_review->rate, $i); ?> >
									<?php endfor; ?>
								</div>
								<div class="review">
									<p></p>
									<p><?php _e("Tell us about your experience", 'duckjoy');?></p>
									<textarea name="review" class="" cols="80" rows="10"><?php echo ($old_review)? $old_review->review:''; ?></textarea>
								</div>
								<br>
								<input type="hidden" name='messages' value="Rate">
								<input type="hidden" name='order_id' value="<?php echo $order['order']->order_id; ?>">
								<input class="popup-submit" type="submit" value="<?php _e("Submit", 'duckjoy');?>">
							</form>
						</div>

						<div id="refund_<?php echo $order['order']->order_id ?>" class="refund-pop">
							<?php  $old_review = $reviews_model->getReviewsForOrderByUserId($order['order']->order_id,$user_id); ?>
							<form action="" name="rate" method="post">
								<h3><?php _e("Request a Refund",'duckjoy');?></h3>
								
								<div class="refund">
									<p></p>
									<p><?php _e("Please explain the reason for the refund", 'duckjoy');?></p>
									<textarea name="refund" class="" cols="80" rows="10"></textarea>
								</div>
								<p><?php _e("You will receive an email confirming your request. We will be in touch soon.", 'duckjoy');?></p>
								<br>
								<input type="hidden" name='messages' value="Refund">
								<input type="hidden" name='order_id' value="<?php echo $order['order']->order_id; ?>">
								<input class="popup-submit" type="submit" value="<?php _e("Submit", 'duckjoy');?>">
							</form>
						</div>
					<?php endforeach; ?>
				<?php else: ?>
					<h4><?php _e("You have no booking history yet.", 'duckjoy');?></h4>
				<?php endif; ?>
				
			</div> <!-- end left -->


			<div id="right">
				<!--<?php _e("Profile", 'duckjoy');?> <br>-->
				<!--<?php _e("Change Password", 'duckjoy');?> <br>-->
				<a href="<?php echo wp_logout_url() ?>">登出</a><!-- logout -->
			</div>
		

	</div><!-- #content -->
</div><!-- #primary -->



<?php get_footer(); ?>

