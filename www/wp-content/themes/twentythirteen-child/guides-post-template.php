<?php
/**
 * Single Post Template: Guides Post Template
 */

get_header(); ?>

<style>

	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo{width:300px !important; height:40px !important;}
	.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
	li.guides-menu a{background-color: white; color: #f75959 !important;}
	
	
	.topright {float:right; width:225px;margin-top:30px; margin-right:100px; }
	.topright ul{margin:0 !important; padding:0 !important;}
	.topright ul li{list-style:none; margin:0 0 20px 0;}
	.topright ul li img{max-width: 285px; max-height: 200px;}

	#content{overflow: auto;}
	.topleft {width:640px;margin-top:30px;margin-left:40px;float:left; border-right:solid 3px #cccccc; padding:0 30px 0 0; color:#999999;}
		.topleft .wp-post-image, .topleft .excerpt{border: 2px dashed rgb(102,191,127); padding: 10px; width: 602px; }
		.topleft div.entry-title{color: rgb(102,191,127); text-decoration:none; font-size: 24px;}
		.topleft .excerpt{min-height: 20px; margin-top: 40px; margin-bottom: 10px;}
		.topleft .sub-spacer{margin-left: 20px;}
		.topleft .body, .topleft .excerpt {font-size: 14px;}

</style>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<div class="topleft">
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="post-wrapper">
						<?php $post_date = get_the_date('Y-m-d'); ?>
						<img src="<?php echo the_field('secondary_image'); ?>" alt="" class="wp-post-image">
						<?php //echo get_the_post_thumbnail( $post->ID, array(584, 344)); ?> 
						<div class="entry-title"><?php echo $post->post_title; ?></div>
						<div class="subtitle">
							<?php echo "作者："; echo the_field('author'); ?> 
							<?php if(function_exists('the_views')) { echo "<span class='sub-spacer'>阅读： </span>"; the_views();  echo "次";} ?>
							<?php echo "<span class='sub-spacer'>发布时间： </span>". format_date($post_date); ?>
						</div>
						<div class="excerpt"><?php echo $post->post_excerpt; ?></div>
						<div class="body"><p><?php echo wpautop($post->post_content); ?></p></div>
					</div>

				<?php endwhile; ?>

				<div id="sharebutton">
					<!-- JiaThis Button BEGIN -->
					<div class="jiathis_style"><span class="jiathis_txt">分享到：</span>
						<a class="jiathis_button_tsina"></a>
						<a class="jiathis_button_tqq"></a>
						<a class="jiathis_button_weixin"></a>
						<a class="jiathis_button_renren"></a>
						<a class="jiathis_button_douban"></a>
						<a class="jiathis_button_fb"></a>
						<a href="http://www.jiathis.com/share" class="jiathis jiathis_txt jiathis_separator jtico jtico_jiathis" target="_blank"></a>
						<a class="jiathis_counter_style"></a>
					</div>
					<script type="text/javascript" >
						var jiathis_config={
							summary:"",
							shortUrl:true,
							hideMore:false
						}
					</script>
					<script type="text/javascript" src="http://v3.jiathis.com/code/jia.js" charset="utf-8"></script>
					<!-- JiaThis Button END -->
					
					<div id="articlelinks"></div>
					<div id="comments"></div>
					<div id="uyan_frame"></div>
					<script type="text/javascript" src="http://v2.uyan.cc/code/uyan.js"></script>
				</div> <!--end of sharebutton-->




			</div>


			<div class="topright">
				<ul>
					<li><a href="http://<?php echo the_field('link_for_ad_1', 12); ?>"><img src="<?php echo the_field('ad_1', 12); ?>"></a></li>
					<li><a href="http://<?php echo the_field('link_for_ad_2', 12); ?>"><img src="<?php echo the_field('ad_2', 12); ?>"></a></li>
					<li><a href="http://<?php echo the_field('link_for_ad_3', 12); ?>"><img src="<?php echo the_field('ad_3', 12); ?>"></a></li>
					<li><a href="http://<?php echo the_field('link_for_ad_4', 12); ?>"><img src="<?php echo the_field('ad_4', 12); ?>"></a></li>
				</ul>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>