<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->


	<!-- custom js -->
	<!-- <script type="text/javascript" src="js/xiaobiantuijian.js"></script> -->
	<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<noscript>
		<style>
			body{
				content : none;
			}
		</style>
		<h1>Please enable javascript to see this website</h1>
		<h1>看到这个网站，请启用javascript。转到<a href="http://enable-javascript.com">enable-javascript.com</a>，并按照指示进行。</h1>
	</noscript>
	<div id="page" class="hfeed site">
	
		<header id="masthead" class="site-header" role="banner">

			<div class="site-branding">
				<div id="weibobutton"><wb:follow-button uid="3202270263" type="gray_2" width="125" height="30" ></wb:follow-button></div> 
				<form action="/search" id="search-form">
					<div id="searchbox"><input type="text" placeholder="搜索资讯、攻略" name="search" /></div>
					<div id='magnifier_glass'>
						<a href="javascript:void(0)" onclick="submit();">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/img/magnifier.png" alt="search" />
						</a>	
					</div>
				</form>
				
				<!-- <div id="login"><a href="<?php echo wp_login_url(get_permalink()); ?> ">登录</a>  |  <a href="<?php echo esc_url('/wp-login.php?action=register&redirect_to=' . get_permalink() ); ?>">注册</a></div> -->
				<?php if(!is_user_logged_in()): ?>
					<div id="login"><a href="/log-in ">登录</a>  |  <a href="/register">注册</a></div>
				<?php else: ?>
					<div id="login"><a href="<?php echo wp_logout_url() ?>">登出</a> |  <a href="/udashboard">我的帐户</a></div>
				<?php endif; ?>

			</div>

			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<?php wp_nav_menu( array( 'menu_class' => 'nav-menu', 'container_class'=>'menu-container') ); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->

		</header><!-- #masthead -->

		<div id="main" class="site-main">
			<?php flashMessagesDisplay(); ?>
