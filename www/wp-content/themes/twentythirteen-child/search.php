<?php
/*
Template Name: Search Page
*/

get_header(); ?>


<style>
	#login{font-size:16px !important; color:#fff !important;}
	#magnifier_glass{float:left;}
	.menu-logo{width:300px !important; height:40px !important;}
	.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
	.menu-logo a:hover{border:none !important; display:block;}
	#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
	#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
	#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
	#main {width: 100%;background-color: white;float: left; min-height: 400px; font-size: 16px;}
</style>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php $search_str = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_SPECIAL_CHARS);?>


			<?php //if the string is empty ?>
			<?php if (empty($search_str)): ?>
				<div class="page-content">
					<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with different keywords.', 'duckjoy' ); ?></p>
					<!-- <form role="search" method="get" class="search-form" action="/">
						<label>
							<span class="screen-reader-text"><?php _e("Search for:");?></span>
							<input type="search" class="search-field" placeholder="Search …" value="" name="search" title="Search for:">
						</label>
						<input type="submit" class="search-submit" value="Search">
					</form> -->

				</div>
			<?php else: ?>
		
				<?php 

					//break the search term into its words
					$terms = explode(" ", $search_str);

					//why is this here? Because the wp_query is case sensitive for some reason and I dont want to modify its 
					// query directly or it will be overwritten when we update the system. 
					global $wpdb; 
					$sql = "SELECT wp_posts.ID FROM wp_posts  
							WHERE 1=1  ";
					foreach ($terms as $term) {
						$sql .="AND (((lower(wp_posts.post_title) LIKE '%{$term}%') OR (lower(wp_posts.post_content) LIKE '%{$term}%'))) ";
					}
					$sql .="AND wp_posts.post_type = 'post' 
							AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'private')  
							ORDER BY lower(wp_posts.post_title) LIKE '%lower(vila)%' DESC, 
							wp_posts.post_date DESC ";

					$posts = $wpdb->get_results($sql);

					if (empty($posts)): ?>
						<div class="page-content">
							<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with different keywords.', 'duckjoy' ); ?></p>
							<!-- <form role="search" method="get" class="search-form" action="/search">
								<label>
									<span class="screen-reader-text"><?php _e("Search for:");?></span>
									<input type="search" class="search-field" placeholder="Search …" value="" name="search" title="Search for:">
								</label>
								<input type="submit" class="search-submit" value="Search">
							</form> -->

						</div>
					<?php endif?>

					<style>
						.page-content{overflow: auto;}
						/* Comments - START */
						ul.comments { list-style: none; margin: 0; padding: 0; border-bottom: 1px solid #ddd; float: left; width: 100%; }
						ul.comments li.comment { padding: 10px 0 30px 200px; margin: 0; float: left; width: 100%; display: block; clear: both; border-top: 1px solid #ddd; position: relative; }
						ul.comments li.alt { background: #fafafa; }

						ul.comments li .meta_data {margin-left: -180px; padding-left: 70px; display: block; clear: both; position: absolute;}
						ul.comments li .meta_data .info { margin: 0; font-size: 1.0em; margin-top: 7px;}
						ul.comments li .meta_data img { margin-left: -70px; float: left; }

						ul.comments li .copy { margin: 0; margin-left:100px; color: #444; width: 500px;}
						ul.comments li .copy .title { font-size: 1.4em; font-weight: bold; }
						ul.comments li .copy .title a{color: rgba(102,192,106,1)}
						ul.comments li .number { position: absolute; right: 10px; top: -30px; color: #eee; font-size: 8em; font-weight: bold; font-family: Helvetica, Arial, sans-serif; }

						/* Comments - END */
					</style>

					<div class="page-content">
						<ul class="comments" id="comments">
						<?php $i = 1; ?>
						<?php foreach ($posts as $post) : ?>
							<?php $post = get_post( $post->ID, 'OBJECT' ); ?>
								
								<?php $post_thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID )); ?>
								<?php $post_thumbnail = get_the_post_thumbnail($post->ID, array(60,60)); ?>
								
								<!-- <a href="<?php echo $post->guid; ?> "><?php echo $post->post_title; ?></a> -->
								
									<li class="comment alt" id="comment_7806">
										<a name="comment_7806"></a>
										<div class="number"><?php echo $i; ?></div>
										<div class="meta_data">
											<p class="info"><?php echo the_field('author', $post->ID); ?><br><?php echo format_date($post->post_date); ?></p>
										</div>
										<div class="copy">
											<p class="title"><a href="#" rel="nofollow"><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></p>
											<!-- <p><?php echo wp_trim_words( $post->post_content, 40, '<a href="'. get_permalink($post->ID) .'"> ...<?php _e("Read More", "duckjoy");?></a>' ); ?></p> -->
											<p><?php echo wpautop(mb_substr($post->post_excerpt, 0, 90)); ?><a href="<?php echo get_permalink($post->ID);?>"> ...<?php _e("Read More", "duckjoy");?></a></p>
										</div>
									</li>
								<?php $i++; ?>
						<?php endforeach; ?>
						</ul>

						<div class="clearfix"></div>

					</div>


			<?php endif; ?> 

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>