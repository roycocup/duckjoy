<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	
	

	<!--Include CSS-->
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/tickets.css" rel="stylesheet" type="text/css" />

	<style>
		#Wrapper #container {
			background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/tickets_bg.jpg');
		}
		#Wrapper #container #main {
			background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/tickets_bg.jpg');
		}
		#Wrapper #container #main #topright {
			background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/tickets_graphic.png');
		}
		#Wrapper #container #main #footertext{
			background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/tickets_bg.jpg');
		}
		
	</style>
    


	<link rel="stylesheet" type="text/css" href="http://static.dohop.com/css/styles.css?v=41261" media="screen">


	<!--Include js-->
	<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
      function dohop_source(request,response){
        $.ajax({
          jsonp:"cb",
          url: "http://picker.dohop.com/search/?lang=zh&sid=completer",
          dataType: "jsonp",
          data: {m: 10,input: request.term},
          success: function(data) {
            response($.map(data.standard, function(item) {
              return {label: item.p + "("+item.ac+") "+item.c,value: item.p + "("+item.ac+") " +item.c};
            }));
          }
         });
      }
      $(document).ready(
        function(){
          $("#a1").autocomplete({source: dohop_source,minLength: 2});
          $("#a2").autocomplete({source: dohop_source,minLength: 2});
          $("#departDate").datepicker({numberOfMonths: 1,showButtonPanel: true, dateFormat: "dd.mm.yy",minDate: new Date(),
            onSelect:function(text,inst){
              $("#returnDate").datepicker("option","minDate",$("#departDate").datepicker("getDate"));
            }
          });
          $("#returnDate").datepicker({numberOfMonths: 1,showButtonPanel: true, dateFormat: "dd.mm.yy",minDate: new Date()});
        }
      );
    </script>
    
    <script type="text/javascript">
		var dohop_userData = {"language": "zh", "countrycode": "GB", "lastsearch": "a1=LHR,LGW,LCY,STN,LTN&a2=CDG,ORY,BVA,XCR&d1=271113&d2=291113", "currency": "GBP", "cdate": "1384130678", "userlevel": 0};
		var rtl = false;
		
	function init() {
		dohop.whitelabel = $.extend({
	
	
	
	
	
	preview: false,
	secondary_site:true,
	r:11018,
	wl:1,
	code:'main',
	localVendors:{},
	airlineFilter:"main",
	showtickers:false,
	smallmarkeroverlay:true,	
	hidedatematrix:true,
	showSelfConnect:true,
	transfertemplatedir: 'g'
}, {})
;
		rm.dateformat = "%y.%m.%d";
		
		dohop.init("",1,0, {"i":"LUN"},
			{"i":"CDG"},
			new Date(2014,1-1, 29,12),
			null,'None');

		initClicktripz({formId: 'flightform', label: 'Duckjoy_flights'});

					
	}	

	</script>
    
<?php //wp_head(); ?>

</head>
<body ontouchstart="void(0);" id="front" onLoad="init();"  <?php body_class(); ?>>
	<div id="Wrapper">
		<div id="container">
			<div id="main">
				<div id="header">

					<div id="nav1">
						<?php wp_nav_menu( array( 'menu_class' => 'nav-menu', 'container_class'=>'menu-container') ); ?>
					</div><!--end of nav1-->

					<!-- <div id="login"><a href="<?php echo wp_login_url(get_permalink()); ?> ">登录</a>  |  <a href="<?php echo esc_url('/wp-login.php?action=register&redirect_to=' . get_permalink() ); ?>">注册</a></div> -->
					<?php if(!is_user_logged_in()): ?>
						<div id="login"><a href="/log-in ">登录</a>  |  <a href="/register">注册</a></div>
					<?php else: ?>
						<div id="login"><a href="<?php echo wp_logout_url() ?>">登出</a> |  <a href="/udashboard">我的帐户</a></div>
					<?php endif; ?>


					<div id="weibobutton"><wb:follow-button uid="3202270263" type="gray_2" width="125" height="30" ></wb:follow-button></div>
					<div id="searchbox"><input type="text" placeholder="搜索资讯、攻略" name="s" size="17" style="font-size:12px;" /><img name="magnifier" src="<?php echo get_stylesheet_directory_uri();?>/img/tickets_magnifier.png" style="margin-left:150px;margin-top:-30px;"/></div>
				</div><!--end of header-->

				