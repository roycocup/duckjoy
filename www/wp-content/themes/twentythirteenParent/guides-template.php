<?php
/**
 * Template Name: Guides
 * 
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">
			<?php
				//page content
				$post = get_page(get_the_id()); 
				$title = apply_filters('the_title', $post->post_title); 
				$content = apply_filters('the_content', $post->post_content); 
			?>

				<h1 class='entry-title'> <?php echo $title; ?> </h1>
				<div class="entry-content"><?php echo $content; ?></div>

			<?php
				//posts listing
				$category_name = 'Guides';
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$posts_per_page = 10;
				$args = array(
					'post_type' => array('post'),
					'posts_per_page' => $posts_per_page,
					'orderby'=>'date',
					'order'=>'DESC',
					'paged' => $paged,
					'cat' => get_cat_ID($category_name),
				);
				$posts = get_posts($args)
			?>
			
			<?php while ( have_posts() ) : the_post(); ?>
				<?php foreach ($posts as $post) : ?>
					<?php $post_date = get_the_date('F j,Y'); ?>
					<li class="post-list">
						<a href="<?php the_permalink(); ?>"><?php the_title() ?></a> <?php echo $post_date; ?>
						<!--<span>viewed </span> <?php if(function_exists('the_views')) { the_views(); } ?><span> times</span>-->
						<p><?php the_excerpt(); ?></p>
						<?php wp_reset_postdata(); ?>
					</li>
				<?php endforeach; ?>
			<?php endwhile; ?>

			<?php 
				$postsInCat = get_term_by('name',$category_name,'category');
				$total_posts = $postsInCat->count;
				$pages_to_show = ceil($total_posts / $posts_per_page);
				$args = array(
					'base'         => '/'.strtolower($category_name).'/%_%',
					'format'       => '/page/%#%',
					'total'        => $pages_to_show,
					'current'      => max( 1, get_query_var('paged') ),
					'show_all'     => False,
					'end_size'     => 2,
					'mid_size'     => 2,
					'prev_next'    => True,
					'prev_text'    => __('« Previous'),
					'next_text'    => __('Next »'),
					'type'         => 'plain',
					'add_args'     => False,
					'add_fragment' => ''
				);
				echo paginate_links($args); 
			?>
			
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>