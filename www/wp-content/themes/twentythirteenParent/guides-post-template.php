<?php
/**
 * Single Post Template: Guides Post Template
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				
				<div class="post-wrapper">
					<?php echo get_the_post_thumbnail( $post->ID, array(172,142)); ?> 
					<h1 class="entry-title"><?php echo $post->post_title; ?></h1>
					<div class="subtitle">
						<?php echo get_the_author(); ?> 
						<?php if(function_exists('the_views')) { echo " | viewed "; the_views();  echo " times";} ?>
						<?php echo " | "; the_date(); ?>
					</div>
					<div class=""><p><?php echo $post->post_content; ?></p></div>
				</div>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>