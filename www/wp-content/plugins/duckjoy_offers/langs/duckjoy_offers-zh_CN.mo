��            )         �     �     �     �     �     �  3   �  /   %     U     j     q     u  )   y     �  "   �     �  2   �  1   )     [  .   h     �  &   �  '   �     �  .     )   J  )   t  ,   �  &   �  ]   �  X   P  �  �     {     �     �     �     �  *   �  -   �     	     (	     /	     6	  !   =	     _	     {	     �	  !   �	  9   �	     
     
     ;
     N
     j
     �
  $   �
     �
     �
     �
       /   +  (   [                                                                                                         
      	                          Results 1-2 days 3-5 days 6-9 days A phone number is required. At least one adult or student needs to be selected. Children can not be without at least one adult. Emails do not match. Filter Max Min Must agree with the terms and conditions. No previous dates entered yet. No previous locations entered yet. No results found No room selected. Please select at least one room. Other passengers need a name and a date of birth. Over 10 days Password should be at least 6 characters long. Passwords do not match. Please fill your country of residence. Please fill your first line of address. Please select a visa option. Primary passenger does not have a valid email. Primary passenger name must not be empty. Primary passenger needs to confirm email. Primary passenger surname must not be empty. Primary phone number must be a number. Too many people for the number of double rooms. Each double room can only have 1 or 2 people. Too many people for the number of single rooms. Each single room can only have 1 person. Project-Id-Version: 
POT-Creation-Date: 2014-01-12 17:10-0000
PO-Revision-Date: 2014-01-12 17:36-0000
Last-Translator: Rodrigo Dias <Rodrigo@rodderscode.co.uk>
Language-Team: 
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: __;_e;gettext_noop
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: /Users/rodrigodias/sites/duckjoy/www/wp-content/plugins/duckjoy_offers
 结果 1-2天 3-5天 6-9天 请输入您的电话号码。 请您至少选择一名成人或学生。 儿童应至少由一名成人陪同参团。 邮箱地址不一致。 筛选 最多 最少 您需要同意本站的条款。 之前没有输入日期。 之前没有输入地点。 没有符合条件的结果。 请您至少选择一间客房。 请您完整填写每个旅客的姓名和出生日期。 10天以上 密码至少应为6位数。 密码不一致。 请输入您的居住地。 请输入您的地址。 请您选择签证选项。 请您输入正确的邮箱地址。 请输入您的名。 请您确认邮箱地址。 请输入您的姓。 电话号码必须为数字。 每间双人房只能入住1名或2名旅客。 每间单人房只能入住1名旅客。 