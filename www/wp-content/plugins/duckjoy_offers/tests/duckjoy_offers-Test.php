<?php
/**
 * Offers Tests
 */

// require_once('user_class.php');

class duck_offersTest extends WP_UnitTestCase {
	public $plugin_slug = 'duck_offers';
	public $offers; 
	public $post_id;

	public function setUp() {
		parent::setUp();
		$this->offers = new DuckjoyOffers();
		$this->post_id 				= $this->createPost();
		$_POST['offer_route_name'] 	= 'Test_route';
		$_POST['offer_code']		= 'DJ345';
		$_POST['stars']				= '3';
		$_POST['transports']		= 'bus, car, plane';
		$_POST['length']			= '5';		
		$_POST['start_date']		= '31-12-2013';
		$_POST['end_date']			= '01-05-2013';
		$_POST['itinerary']			= 'Porto, Madrid, Lisboa, Seixal, Casablanca (Morocco)';
		$_POST['prices']			= array('350', '250', '150', '35', '25');
		$_POST['info_boxes']		= array('text for 1', 'text for 2', 'text for 3', 'text for 4');
		$this->time = '123213424';
		$_POST['availability_departure'][$this->time] = '01-03-2014';
		$_POST['availability_return'][$this->time] = '01-03-2014';
		$_POST['status'][$this->time] = true;
	}

	public function createPost($post = array()){
		$default = array(
			'post_title'    => 'My post',
			'post_content'  => 'This is my post.',
			'post_status'   => 'publish',
			'post_author'   => 1,
		);

		$wp_error = '';
		$id = wp_insert_post(array_merge($default, $post), $wp_error);
		$this->assertTrue(empty($wp_error));
		return $id;
	}


	public function testIsObject(){
		$this->assertTrue(true, is_object($this->offers) );
	}

	public function test_object_should_be_type_duckjoyOffers(){
		$this->assertEquals(get_class($this->offers), 'DuckjoyOffers');
	}

	public function test_save_start_date_can_save_null(){
		$key = 'start_date';
		$post_field = null; 
		$this->offers->sanitize_text_field($post_field);
		$res = $this->offers->save_meta($this->post_id, $key, $post_field);
		//a number is its new, true if updated, false if failed
		$this->assertEquals($res, true);
		$metadata = get_post_meta( $this->post_id, $key, true );
		$this->assertEquals('', $metadata);
	}


	public function test_save_itinerary_meta_only_with_data(){		
		$key = 'itinerary';
		$post_field = 'north pole, canada, porto (portugal)'; 
		$this->offers->sanitize_text_field($post_field);
		$res = $this->offers->save_meta($this->post_id, $key, $post_field);
		$this->assertEquals($res, true);
	}


	public function test_save_all_metas_null(){
		$keys = array('offer_route_name', 'offer_code', 'stars', 'transports', 'start_date', 'end_date', 'length');
		$value = null;
		foreach($keys as $key){
			$this->offers->save_meta($this->post_id, $key, $value);
			$metadata = get_post_meta( $this->post_id, $key, true );
			$this->assertTrue(empty($metadata));
		}
	}


	public function test_save_availability_boxes_saves_ok(){
		global $wpdb;
		$_POST['availability_departure'][$this->time] = '12-01-2014';
		$_POST['availability_return'][$this->time] = '17-01-2014';
		$_POST['status'][$this->time] = false;
		$this->offers->save_availability_boxes($this->post_id);
		$saved = $wpdb->get_results("SELECT * FROM wp_duck_availability where availability_code = $this->time");
		$expected = '2014-01-12 00:00:00';
		$this->assertEquals($saved[0]->departure,  $expected);
		$expected = '2014-01-17 00:00:00';
		$this->assertEquals($saved[0]->return,  $expected);
		
		//is status empty?
		$this->assertTrue(empty($saved[0]->status));
		//is it int
		$this->assertEquals(($saved[0]->status), 0);
	}

	public function test_save_availability_boxes_status_true(){
		global $wpdb;
		$_POST['availability_departure'][$this->time] = '12-01-2014';
		$_POST['availability_return'][$this->time] = '17-01-2014';
		$_POST['status'][$this->time] = true;
		$this->offers->save_availability_boxes($this->post_id);
		$saved = $wpdb->get_results("SELECT * FROM wp_duck_availability where availability_code = $this->time");
		//is status empty?
		$this->assertTrue(!empty($saved[0]->status));
		//not null
		$this->assertNotNull($saved[0]->status);	
		//is it int
		$this->assertEquals($saved[0]->status, 1);

	}


	public function test_frontend_page(){
		
	}




}
