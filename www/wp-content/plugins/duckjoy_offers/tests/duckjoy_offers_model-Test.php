<?php
/**
 * Offers Tests
 */

// require_once('user_class.php');
class duck_offers_modelTest extends WP_UnitTestCase {
	public $plugin_slug = 'duck_offers';
	public $offers; 
	public $post_id;

	public function setUp() {
		parent::setUp();
		$this->offers = new DuckjoyOffers();
	}


	//this is what the search results calls
	//returns array of posts 
	public function test_get_posts_with_origin_and_destination(){
		$posts = $this->offers->model->get_posts_with_origin_and_destination($this->offers->post_type, 'london', 'frankfurt');

		$this->assertTrue(is_array($posts));

		$frankfurt = false;
		foreach ($posts as $post) {
			$all_meta = Offers_model::get_meta_for_post($post);
			if (stripos($all_meta['itinerary'][0], 'frankfurt') !== false) $frankfurt = true;;
		}
		//must find the word frankfurt in the array
		$this->assertTrue($frankfurt);

	}

	//this is what ajax calls
	//returns an array of cities
	public function test_get_available_destinations_for_origin(){
		$result = $this->offers->model->get_available_destinations_for_origin('london');

		//resturns an array
		$this->assertTrue(is_array($result));

		$frankfurt = false;
		foreach ($result[0] as $result_obj) {
			if ($result_obj->place_name == 'frankfurt') $frankfurt = true;
		}
		//must find the word frankfurt in the array
		$this->assertTrue($frankfurt);

	}

}
