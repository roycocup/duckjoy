<?php
// Load the test environment
// https://github.com/nb/wordpress-tests

$path = '/Applications/XAMPP/xamppfiles/htdocs/duckjoy/wordpress-tests/bootstrap.php';

if (file_exists($path)) {
        $GLOBALS['wp_tests_options'] = array(
                'active_plugins' => array('duckjoy_offers/duckjoy_offers.php')
        );
        require_once $path;
} else {
        exit("Couldn't find wordpress-tests/bootstrap.php");
}
