<?php

/**
*
* Plugin Name: Duckjoy Offers
* Description: A custom built plugin to enable holiday packages to be created and displayed as a list of own entities in wordpress
* Provides: Duckjoy-Sys
* Depends: duck_db
* Author: Rodrigo Dias
* Version: 1.0
* Author URI: http://rodderscode.co.uk
*
**/


// How to add a subpage to custom post type 
//add_action('admin_menu', function() { remove_meta_box('pageparentdiv', 'chapter', 'normal');});
//   add_action('add_meta_boxes', function() { add_meta_box('chapter-parent', 'Part', 'chapter_attributes_meta_box', 'chapter', 'side', 'high');});
//   function chapter_attributes_meta_box($post) {
//     $post_type_object = get_post_type_object($post->post_type);
//     if ( $post_type_object->hierarchical ) {
//       $pages = wp_dropdown_pages(array('post_type' => 'part', 'selected' => $post->post_parent, 'name' => 'parent_id', 'show_option_none' => __('(no parent)'), 'sort_column'=> 'menu_order, post_title', 'echo' => 0));
//       if ( ! empty($pages) ) {
//         echo $pages;
//       } // end empty pages check
//     } // end hierarchical check.
//   }

require_once(dirname(__FILE__)."/helpers.php");
require_once(dirname(__FILE__)."/models/offers.php");
require_once(dirname(dirname(__FILE__))."/duckjoy_orders/models/orders.php");

add_action('init', 'duckoffers_init');
function duckoffers_init(){
	load_plugin_textdomain('duckjoy_offers', false, dirname( plugin_basename( __FILE__ ) ) . '/langs/');
	new DuckjoyOffers();
}


class DuckjoyOffers {

	public $post_type = 'compares';
	const PLUGIN_NAME = 'compares';

	public $itinerary_table 	= 'wp_duck_itinerary';
	public $countries_table 	= 'wp_duck_countries';
	public $availability_table 	= 'wp_duck_availability';
	public $cities_table		= 'wp_duck_cities';
	public $pickup_locations_table		= 'wp_duck_pickup_locations';

	public function __construct(){
		//start the session variables for multiple pages
		//if(!isset($_SESSION)) session_start();

		//taxonomies and metaboxes
		$this->register_post_type();
		$this->taxonomies();
		add_action( 'add_meta_boxes', array( $this, 'meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save' ) );
		add_action( 'admin_menu', array( $this, 'remove_metaboxes' ));

		//ajax calls
		if ( is_admin() ){
			add_action("wp_ajax_duck_ajax_getvars", array($this, "duck_ajax_getvars"));
			add_action("wp_ajax_nopriv_duck_ajax_getvars", array($this, 'duck_ajax_getvars'));
		}

		//page handling
		add_filter('wp', array( $this, 'process_order_info'));
		add_filter('wp', array( $this, 'process_passenger_info'));
		add_filter('the_content', array( $this, 'search'));

		//extra script		
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
		wp_enqueue_script( 'datetime_picker', get_stylesheet_directory_uri()."/js/jquery-ui-timepicker-addon.js", 'duck_offers', '0.1', true );
		wp_enqueue_style( 'datetime_picker_css', get_stylesheet_directory_uri()."/css/jquery_datetime_picker.css", array('jquery-ui-datepicker'));
		wp_register_script( 'duck_offers', get_stylesheet_directory_uri() . '/js/offers.js', array('jquery-ui-datepicker'), null, true ); // loaded at footer
		wp_enqueue_script( 'duck_offers' );

		$this->model = new Offers_model();
		$this->reviews = new Reviews_model();
	}


	/*============================
	=            Ajax            =
	============================*/
	
	
	/**
	* Endpoint for all ajax calls
	*/
	public function duck_ajax_getvars(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$function_name = @$_REQUEST['function_name'];
			$parameters = @$_REQUEST['parameters'];

			if (!empty($parameters)) {
				$result = $this->$function_name($parameters);	
			} else {
				$result = $this->$function_name();	
			}
			echo json_encode($result);
			exit();
		}
	}


	public function get_available_destinations($origin_value = '') {
		$csv_results = '';
		if (!empty($origin_value)){
			$results = $this->model->get_available_destinations_for_origin($origin_value);
			if (!empty($results)) {
				foreach ($results[0] as $value) {
					$array_results[] = $value->place_name;
				}
				return $array_results;
			}
		}
	}



	/**
	*
	* Processes the search and results pages
	*
	**/
	public function search($content){
		if (!is_page_template('compare-template.php')){
			return $content;
		}

		//If nothing was inserted - for the search page when its empty
		if (empty($_REQUEST['destination']) && empty($_REQUEST['after']) && empty($_REQUEST['until']) ){
			include('views/search-page.php');
			return $content; 	
		}


		/*==========  search results  ==========*/
		
		//get the search terms
		if (!empty($_REQUEST['destination'])) $destination = sanitize_text_field($_REQUEST['destination']);
		$args['destination'] 	= !empty($destination) ? $destination : '';		
		$args['after'] 			= !empty($_REQUEST['after'])? $_REQUEST['after']:'';
		$args['until'] 			= !empty($_REQUEST['until'])? $_REQUEST['until']:'';

		$filters = array();

		//lenght of journey (days)
		if (!empty($_REQUEST['days'])) {

			foreach ($_REQUEST['days'] as $day) {
				if ($day == 1) {
					$filters['min_length'] = 1;
					$filters['max_length'] = 2;
				}
				if ($day == 3) {
					if (empty($filters['min_length'])) $filters['min_length'] = 3;
					$filters['max_length'] = 5;
				}
				if ($day == 6) {
					if (empty($filters['min_length'])) $filters['min_length'] = 6;
					$filters['max_length'] = 9;
				}
				if ($day == 10) {
					//$filters['min_length'] = 10;
					if (empty($filters['min_length'])) $filters['min_length'] = 10;
					unset($filters['max_length']);
				}
			}
			
		}

		if (!empty($_REQUEST['company'])) $filters['company'] 			= $_REQUEST['company'];
		if (!empty($_REQUEST['stars'])) $filters['stars'] 				= $_REQUEST['stars'];
		if (!empty($_REQUEST['transports'])) $filters['transports'] 	= $_REQUEST['transports'];
		if (!empty($_REQUEST['guest_score'])) $filters['guest_score'] 	= $_REQUEST['guest_score'];
		if (!empty($_REQUEST['min_price'])) $filters['min_price'] 		= $_REQUEST['min_price'];
		if (!empty($_REQUEST['max_price'])) $filters['max_price'] 		= $_REQUEST['max_price'];
		if (!empty($_REQUEST['rating'])) $filters['rating'] 			= $_REQUEST['rating'];
		

		$offers_ids = array();

		//destination, until and after
		$offers_ids = $this->model->get_posts_for_search($this->post_type, $args);

		//this bit is a HOG of performance. Needs optimizing
		$companies = $this->model->get_all_companies();

		//all of the filters go here
		if (!empty($filters))
			$offers_ids = $this->model->filter_post_ids_by_args($offers_ids, $filters); 
		
		

		$num_results = count($offers_ids);
		//get the actual posts and add extra data to them
		$offers = array();
		foreach ($offers_ids as $value) {
			$offer = $this->model->get_post_by_id($value->ID);
			$offer = $this->model->add_data_to_post($offer, array('company'));
			$offers[] = $offer;
		}

		//set the content to be output by the template
		//$content = $this->view->get_markup_for_offers($offers);
		$posts_per_page = 7;
		include('views/search-results-page.php');
		
			
	}


	/*==================================================
	=            Forms and pages Processing            =
	==================================================*/
	
	
	public function process_passenger_info($content){
		
		if ( !is_single() ){
			return false;  
		}

		//this form coming back?
		if (isset($_POST['compare_passenger_info'])){

			//validate the nonce from passenger info
			if (!wp_verify_nonce($_POST['nonce'], 'offer_passenger_info')){
				error_log(__FILE__ . ": Failed the nonce.". __LINE__);
				debug_log('failed nonce on '. __FILE__.":".__LINE__);
				die();
			}
			
			//validate the form posts
			if (!is_user_logged_in()){

				//the passwords only have to be validated if the user decides to cretae a pasword.
				//Users can buy without being registered as members
				if (!empty($_POST['primary']['password']) ||  !empty($_POST['primary']['cpassword'])) {
					//passwords must match
					if ($_POST['primary']['password'] !== $_POST['primary']['cpassword']){
						$_POST['messages'][] = __("Passwords do not match.", 'duckjoy_offers');	
					}
					//passwords must be bigger than 6 characters
					if (strlen($_POST['primary']['password']) < 6){
						$_POST['messages'][] = __("Password should be at least 6 characters long.", 'duckjoy_offers');	
					}	
				}
			}
			
			//primary title must not be empty
			// if (empty($_POST['primary']['title']) ){
			// 	$_POST['messages'][] = __("Primary passenger title must not be empty.", 'duckjoy_offers');	
			// }
			//primary surname must not be empty
			if (empty($_POST['primary']['surname']) ){
				$_POST['messages'][] = __("Primary passenger surname must not be empty.", 'duckjoy_offers');	
			}
			//primary first_name must not be empty
			if (empty($_POST['primary']['first_name']) ){
				$_POST['messages'][] = __("Primary passenger name must not be empty.", 'duckjoy_offers');	
			}
			//primary dob must not be empty
			//primary email must not be empty
			//primary email must be valid email
			if (!is_email($_POST['primary']['email']) ){
				$_POST['messages'][] = __("Primary passenger does not have a valid email.", 'duckjoy_offers');		
			}

			//confirm email must not be empty
			if (empty($_POST['primary']['confirm'])){
				$_POST['messages'][] = __("Primary passenger needs to confirm email.", 'duckjoy_offers');		
			}

			//primary email must match 
			if ($_POST['primary']['email'] !== $_POST['primary']['confirm']){
				$_POST['messages'][] = __("Emails do not match.", 'duckjoy_offers');		
			}

			//primary type_passenger must not be empty
			//primary address1 must not be empty
			if (empty($_POST['primary']['address1']) ){
				$_POST['messages'][] = __("Please fill your first line of address.", 'duckjoy_offers');
			}
			//primary country must not be empty
			if (empty($_POST['primary']['country'])){
				$_POST['messages'][] = __("Please fill your country of residence.", 'duckjoy_offers');	
			}
			//primary phone must not be empty
			if (empty($_POST['primary']['phone'])){
				$_POST['messages'][] = __("A phone number is required.", 'duckjoy_offers');	
			}
			//primary phone must be a number
			if (!is_numeric($_POST['primary']['phone'])){
				$_POST['messages'][] = __("Primary phone number must be a number.", 'duckjoy_offers');	
			}
			
            //number of filled passengers should match total_people -1 

			//number of couples should not be greater than total_people/2 
			
			
			//the number of passengers should be correct

			//the passengers should have all fields filled
			if (!empty($_POST['passenger'])){
				foreach ($_POST['passenger'] as $passenger){
					if (empty($passenger['name']) || empty($passenger['dob']) ){
						$_POST['messages'][] = __("Other passengers need a name and a date of birth.", 'duckjoy_offers');	
					}
				}
			}
			//we should be expecting x number of adults, y number of children, z number of students

            //visa
			if (empty($_POST['visa'])){
				$_POST['messages'][] = __("Please select a visa option.", 'duckjoy_offers');		
			}

			//t&c aggreement
			if (empty($_POST['t&c'])){
				$_POST['messages'][] = __("Must agree with the terms and conditions.", 'duckjoy_offers');		
			}

			// $_POST['messages'][] = __("This failed for a reason or another.", 'duckjoy_offers');	

			//passengers
			$total_rooms = $_SESSION['compare_form_vars']['total_rooms'];
			$total_people = $_SESSION['compare_form_vars']['doubles_adult'] + 
					$_SESSION['compare_form_vars']['doubles_student'] +
					$_SESSION['compare_form_vars']['doubles_children'] +
					$_SESSION['compare_form_vars']['singles_adult'] +
					$_SESSION['compare_form_vars']['singles_student'] +
					$_SESSION['compare_form_vars']['singles_children'] ;

			$single_rooms = $_SESSION['compare_form_vars']['singles_number'];
			$double_rooms = $_SESSION['compare_form_vars']['doubles_number'];
			$num_adults = $_SESSION['compare_form_vars']['doubles_adult'] + $_SESSION['compare_form_vars']['singles_adult'];
			$num_children = $_SESSION['compare_form_vars']['doubles_children'] + $_SESSION['compare_form_vars']['singles_children'] ;
			$num_students = $_SESSION['compare_form_vars']['singles_student'] + $_SESSION['compare_form_vars']['doubles_student'];


			if (!empty($_POST['messages'])){
				// if the post invalid, return back to form with default values
				$_POST['next_page'] = 'compare_passenger_info';
				return;
			} else {
				//if the form is all valid 
				//store in session variables from the booking AND from passenger info.
				if (empty($_POST['couples'])) $_POST['couples'] = 0;
				$_SESSION['order']['passenger_details'] = $_POST;
				$_SESSION['order']['booking_details'] = $_SESSION['compare_form_vars'];
				$_SESSION['order']['booking_details']['num_rooms'] = $total_rooms;
				$_SESSION['order']['booking_details']['total_people'] = $total_people;
				$_SESSION['order']['booking_details']['single_rooms'] = $single_rooms;
				$_SESSION['order']['booking_details']['double_rooms'] = $double_rooms;
				$_SESSION['order']['booking_details']['num_adults'] = $num_adults;
				$_SESSION['order']['booking_details']['num_children'] = $num_children;
				$_SESSION['order']['booking_details']['num_students'] = $num_students;
				
				
				//go to orders confirm
				wp_redirect('/orders/confirm');
			}
			
		}

		

	}



	public function process_order_info($content){
		global $wpdb, $post;

		if ( !is_single() ){
			return false;  
		}

		//these are not the droids you are looking for
		if (empty($_POST) && !isset($_POST['orderbutton']) || !empty($_POST['group_detail']) ){
			return false;
		}

		// verify the code
		if (!isset($_REQUEST['nonce']) || !wp_verify_nonce( $_REQUEST['nonce'], 'orderinfo_'.$post->ID )){
			return false;
		}

		//dump(array($_POST));

		// setting the variables we are going to work with later
		$traveldates 		= isset($_POST['traveldates']) ? intval($_POST['traveldates']) : 0;
		$doubles_number 	= isset($_POST['doubles_number']) ? intval($_POST['doubles_number']) : 0; 
		$singles_number 	= isset($_POST['singles_number']) ? intval($_POST['singles_number']) : 0; 
		$doubles_adult 		= isset($_POST['doubles_adult']) ? intval($_POST['doubles_adult']) : 0; 
		$doubles_children 	= isset($_POST['doubles_children']) ? intval($_POST['doubles_children']) : 0;
		$doubles_student 	= isset($_POST['doubles_student']) ? intval($_POST['doubles_student']) : 0;
		$singles_adult 		= isset($_POST['singles_adult']) ? intval($_POST['singles_adult']) : 0;
		$singles_children 	= isset($_POST['singles_children']) ? intval($_POST['singles_children']) : 0;
		$singles_student 	= isset($_POST['singles_student']) ? intval($_POST['singles_student']) : 0;
		$orderbutton 		= isset($_POST['orderbutton']) ? sanitize_text_field($_POST['orderbutton']) : 0;

		$route_name 		= isset($_POST['route_name']) ? sanitize_text_field($_POST['route_name']) : 0;
		$offer_code 		= isset($_POST['offer_code']) ? sanitize_text_field($_POST['offer_code']) : 0;
		$post_id 			= isset($_POST['post_id']) ? sanitize_text_field($_POST['post_id']) : 0;
		$stars 				= isset($_POST['stars']) ? sanitize_text_field($_POST['stars']) : 0;
		$availability_code	= isset($_POST['traveldates']) ? sanitize_text_field($_POST['traveldates']) : 0;
		$departure_date = $this->model->get_departure_date_for_availability_code($availability_code);
		$return_date = $this->model->get_return_date_for_availability_code($availability_code);


		$all_meta = get_post_custom(get_the_ID()); 
		$all_tax = get_terms( array('company'));
		foreach($all_tax as $tax){
			$taxonomies[$tax->taxonomy] = $tax;
		}
		
		$availability = $wpdb->get_results("select * from $this->availability_table where offer_id = ".$post->ID);
		$pickup_locations = $wpdb->get_results("select * from $this->pickup_locations_table where offer_id = ".$post->ID);
		$prices = unserialize(unserialize($all_meta['prices'][0])); 

		$cost_doubles_adult = ($prices['double'] * $doubles_adult);
		$cost_doubles_student = ($prices['double_student'] * $doubles_student);
		$cost_doubles_children = ($prices['children'] * $doubles_children);

		$cost_singles_adult = ($prices['single'] * $singles_adult);
		$cost_singles_student = ($prices['single_student'] * $singles_student);
		$cost_singles_children = ($prices['children'] * $singles_children);


		$total_price = $cost_doubles_adult 
			+ $cost_singles_student 
			+ $cost_singles_children 
			+ $cost_singles_adult 
			+ $cost_singles_student
			+ $cost_singles_children;
		$total_rooms = $doubles_number + $singles_number;

		
		//rules & validation
		$_POST['order_info'] = false;
		//Must have at least one room selected
		if (empty($doubles_number) && empty($singles_number)){
			$_POST['messages'][] = __("No room selected. Please select at least one room.", 'duckjoy_offers');
		}

		if (empty($doubles_adult) && empty($doubles_student) && empty($singles_adult) && empty($single_student)){
			if (!empty($doubles_children) || !empty($singles_children)){
				$_POST['messages'][] = __("Children can not be without at least one adult.", 'duckjoy_offers');	
			}
			$_POST['messages'][] = __("At least one adult or student needs to be selected.", 'duckjoy_offers');
		}

		//rule for cant have more people than twice the rooms
		$total_doubles_people = $doubles_adult + $doubles_student;
		if ($total_doubles_people > (2 * $doubles_number)){
			$_POST['messages'][] = __("Too many people for the number of double rooms. Each double room can only have 1 or 2 people.", 'duckjoy_offers');	
		}

		//we cant have more people than singles rooms
		$total_singles_people = $singles_adult + $singles_student;
		if ($total_singles_people > $singles_number){
			$_POST['messages'][] = __("Too many people for the number of single rooms. Each single room can only have 1 person.", 'duckjoy_offers');	
		}

		// rules for too many rooms for less people
		if ($total_singles_people < $singles_number){
			$_POST['messages'][] = __("You have selected $singles_number single rooms for just $total_singles_people people. ", 'duckjoy_offers');	
		}

		//cant have more double rooms than the double of the people. This enables the 2 people for 1 double room
		// number of people can only be double the room OR double -1
		$test_1 = true;
		$test_2 = true;
		if (  $total_doubles_people != $doubles_number*2 ){
			$test_1 = false;
		}

		if ($total_doubles_people != $doubles_number*2-1){
			$test_2 = false;
		}

		//if there are more people than the double of rooms, there is another rule above that takes care of that
		if ($test_1 == false && $test_2 == false && $total_doubles_people < $doubles_number*2){
			$_POST['messages'][] = __("You have selected $doubles_number double rooms for just $total_doubles_people people. ", 'duckjoy_offers');	
		}

		//if there is an idication to go back
		if (!empty($_POST['messages'])){
			$_POST['next_page'] = 'compare_detail';	
			return false;
		} else {

			//store it in the session
			//if(session_status() !== PHP_SESSION_ACTIVE) session_start();
			

			if (isset($_SESSION['compare_form_vars'])) unset($_SESSION['form_vars']);

			$data = array(
				//number of rooms
				'doubles_number'=>$doubles_number,
				'singles_number'=>$singles_number,
				//number of people
				'doubles_adult'=>$doubles_adult,
				'doubles_student'=>$doubles_student,
				'doubles_children'=>$doubles_children,
				'singles_adult'=>$singles_adult,
				'singles_student'=>$singles_student,
				'singles_children'=>$singles_children,
				//totals
				'total_price'=>$total_price,
				'total_rooms'=>$total_rooms,
			);

			foreach ($data as $k=>$v){
				$_SESSION['compare_form_vars'][$k] = $v;	
			}
			
			$_SESSION['compare_form_vars']['total_price'] 	= $total_price;
			$_SESSION['compare_form_vars']['total_rooms'] 	= $total_rooms;
			$_SESSION['compare_form_vars']['locations'] 	= $pickup_locations;
			$_SESSION['compare_form_vars']['route_name'] 	= $route_name;
			$_SESSION['compare_form_vars']['post_id'] 		= $post_id;
			$_SESSION['compare_form_vars']['offer_code'] 	= $offer_code;
			$_SESSION['compare_form_vars']['stars']		 	= $stars;
			$_SESSION['compare_form_vars']['return_date']	= $return_date;
			$_SESSION['compare_form_vars']['departure_date']= $departure_date;

			

			//Proceed with order
			$_POST['next_page'] = 'compare_passenger_info';	
		}

		
	
	/*==================================
	=  Post Type definition            =
	==================================*/




	}

	public function register_post_type(){
		$args = array(
			'labels' => array(
				'name'              => 'Holiday Package',
				'singular_name'     => 'Holiday Package',
				'menu_name'         => 'Compare',
				'all_items'         => 'Holiday Packages',
				'add_new'			=> 'Add New',
				'add_new_item'		=> 'Add New holiday package',
				'edit_item'			=> 'Edit Holiday Package',
				'new_item'			=> 'New Holiday Package',
				'view_item'			=> 'View Holiday Package',
				'search_items'		=> 'Search Holiday Package',
				'not_found'			=> 'No holiday packages found',
				'not_found_in_trash'=> 'No holiday packages in trash',
				'parent_item_colon'	=> '',
				),
			'public' => true, 
			'exclude_from_search' => true,
			//'menu_icon' => admin_url() . '/images/wpspin_light.gif',
			'supports' => array(
				'title',
				'thumbnail',
				),
			);

		register_post_type($this->post_type, $args);

	}

	/*==================================
	=            Taxonomies            =
	==================================*/
	
	
	public function makeTaxonomy($singular, $plural){
		$new_tax = array();

		$joint_singular = str_replace(' ', '_', $singular);

		
		$new_tax[strtolower($joint_singular)] = array(
			'hierarchical' => true,
			'query_var' => $joint_singular,
			'rewrite' => array(
				'slug' => 'offer/'.$joint_singular
				),
			'labels' => array(
				'name'              => ucwords($singular),
				'singular_name'     => $singular,
				'menu_name'         => $plural,
				'all_items'         => $plural,
				'add_new'			=> 'Add New '.ucwords($singular),
				'add_new_item'		=> 'Add New '.ucwords($singular),
				'edit_item'			=> 'Edit '.ucwords($singular),
				'update_item'		=> 'Update '.ucwords($singular),
				'new_item'			=> 'New '.ucwords($singular),
				'view_item'			=> 'View '.ucwords($singular),
				'search_items'		=> 'Search '.ucwords($plural),
				'not_found'			=> 'No '.strtolower($plural).' found',
				'not_found_in_trash'=> 'No '.strtolower($plural).' found in trash',
				'parent_item_colon'	=> '',
				),
			'public' => true,
			);		

		return $new_tax;
	}


	public function taxonomies(){
		$taxonomies = array();

		$company 		= $this->makeTaxonomy('Company', 'Companies');
		// $offer_code 	= $this->makeTaxonomy('offer code', 'offer codes');

		$taxonomies['company'] 			= $company['company'];
		// $taxonomies['offer_code'] 		= $offer_code['offer_code'];

		$this->register_all_taxonomies($taxonomies);

	}


	public function register_all_taxonomies($taxonomies){
		foreach($taxonomies as $taxonomy_name => $args){
			register_taxonomy($taxonomy_name, $this->post_type, $args);
		}
	}

	/*=================================
	=            Metaboxes            =
	=================================*/
	
	
	/**
	*
	* initial endpoint for all metadata
	* Place - normal, advanced, side
	* priority - high, core, low 
	*
	**/
	public function meta_boxes(){
		$metaboxes = array();

		//priority high
		

		//priority core
		// $metaboxes['offer_route_name'] = array(
		// 	'slug' => $this->post_type.'_offer_route_name',
		// 	'title' => 'Route Name',
		// 	'callback' => array($this, 'render_offer_route_name'),
		// 	'place' => 'normal',
		// 	'priority' => 'high'
		// 	);
		$metaboxes['availability'] = array(
			'slug' => $this->post_type.'_availability',
			'title' => 'Available Dates',
			'callback' => array($this, 'render_availability'),
			'place' => 'normal',
			'priority' => 'core'
			);
		$metaboxes['company'] = array(
			'slug' => $this->post_type.'_company',
			'title' => 'Company',
			'callback' => array($this, 'render_company'),
			'place' => 'side',
			'priority' => 'core',
			);
		$metaboxes['offer_code'] = array(
			'slug' => $this->post_type.'_offer_code',
			'title' => 'Package Code',
			'callback' => array($this, 'render_offer_code'),
			'place' => 'side',
			'priority' => 'core',
			);

		//priority low
		$metaboxes['pickup_locations'] = array(
			'slug' => $this->post_type.'_pickup_locations',
			'title' => 'Pickup Locations and Times',
			'callback' => array($this, 'render_pickup_locations'),
			'place' => 'normal',
			'priority' => 'core'
			);
		$metaboxes['stars'] = array(
			'slug' => $this->post_type.'_stars',
			'title' => 'Stars',
			'callback' => array($this, 'render_stars'),
			'place' => 'side',
			'priority' => 'low'
			);
		$metaboxes['transports'] = array(
			'slug' => $this->post_type.'_transports',
			'title' => 'Transports',
			'callback' => array($this, 'render_transports'),
			'place' => 'side',
			'priority' => 'low'
			);
		$metaboxes['length'] = array(
			'slug' => $this->post_type.'_length',
			'title' => 'Length',
			'callback' => array($this, 'render_length'),
			'place' => 'side',
			'priority' => 'low'
			);
		$metaboxes['start'] = array(
			'slug' => $this->post_type.'_start_date',
			'title' => 'Start Date',
			'callback' => array($this, 'render_start_date'),
			'place' => 'side',
			'priority' => 'low'
			);
		$metaboxes['end'] = array(
			'slug' => $this->post_type.'_end_date',
			'title' => 'End Date',
			'callback' => array($this, 'render_end_date'),
			'place' => 'side',
			'priority' => 'low'
			);
		$metaboxes['itinerary'] = array(
			'slug' => $this->post_type.'_itinerary',
			'title' => 'Itinerary',
			'callback' => array($this, 'render_itinerary'),
			'place' => 'normal',
			'priority' => 'low'
			);
		$metaboxes['prices'] = array(
			'slug' => $this->post_type.'_prices',
			'title' => 'Prices',
			'callback' => array($this, 'render_prices'),
			'place' => 'normal',
			'priority' => 'low'
			);
		
		// $metaboxes['info_boxes'] = array(
		// 	'slug' => $this->post_type.'_info_boxes',
		// 	'title' => 'Information',
		// 	'callback' => array($this, 'render_info_boxes'),
		// 	'place' => 'normal',
		// 	'priority' => 'low'
		// 	);

		$this->add_metaboxes($metaboxes);
	}

	/*
	* Adds all metaboxes at once
	* Place - normal, advanced, side
	* priority - high, core, low 
	*/
	public function add_metaboxes($metaboxes){
		foreach($metaboxes as $metabox){
			$this->add_meta_box(
				$metabox['slug'], 
				$metabox['title'], 
				$metabox['callback'], 
				$metabox['place'], //normal, advanced, side
				$metabox['priority'] //high, core, low
				);
		}
	}

	
	/*
	* Adds one metabox only
	*/
	public function add_meta_box($slug, $title, $callback, $place = 'side', $priority = 'core') {
		add_meta_box(
			$slug //slug
			,$title //title
			,$callback
			,$this->post_type,
			$place,
			$priority
			);
	}


	/**
	* Custom metaboxes
	**/
	public function remove_metaboxes(){
		remove_meta_box('companydiv', $this->post_type, 'side');
	}



	/*==========  meta renders  ==========*/
	

	public function render_offer_route_name(){
		global $post; 
		$meta_name = 'offer_route_name';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo '<input name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'" size="50">';

	}

	public function render_offer_code(){
		global $post; 
		$meta_name = 'offer_code';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo '<input name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'">';

	}
	

	public function render_transports(){
		global $post; 
		$meta_name = 'transports';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo "Used means of transport (comma separated): ";
		echo '<input name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'">';
	}

	public function render_start_date(){
		global $post; 
		$meta_name = 'start_date';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo "Date offer is available (yyyy-mm-dd): ";
		echo '<input class="datepicker" name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'">';
	}

	public function render_end_date(){
		global $post; 
		$meta_name = 'end_date';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo "Date offer ends (yyyy-mm-dd): ";
		echo '<input class="datepicker" name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'">';
	}


	public function render_availability(){
		global $post, $wpdb; 
		$meta_name = 'availability';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_values = $wpdb->get_results("SELECT * FROM {$this->availability_table} tb1 WHERE offer_id = {$post->ID} ORDER BY departure ASC");
		$previous_dates = "";
		
		if (empty($saved_values)){
			$previous_dates = __("No previous dates entered yet.", 'duckjoy');
		} else {
			foreach ($saved_values as $value) {
				$departure = date('Y-m-d',strtotime($value->departure));
				$return = date('Y-m-d',strtotime($value->return));
				$status = (empty($value->status))? '' : 'checked'; 
				$previous_dates .='<tr><td><input type="text" class="datepicker" name="availability_departure['.$value->availability_code.']" value="'.$departure.'"></td>';
				$previous_dates .='<td><input type="text" class="datepicker" name="availability_return['.$value->availability_code.']" value="'.$return.'"></td>';
				$previous_dates .='<td><input type="checkbox" name="status['.$value->availability_code.']" '.$status.' ></td></tr>';
			}
		}
		?>
		<table id="availability_table" style="text-align:center">
			<thead>
				<td>Departure Date</td>
				<td>Return Date</td>
				<td>Active</td>
			</thead>
			<tbody>
				<?php echo $previous_dates; ?>
				<?php $time_now = time(); ?>
				<td><input type="text" class="datepicker" name="availability_departure[<?php echo $time_now; ?>]"></td>
				<td><input type="text" class="datepicker" name="availability_return[<?php echo $time_now; ?>]"></td>
				<td><input type="checkbox" name="status[<?php echo $time_now; ?>]" checked='true'></td>
			</tbody>
		</table>
		<div><a href="javascript:void(0)" id="add_availability"> + Add another date</a></div>
		<?php
	}

	public function render_pickup_locations(){
		global $post, $wpdb; 
		$meta_name = 'pickup_locations';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_values = $wpdb->get_results("SELECT * FROM $this->pickup_locations_table tb1 WHERE offer_id = {$post->ID}");
		$previous_dates = "";
		
		if (empty($saved_values)){
			$previous_dates = __("No previous locations entered yet.");
		} else {
			foreach ($saved_values as $value) {
				$time = date('H:i',strtotime($value->time));
				$previous_dates .='<tr><td><input type="text" size="50" name="pickup_location['.$value->pickup_code.']" value="'.$value->location.'"></td>';
				$previous_dates .='<td><input type="text" class="timepicker" name="pickup_time['.$value->pickup_code.']" value="'.$time.'"></td></tr>';
			}
		}
		?>
		<table id="pickup_table" style="text-align:center">
			<thead>
				<td>Location</td>
				<td>Time</td>
			</thead>
			<tbody>
				<?php echo $previous_dates; ?>
				<?php $time_now = time(); ?>
				<td><input type="text" size="50" name="pickup_location[<?php echo $time_now; ?>]"></td>
				<td><input type="text" class="timepicker" name="pickup_time[<?php echo $time_now; ?>]"></td>
			</tbody>
		</table>
		<div><a href="javascript:void(0)" id="add_location"> + Add another location & time</a></div>
		<?php
	}


	public function render_prices(){
		global $post; 
		$meta_name = 'prices';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_values = unserialize(get_post_meta( $post->ID, $meta_name, true));
		
		echo "<p>Please insert prices without the currency, example - 399</p>";
		echo "<label>Price for Single Room: ";
		echo '<input name="'.$meta_name.'[single]" type="text" value="'.esc_attr($saved_values['single']).'"></label><br/>';
		echo "<label>Price for Double Room: ";
		echo '<input name="'.$meta_name.'[double]" type="text" value="'.esc_attr($saved_values['double']).'"></label><br/>';
		echo "<label>Price for Single Room Student: ";
		echo '<input name="'.$meta_name.'[single_student]" type="text" value="'.esc_attr($saved_values['single_student']).'"></label><br/>';
		echo "<label>Price for Double Room Student: ";
		echo '<input name="'.$meta_name.'[double_student]" type="text" value="'.esc_attr($saved_values['double_student']).'"></label><br/>';
		echo "<label>Price for Children: ";
		echo '<input name="'.$meta_name.'[children]" type="text" value="'.esc_attr($saved_values['children']).'"></label><br/>';
		echo "<label>Price for Pick Up: ";
		echo '<input name="'.$meta_name.'[pickup]" type="text" value="'.esc_attr($saved_values['pickup']).'"></label><br/>';
	}

	public function render_info_boxes(){
		global $post; 
		$meta_name = 'info_boxes';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_values = json_decode(get_post_meta( $post->ID, $meta_name, true));
		$message = "";
		$fare_description_str = $message;
		$day_by_day_str = $message;
		$visa_str = $message;
		$precautions_str = $message;
		if (is_object($saved_values)){
			$fare_description_str 	= esc_attr($saved_values->fare_description);
			$day_by_day_str 		= esc_attr($saved_values->day_by_day);
			$visa_str 				= esc_attr($saved_values->visa);
			$precautions_str 		= esc_attr($saved_values->precautions);
		}

		echo "<h3>Fare Description</h3>";
		$settings = array('textarea_name'=>$meta_name.'[fare_description]', 'teeny'=>false,'media_buttons' => false,'editor_height' => 50);
		wp_editor($fare_description_str, 'fare_description', $settings);

		echo "<br><h3>Day-by-Day</h3>";
		$settings['textarea_name'] = $meta_name.'[day_by_day]';
		wp_editor($day_by_day_str, 'day_by_day', $settings);

		echo "<br><h3>Visa Information </h3>";
		$settings['textarea_name'] = $meta_name.'[visa]';
		wp_editor($visa_str, 'visa', $settings);

		echo "<br><h3>Precautions </h3>";
		$settings['textarea_name'] = $meta_name.'[precautions]';
		wp_editor($precautions_str, 'precautions', $settings);
	}


	/**
	* Company render - Custom Taxonomy metabox
	**/
	public function render_company(){
		global $post; 
		$tax_name = 'company';
		$tax_title = ucwords($tax_name);
		$tax_slug = $this->post_type.'_'.$tax_name; 
		$tax = get_taxonomy($tax_name);
		//The name of the form
		$name = 'tax_input[' . $tax_name . ']';
		//Get all the terms for this taxonomy
		$terms = get_terms($tax_name,array('hide_empty' => 0));
		$postterms = get_the_terms( $post->ID,$tax_name );
		$current_term = ($postterms ? array_pop($postterms) : false);
		?>
		<!-- Display taxonomy terms -->
		<div id="<?php echo $tax_name; ?>-all" class="tabs-panel">
			<select name="<?php echo $name; ?>">
				<?php foreach($terms as $term){
					$selected = ($term->term_id == $current_term->term_id)? true : false;
					if ($selected){
						echo '<option value="'.$term->term_id.'" selected>'.$term->name.'</option>';	
					} else {
						echo '<option value="'.$term->term_id.'">'.$term->name.'</option>';
					}
				}?>
			</select>
		</div>
		<?php
	}


	
	public function render_itinerary(){
		global $post; 
		$meta_name = 'itinerary';
		$meta_title = ucwords($meta_name);
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		?>
		<p>Please insert the names of all the cities this journey separated by comma,<br/> 
			e.g., London, Frankfurt, Brussels (Belgium), Porto (portugal)<br/>
			The first and the last cities will be the origin and the destination for the tour.
		</p>
		<textarea name="<?php echo $meta_name; ?>" id="" cols="100" rows="5"><?php echo $saved_value; ?></textarea>
		<?php
	}


	public function render_stars(){
		global $post; 
		$meta_name = 'stars';
		$meta_title = 'Stars';
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		$stars = array(1,2,3,4,5); 
		?>
		<select name="<?php echo $meta_name; ?>" >
			<?php
			foreach($stars as $star){
				$selected = ($star == $saved_value)? true : false;
				if ($selected){
					echo '<option value="'.$star.'" selected>'.$star.'</option>';	
				} else {
					echo '<option value="'.$star.'">'.$star.'</option>';
				}
			}
			?>
		</select>

		<?php
	}


	public function render_length(){
		global $post; 
		$meta_name = 'length';
		$meta_title = ucwords($meta_name);
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		$days = range(1,30); 
		?>
		<select name="<?php echo $meta_name; ?>" >
			<?php
			foreach($days as $day){
				$selected = ($day == $saved_value)? true : false;
				if ($selected){
					echo '<option value="'.$day.'" selected>'.$day.' days</option>';	
				} else {
					echo '<option value="'.$day.'">'.$day.' days</option>';
				}
			}
			?>
		</select>
		<?php
	}

	/*===================================
	=            Plugin CRUD            =
	===================================*/

	public function save( $post_id ) {
		global $wpdb;

		// Check if our nonce is set.
		// if ( ! isset( $_POST[$this->post_type.'_company_name'] ) )
		// 	return $post_id;
		// $nonce = $_POST['duck_offers_nonce'];

		// // Verify that the nonce is valid.
		// if ( ! wp_verify_nonce( $nonce, 'duck_offers_nonce_field' ) )
		// 	return $post_id;

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( ! current_user_can( 'edit_page', $post_id ) )
			return $post_id;

		// save metas
		if (empty($_POST['offer_route_name'])) $_POST['offer_route_name'] = null;
		$offer_route_name = sanitize_text_field( $_POST['offer_route_name'] );
		$this->save_meta( $post_id, 'offer_route_name', $offer_route_name );

		if (empty($_POST['offer_code'])) $_POST['offer_code'] = '--';
		$offer_code = sanitize_text_field( $_POST['offer_code'] );
		$this->save_meta( $post_id, 'offer_code', $offer_code );

		if (empty($_POST['stars'])) $_POST['stars'] = 3;
		$stars = sanitize_text_field( $_POST['stars'] );
		$this->save_meta( $post_id, 'stars', $stars );


		if (empty($_POST['transports'])) $_POST['transports'] = null;
		$transports	 = $this->sanitize_text_field( $_POST['transports'] );
		$this->save_meta( $post_id, 'transports', $transports );

		if (empty($_POST['length'])) $_POST['length'] = null;
		$length	= sanitize_text_field( $_POST['length'] );
		$this->save_meta( $post_id, 'length', $length );
		
		
		if (empty($_POST['start_date'])) $_POST['start_date'] = null;
		$start_date	= $this->sanitize_text_field( $_POST['start_date'] );
		$this->save_meta($post_id, 'start_date', $start_date);

		if (empty($_POST['end_date'])) $_POST['end_date'] = null;
		$end_date = $this->sanitize_text_field( $_POST['end_date'] );
		$this->save_meta( $post_id, 'end_date', $end_date );

		//itinerary saves to 2 places. To the meta and to its own table.
		//the metabox is the one for display
		//its own table serves only so to index all cities later
		$itinerary = sanitize_text_field( @$_POST['itinerary'] );
		$this->save_meta($post_id, 'itinerary', $itinerary);
		$this->model->save_itinerary($itinerary);

		//prices is an array of values
		$this->save_prices($post_id);


		//info boxes
		if (!empty($_POST['info_boxes'])){
			foreach ($_POST['info_boxes'] as &$value) {
				$value = sanitize_text_field($value);
			}
			$info_boxes = json_encode(($_POST['info_boxes']));
			update_post_meta( $post_id, 'info_boxes', $info_boxes );			
		}

		$this->save_availability_boxes($post_id);

		$this->save_pickup_boxes($post_id);

	}

	public function sanitize_text_field($text_field){
		return sanitize_text_field($text_field);
	}

	public function save_meta($post_id, $key, $post_field){
		return update_post_meta( $post_id, $key, $post_field );
	}

	public function save_prices($post_id){
		if (!empty($_POST['prices'])){
			foreach($_POST['prices'] as &$price){
				//TODO: get only the numbers in here - This didnt work for some reason... 
				// preg_match('/([0-9]*)/', $price, $match);
				//convert this to a number no matter what it is.
				$price = ($price)? $price : (int) 0;
			}
			$prices = serialize($_POST['prices']);
			$this->save_meta( $post_id, 'prices', $prices );
		}
	}

	public function save_availability_boxes($post_id){
		global $wpdb;
		//If I have ANY value on all the departure dates
		// so initial page will not go into this code
		if (!empty($_POST['availability_departure'])){
			foreach ($_POST['availability_departure'] as $key => $value) {
				$key = strip_tags($key); 
				$departure = $value;
				$return = $_POST['availability_return'][$key];
				$status = (!empty($_POST['status'][$key])) ? true : false ;
				
				//if there is a missing return date, ignore this entry
				if (empty($departure)){
					//we need to delete
					$wpdb->delete($this->availability_table, array('availability_code'=>$key));
					continue;
				} 
				
				$departure = date('Y-m-d 00:00:00', strtotime($departure));
				$return = date('Y-m-d 00:00:00', strtotime($return));
				$data = array(
					'availability_code'	=>	$key,
					'offer_id'			=>	$post_id,
					'departure'			=>	$departure,
					'return'			=>	$return,
					'status'			=>	$status
					);
				
				$wpdb->replace( $this->availability_table, $data );
			}
		} 
	}

	public function save_pickup_boxes($post_id){
		global $wpdb;
		//If I have ANY value on all the departure dates
		// so initial page will not go into this code
		if (!empty($_POST['pickup_location'])){
			foreach ($_POST['pickup_location'] as $key => $value) {
				$key = strip_tags($key); 
				$location = sanitize_text_field($value);
				$time = date('H:i:s', strtotime($_POST['pickup_time'][$key]) );

				//if there is no location just delete or skip
				if (empty($location)){
					//we need to delete
					$wpdb->delete($this->pickup_locations_table, array('pickup_code'=>$key));
					continue;
				} 

				$data = array(
					'pickup_code'		=>	$key,
					'offer_id'			=>	$post_id,
					'location'			=>	$location,
					'time'				=>	$time
					);
				$format = array( 
					'%d',
					'%d', 
					'%s',
					'%s',
					); 

				$wpdb->replace( $this->pickup_locations_table, $data, $format );
			}
		} 
	}


	



} //end of class





