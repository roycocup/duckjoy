<?php 



class Offers_model extends db{

	
	public function __construct(){
		global $wpdb;
		$this->wpdb = $wpdb;
	}


	/**
	*
	* Get a post by id
	*
	**/
	public function get_post_by_id($post_id){
		$post = get_post($post_id, 'object');
		return $post; 
	}


	public function add_data_to_post($post, $terms = array('company') ){
		$taxonomies = self::get_taxonomies_for_post($post, $terms);
		$post->meta = self::get_meta_for_post($post);
		$post->taxonomies = $taxonomies;
		$post->availability = $this->get_availability_for_post($post);
		$post->prices = unserialize(unserialize($post->meta['prices'][0]));

		require_once(dirname(dirname(dirname(__FILE__)))."/duckjoy_orders/models/reviews.php");
		$reviews = new Reviews_model();
		$post->rating = $reviews->getAverageRatingForOfferId($post->ID);
		
		return $post;
	}

	public static function get_meta_for_post( $post ){
		$all_meta = get_post_custom($post->ID);
		return $all_meta;
	}


	public static function get_taxonomies_for_post($post, $terms){
		foreach($terms as $k => $term){
			$name = wp_get_post_terms($post->ID, $term);
			$taxonomies[$term] = $name[0];
		}
		return $taxonomies;
	}

	public function get_availability_for_post($post){
		$sql = "SELECT * FROM $this->availability_table WHERE offer_id = ".$post->ID;
		$sql .= " ORDER BY departure asc";
		$results = $this->wpdb->get_results($sql);
		return $results;
	}

	public function get_availability_for_post_id($post_id, $after_today = false){
		$sql = "SELECT * FROM $this->availability_table WHERE offer_id = ".$post_id;
		if ($after_today){
			$sql .= " AND departure > now()"; 
		}
		$sql .= " ORDER BY departure asc";
		$results = $this->wpdb->get_results($sql);
		return $results;
	}

	public function get_all_companies(){
		$sql = "SELECT distinct(t.name)
				FROM wp_terms AS t
				INNER JOIN wp_term_taxonomy AS tt ON tt.term_id = t.term_id
				INNER JOIN wp_term_relationships AS tr ON tr.term_taxonomy_id = tt.term_taxonomy_id
				WHERE tt.taxonomy IN ('company')";
		$rs = $this->wpdb->get_results($sql);
		foreach ($rs as $comp_obj) {
			$companies[] = $comp_obj->name;
		}
		return $companies;
	}

	public function get_cities( $extra_args = array() ){

		$sql = "SELECT * FROM $this->cities_table t1";

		if(!empty($extra_args)){
			foreach ($extra_args as $key => $value) {
				$sql .=  " ".$value;
			}
		}

		$results = $this->wpdb->get_results($sql);

		return $results;
	}


	public function get_city_on_itinerary_by_name($city_name){
		$sql = "SELECT * FROM $this->itinerary_table WHERE place_name = '{$city_name}'";
		$results = $this->wpdb->get_results($sql);

		return $results;
	}


	public function save_itinerary($itinerary){
		global $post, $wpdb;
		
		mb_regex_encoding('UTF-8');
		mb_internal_encoding("UTF-8"); 
		$itinerary = mb_split( '[，,]+' , $itinerary );
		//$itinerary = explode(',', $itinerary);

		
		$origin = strtolower(trim($itinerary[0])); 
		$destination = strtolower(trim($itinerary[count($itinerary)-1]));
		
		
		//dont do anything unless this is a full post with an ID. 
		if (isset($post->ID)) {

			$previous_itinerary = $wpdb->query("DELETE FROM $this->itinerary_table WHERE post_id = $post->ID");

			foreach ($itinerary as $place_name) {
				$place_name = strtolower(trim($place_name));
				if (empty($place_name)) continue;

				$data = array(
					'post_id' => $post->ID,
					'place_name'=>$place_name
					);
				$data['origin'] = ($place_name == $origin) ? 1 : 0;
				$data['destination'] = ($place_name == $destination) ? 1 : 0; 

				$wpdb->replace( $this->itinerary_table, $data );	
			}
			
			$this->reindex_countries(); 
		}
		return true;
		
	}


	private function reindex_countries(){
		global $wpdb;

		//get the list from itenerary
		$sql = "SELECT DISTINCT(place_name) FROM $this->itinerary_table";
		$cities = $wpdb->get_results($sql);

		$countries_found = $this->find_country_in_cities($cities);
		asort($countries_found);
		foreach ($countries_found as $country) {
			//check that exists there first
			$country_exists = $this->find_country_in_db($country);
			//if it could not find anything
			if (!$country_exists){
				$wpdb->insert($this->countries_table, array('name'=>$country) );
			}
		}
		$this->reindex_cities();
	}


	private function reindex_cities(){
		global $wpdb;

		$sql = "SELECT DISTINCT(place_name) FROM $this->itinerary_table";
		$cities = $wpdb->get_results($sql);

		$sql = "TRUNCATE TABLE $this->cities_table";
		$wpdb->query($sql);
		
		//many hours of work to sync cities and countries, turns out chinese caracters and regex dont get on well
		foreach($cities as $city){
			$country = $this->find_country_in_city($city);
			$country = $this->find_country_in_db($country);
			$data = array(
				'name'=>$city->place_name
				);
			if (!empty($country)){
				$data['country_id'] = $country[0]->country_id;
			} 
			
			//check that the city is an origin for ANY itineary
			$city_in_itinerary = $this->get_city_on_itinerary_by_name($city->place_name);
			foreach ($city_in_itinerary as $value) {
				//assign the data to the query to be updated
				if ($value->origin) $data['origin'] = 1;
				if ($value->destination) $data['destination'] = 1;
			}
			//update the table
			$wpdb->insert($this->cities_table, $data );
		}
		
	}


	private function find_country_in_cities($cities){
		foreach($cities as $city){
			$country = $this->find_country_in_city($city);
			if (!empty($country)){
				$result[] = $country;
			}
		}
		return $result;
	}

	private function find_country_in_city($city){
		//$pattern = '/.*?\(([a-z]*)/u';
		$pattern = '/[\x{3400}-\x{4DB5}]*';
		//$pattern .= '[\x{3200}-\x{9FFF}]*';
		$pattern .= '\(([a-z]*)/u';
			preg_match($pattern, $city->place_name, $match);
			if (!empty($match[1])){
				return $match[1];
			}
	}


	private function find_country_in_db($country){
		global $wpdb;
		$country = $wpdb->get_results("SELECT * FROM $this->countries_table WHERE name = '{$country}'");
		if (empty($country[0]->name)){
			$country = false;
		}
		return $country;
	}



	public function get_posts_for_search($post_type = 'compares', $args){
		$sql = "SELECT DISTINCT(p.ID)
				FROM wp_posts p
				JOIN wp_duck_itinerary i ON i.post_id = p.ID
				JOIN wp_duck_availability a ON a.offer_id = p.ID
				JOIN wp_postmeta pm ON pm.post_id = p.ID
				WHERE p.post_type = 'compares' AND p.post_status = 'publish' ";
		if (!empty($args['destination'])) 		$sql .= "AND i.place_name LIKE '%{$args['destination']}%' ";
		if (!empty($args['after'])) {
			$after = date('Y-m-d h:i:s', strtotime($args['after']));
			$sql .= "AND a.departure >= '{$after}' AND a.`status` = 1  ";
		}
		if (!empty($args['until'])) {
			$until = date('Y-m-d h:i:s', strtotime($args['until']));
			if (empty($args['after'])) {
				$sql .= "AND a.departure between now() and '{$until}' AND a.`status` = 1 ";
			} else {
				$sql .= "AND a.departure <= '{$until}' AND a.`status` = 1  ";	
			}
			
		}

		$rs = $this->wpdb->get_results($sql);

		return $rs;
	}

	/**
	*
	* Used mostly by the search results filters
	* This will filter a set of post ids and return back the ones that pass it.
	*
	**/
	public function filter_post_ids_by_args($post_ids, $args){

		//One big foreach so we deal with everything inside each post
		$new_list = array();
		foreach ($post_ids as $value){
			$good = array();
			$post = $this->get_post_by_id($value->ID);
			$post = $this->add_data_to_post($post);

			//stars
			if (!empty($args['stars'])){
				foreach ($args['stars'] as $star){
					if ($post->stars == $star) 	{
						$good[$post->ID] = $value;
					}
				}
				if (empty($good[$post->ID])) continue;
			} 

			//length
			if (!empty($args['min_length']) || !empty($args['max_length'])){

				if (!empty($args['min_length']) && empty($args['max_length'])) $case = 1; //only min
				if (empty($args['min_length']) && !empty($args['max_length'])) $case = 2; //only max
				if (!empty($args['min_length']) && !empty($args['max_length'])) $case = 3;// max and min


				if ($case == 1){
					if ($post->length >= $args['min_length']) {
						$good[$post->ID] = $value;
					}else{
						continue;
					}
				}
				if ($case == 2){
					if ($post->length <= $args['max_length']) {
						$good[$post->ID] = $value;
					}else{
						continue;
					}
				}
				if ($case == 3){
					if ($post->length >= $args['min_length'] && $post->length <= $args['max_length']) {
						$good[$post->ID] = $value;
					} else {
						continue;
					}
				}
				
			}


			//transports 
			if (!empty($args['transports'])){
				foreach ($args['transports'] as $transport) {
					if (stripos($post->transports, $transport) !== false){
						$good[$post->ID] = $value;
					}
				}
				if (empty($good[$post->ID])) continue;
			}

			
			//company
			if (!empty($args['company'])){
				foreach ($args['company'] as $company) {
					if ($post->taxonomies['company']->name == $company){
						$good[$post->ID] = $value;
					}
				}
				if (empty($good[$post->ID])) continue;
			}


			//price
			if (!empty($args['min_price']) || !empty($args['max_price'])) {

				if (!empty($args['min_price']) && empty($args['max_price'])) $case = 1; //only min
				if (empty($args['min_price']) && !empty($args['max_price'])) $case = 2; //only max
				if (!empty($args['min_price']) && !empty($args['max_price'])) $case = 3;// max and min


				if ($case == 1){
					if ($post->prices['double'] >= $args['min_price']) {
						$good[$post->ID] = $value;
					}else{
						continue;
					}
				}
				if ($case == 2){
					if ($post->prices['double'] <= $args['max_price']) {
						$good[$post->ID] = $value;
					}else{
						continue;
					}
				}
				if ($case == 3){
					if ($post->prices['double'] >= $args['min_price'] && $post->prices['double'] <= $args['max_price']) {
						$good[$post->ID] = $value;
					}else{
						continue;
					} 
				}
				
			}


			//customer rating
			if (!empty($args['rating'])){				
				if ($post->rating >= (int)$args['rating']){
					$good[$post->ID] = $value;
				}else {
					continue;
				}
			}

			//seems like this one is a good one, so add it to the list
			if(!empty($good))
				$new_list[] = $good[$post->ID];
		}
		return $new_list;
	}



	/**
	*
	* get all the posts there are for this city
	*
	**/
	public function get_posts_by_city($post_type = 'compares', $city = ''){

		$selected_posts = array();
		$args = array(
			'posts_per_page'   => -1,
			'post_type' => array($post_type),
			'orderby'=>'date',
			'order'=>'DESC',
			'post_status'=>'publish',
			);
		$posts = get_posts($args);


		//check for city in the itinerary
		foreach($posts as $post){
			$all_meta = self::get_meta_for_post($post);
			if (stripos($all_meta['itinerary'][0], $city) === false) continue;
			$selected_posts[] = $post;
		}
		return $selected_posts;
	}



	public function get_posts_with_origin_and_destination($post_type, $origin, $destination){
		//get all the posts with the origin
		$origin_posts_ids = $this->get_posts_ids_for_origin($origin);
		//search for the destination inside each one of these posts
		$posts = $this->get_city_present_in_post($origin_posts_ids, $destination);

		return $posts;
	}

	/*
	* Origin posts is an array of objects with post->post_id
	* Needle city is just a name
	*/
	public function get_city_present_in_post($origin_posts, $needle_city){
		$selected_posts = array();
		foreach ($origin_posts as $post){
			$post = $this->get_post_by_id($post->post_id);
			$all_meta = self::get_meta_for_post($post);
			if (stripos($all_meta['itinerary'][0], $needle_city) === false) continue;
			$selected_posts[] = $post;
		}
		
		return $selected_posts;
	}


	/**
	*
	* Get the post ids for the posts that have this city as their origin
	* @returns array of objects
	* 
	**/
	public function get_posts_ids_for_origin($city_name){		
		$sql = "SELECT post_id from $this->itinerary_table where place_name = '{$city_name}' and origin = 1";
		$results = $this->wpdb->get_results($sql);
		return $results;
	}

	/**
	*
	* Get the posts for a city that is the origin of the itinerary of that post.
	* Same as get_posts_ids_for_origin but then gets the actual posts for each one and returns them
	* @returns array of POST object
	*
	**/
	public function get_posts_ids_for_origin_city($city_name){
		$post_ids = $this->get_posts_ids_for_origin($city_name);
		foreach ($post_ids as $key => $value) {
			$posts[] = $this->get_post_by_id($value->post_id);
		}

		return $posts;
	}


	/**
	*
	* AJAX is going to call
	* this at multiple times so better to have empty results a lot
	*
	**/
	
	public function get_available_destinations_for_origin($origin_value){
		$cities = array();
		if (empty($origin_value)) return $cities;

		//get the post ids where the origin is on
		$sql = "SELECT post_id FROM {$this->itinerary_table} WHERE place_name = '{$origin_value}' AND origin = 1";
		$post_ids = $this->wpdb->get_results($sql);


		//get all the cities in between 
		if (empty($post_ids)) return $cities;
		$str_post_ids = '';
		$i = 1; 
		$num_posts = count($post_ids);
		foreach ($post_ids as $key => $post) {
			$str_post_ids .= $post->post_id;
			if ($i < $num_posts) $str_post_ids .= ', ';
			$i++;
		}

		$sql = "SELECT place_name FROM {$this->itinerary_table} WHERE post_id in ({$str_post_ids}) AND origin = 0 ";
		$cities[] = $this->wpdb->get_results($sql);	
		return $cities;
	}


	public function get_departure_date_for_availability_code($availability_code){
		$sql = "SELECT departure FROM $this->availability_table WHERE availability_code = {$availability_code} limit 1";
		$result = $this->wpdb->get_var($sql);
		return $result; 
	}


	public function get_return_date_for_availability_code($availability_code){
		$sql = "SELECT `return` FROM $this->availability_table WHERE availability_code = {$availability_code} limit 1";
		$result = $this->wpdb->get_var($sql);
		return $result; 
	}

	public function get_availability_for_availability_code($availability_code){
		$sql = "SELECT status FROM $this->availability_table WHERE availability_code = {$availability_code} limit 1";
		$result = $this->wpdb->get_var($sql);
		return $result; 
	}


}






?>