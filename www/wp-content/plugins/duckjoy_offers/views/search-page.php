<?php 
	/**
	*	
	* Template for this is compare-template
	*
	**/
?>

<?php 
	$model = new Offers_model();
	$cities = $model->get_cities(array('order by name asc'));
	$origins = $model->get_cities(array('where origin = 1 order by name asc'));
	

	$origins_str = "";
	foreach ($origins as  $value) {
		$origins_str .= '"'.$value->name.'", ';
	}
	
	$cities_str = "";
	foreach ($cities as  $value) {
		$cities_str .= '"'.$value->name.'", ';
	}
?>
<style>
	/*This is the background for the first page. For other pages check compare-template.php under the theme*/
	#content{background-image: url('<?php echo get_stylesheet_directory_uri()."/img/search-green-bg.jpg"?>'); background-repeat:repeat-y; min-height: 700px;}
	#content{font-size: 18px; color: rgba(102,102,102,1);}
	table, td, tr{border: none; font-size: inherit; padding: 0; margin: 0;}

	/*left side*/
	#left-side{float:left; width:620px;}
	.content-side-wrapper{ width: 540px; padding-top: 220px; padding-left: 45px;}
	input{font-size: 18px;}
	input.text-mid{width: 180px;}
	input.destinations{width: 387px;}
	.comparearrow{width: 31px; height: 31px;}

	/*right side*/
	#side{ float: right; width: 440px; margin-top: 300px; text-align: right;}
		.address{font-size: 12px; color: white;}
		.spacer{height: 100px;}
		.side-innerwrapper{padding: 10px; line-height: 30px;}

	input[type='submit']{ padding:5px 20px; background-color: white; color: #3d8150; font-size: 16px; border:none;}

	.menu-logo{position: relative; top:-5px;}
	.titlestyle{font-size: 26px;!important
	width: 486px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	color: rgb(255,255,255);
	height: 40px;
    background: rgb(169,219,128); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(169,219,128,1) 0%, rgba(150,197,111,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(169,219,128,1)), color-stop(100%,rgba(150,197,111,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(169,219,128,1) 0%,rgba(150,197,111,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(169,219,128,1) 0%,rgba(150,197,111,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(169,219,128,1) 0%,rgba(150,197,111,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(169,219,128,1) 0%,rgba(150,197,111,1) 100%); /* W3C */

filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a9db80', endColorstr='#96c56f',GradientType=0 ); /* IE6-9 */	vertical-align: middle;
	padding-top: 0px;
	padding-left: 10px;
	-moz-border-radius: 35px;
	border-radius: 35px;
	margin-left: 0px;margin-top:-20px;
}
</style>


<script>
	jQuery(document).ready(function($){


		//all the origins
		var origins_str = [<?php echo $origins_str; ?>];
		//all the cities
		var cities_str = [<?php echo $cities_str; ?>];			

		$( ".origins" ).autocomplete({
			source: origins_str
		});

		$( ".destinations" ).autocomplete({
			source: cities_str
		});

	});
</script>


<!-- <script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script> -->
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/css/compare_search_calendar.css'; ?>">
<script src="<?php echo get_stylesheet_directory_uri().'/js/compare_search_calendar.js';?>"></script>
<!--<script src="<?php echo get_stylesheet_directory_uri().'/js/offers.js';?>"></script>-->

<style>
	.DynarchCalendar-hover-title div {background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/img/drop-down.gif");}
	.DynarchCalendar-pressed-title div {background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/img/drop-up.gif");}
	.DynarchCalendar-prevMonth div {background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/img/nav-left.gif");}
	.DynarchCalendar-nextMonth div {background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/img/nav-right.gif");}
	.DynarchCalendar-prevYear div {background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/img/nav-left-x2.gif");}
	.DynarchCalendar-nextYear div {background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/img/nav-right-x2.gif");}
</style>

<script type="text/javascript">
 $(function(){
 	$('input.time:text').datepicker_compare()
	})
</script>

<div id="left-side">
	<div class="content-side-wrapper">
		<form action="" method="get">
					
            <div class="titlestyle">Duckjoy-度假搜索</div>
				
			<table style="margin-top:30px;">
            

				<tr>
					<td>目的地:</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"><input size="46px" type="text" name="destination" class="destinations" value="<?php if (!empty($_GET['destination'])) echo $_GET['destination']; ?>"></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			<table>
				<tr>
					<td>最早出发:</td>
					<td>最晚出发:</td>
					<td></td>
				</tr>
				<tr>

					<!-- <td><input type="text" name="after" id="special_start_time" class="time text-con text-mid" /></td> -->
					<td><input type="text" class="time text-con text-mid" id="after" name="after" /></td>
					<td><input type="text" class="time text-con text-mid" id="until" name="until" /></td>
					<td><input type="submit" value="搜索"></td>
				</tr>
			</table>

		</form>             
	</div> <!-- content-side-wrapper -->
</div> <!-- left-side -->

<div id="side">
	<div class="side-innerwrapper">
		<div id="bus">
			<img src="<?php echo get_stylesheet_directory_uri();?>/img/comparegraphic.png" alt="">
		</div>

		<div class="clearfix spacer"></div>

		<div class="clearfix address">
			Duckjoy Limited 2A Compton House, Guildford, GU1 4TX 
			<br/>Tel: +44 (0)7410 429595
			<br/>@2013-2015 大脚鸭旅游网 DUCKJOY LTD | ALL rights reserved
		</div>
	</div>
</div> <!-- #side -->



<div class="clearfix"></div>

