
<?php wp_enqueue_script('pajinate',get_stylesheet_directory_uri().'/js/jquery.pajinate.min.js', 'jquery'); ?>

<style>

	.searchbutton {
		background: rgb(169,219,128);
		background: -moz-linear-gradient(top, rgba(169,219,128,1) 0%, rgba(150,197,111,1) 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(169,219,128,1)), color-stop(100%,rgba(150,197,111,1)));
		background: -webkit-linear-gradient(top, rgba(169,219,128,1) 0%,rgba(150,197,111,1) 100%);
		background: -o-linear-gradient(top, rgba(169,219,128,1) 0%,rgba(150,197,111,1) 100%);
		background: -ms-linear-gradient(top, rgba(169,219,128,1) 0%,rgba(150,197,111,1) 100%);
		background: linear-gradient(to bottom, rgba(169,219,128,1) 0%,rgba(150,197,111,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a9db80', endColorstr='#96c56f',GradientType=0 );
		height: 30px;
		width: 183px;
		font-size: 18px;
		color: rgba(255,255,255,1);s
		-moz-border-radius: 35px;
		border-radius: 35px;
		border: 2px solid rgb(255,255,255);
		margin-right: 60px; 
	}

	.tablerowbg{
		background: rgb(255,255,255); /* Old browsers */
		background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(47%,rgba(246,246,246,1)), color-stop(100%,rgba(237,237,237,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* IE10+ */
		background: linear-gradient(to bottom,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* W3C */
	}

	#listtitle {
		height: 35px;
		font-size: 18px;
		vertical-align: middle;
		padding-top: 10px;
		background: rgb(255,255,255); /* Old browsers */
		background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(47%,rgba(246,246,246,1)), color-stop(100%,rgba(237,237,237,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* IE10+ */
		background: linear-gradient(to bottom,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed',GradientType=0 ); /* IE6-9 */
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		border-top-width: 1px;
		border-right-width: 1px;
		border-bottom-width: 1px;
		border-left-width: 1px;
		border-top-style: solid;
		border-right-style: none;
		border-bottom-style: solid;
		border-left-style: none;
		border-top-color: rgb(204,204,204);
		border-right-color: rgb(204,204,204);
		border-bottom-color: rgb(204,204,204);
		border-left-color: rgb(204,204,204);
}

	/*This is the background for the first page. For other pages check compare-template.php under the theme*/
	#content{ overflow: auto;}
	#content{font-size: 18px; color: rgba(102,102,102,1);}
	table, td, tr{border: none; font-size: inherit; padding: 0; margin: 0;}

	/*left side*/
	#left-side{float: left; width:693px;}
		#left-side .content-side-wrapper{ background-color: white; width: 633px; margin: 30px auto; padding: 40px 15px;}
		#left-side .green{color: rgb(102,191,125);}
		#left-side .pagination{float: right;}
			#left-side .pagination .page-cur{ padding: 0; margin: 0; }
			#left-side .pagination .element{float: right; padding-right: 5px;}
		#left-side table{border-top:2px #ccc; border-bottom:2px #ccc; line-height: 1}
		#left-side table thead td{text-align: center;}
		#left-side td, tr{border:none;}
		#left-side .items tr td{text-align: center; max-width: 50px; padding:40px 0 40px 5px; margin-right:10px; border-top:1px solid #ccc; border-bottom: 1px solid #ccc; }
		#left-side .items tr td:first-child{text-align: left; width: 100px;}
		#left-side .items tr td:nth-last-child(2){width: 80px;}
		#left-side .items tr td:nth-child(2){width: 50px;}
		#left-side .stars{font-size: 10px; font-family: 'Comic Sans MS', cursive;}

	#right-side{ float: right; width: 370px; margin-top: 30px; text-align: right; padding-right: 30px; padding-left:30px; font-size: 14px; line-height: 10px;}
		#filters {color:white;}
			#right-side tr td {padding-bottom: 10px;}


	.clearfix {display: inline-block;clear: both;}
	.clearfix:after {visibility: hidden;display: block;font-size: 0;content: " ";clear: both;}

	.address{float:right; font-size: 12px; color: white; clear: both; margin-bottom: 30px; margin-top: 50px;}
	
	.menu-logo{position: relative; top:-5px;}

</style>


<script>
	jQuery(document).ready(function($) {
		
		//function to create and place numbers for pagination
		function get_pagination_numbers(cur_page, show_each_side){
			var num_pages = $('tbody').length-1;
			var $el = $('.pagination-pages .page-numbers');
			
			if (cur_page < 1) cur_page = 1;
			if (cur_page > num_pages) cur_page = num_pages;

			var left = '';
			var right = '';

			var i; 
			for (i = 1; i <= (cur_page-1); i++){
				left = left + "<a href='#'><span class='page-number' id='"+i+"'>"+i+"</span></a>";
			}
			cur_page = parseInt(cur_page);
			for (i = cur_page+1;  i <= num_pages; i++){
				right = right + "<a href='#'><span class='page-number' id='"+i+"'>"+i+"</span></a>";
			}

			$el.html(left+cur_page+right); 

			return cur_page;
		}

		//function that controls next page arrow
		function showNext(){
			if ( $('.active').next().html() === undefined)  return false;
			$('.active').removeClass('active').fadeOut('slow').next().addClass('active').fadeIn('slow');
		}

		//function that controls previous page arrow
		function showPrev(){
			if ( $('.active').prev('tbody').html() === undefined)  return false;
			$('.active').removeClass('active').fadeOut('slow').prev().addClass('active').fadeIn('slow');
		}

		//function to show a specific page
		function show(page){
			$('.active').removeClass('active').fadeOut('slow');
			$('.page_'+page).addClass('active').fadeIn('slow');
		}
		
		// definitions
		var show_each_side = 5;
		//get the first pages
		cur_page = get_pagination_numbers(1, show_each_side);

		//hide all of the tables
		$('#left-side tbody').hide();
		//show the first one 
		$('#left-side tbody:first').addClass('active').show();

		//handle the click on next
		$('#left-side .page-next').click(function(){
			showNext();
			get_pagination_numbers(++cur_page, show_each_side);
		});
		//handle the cick on previous
		$('#left-side .page-prev').click(function(){
			showPrev();
			get_pagination_numbers(--cur_page, show_each_side);
		});
		//handle click numbers
		$('#left-side .page-number').click(function(){
			var next_page = $(this).html();
			show(next_page);
			cur_page = get_pagination_numbers(next_page, show_each_side);
		});
		
		
	});
</script>


<div id="left-side">
	<div class="content-side-wrapper">
		<div class="pagination">
			<div class="pagination-pages element">
				<a href="#"><span class="page-prev"><</span></a>
				<span class="page-numbers"></span>
				<a href="#"><span class="page-next">></span></a>
			</div>
			<div class="element"><?php echo $num_results; _e(" Results", 'duckjoy_offers');?></div>
		</div>
		<table>
			<thead>
				<tr id="listtitle" style="background-color: rgb(243,243,243);">
					<td>路线名称</td> <!-- route -->
					<td>天数</td> <!-- days -->
					<td>星级</td> <!-- stars -->
					<td>旅游公司</td> <!-- company -->
					<td>评分</td> <!-- rating -->
					<td>出行方式</td> <!-- transports -->
					<td>价格</td> <!-- price -->
				</tr>
			</thead>
			<?php if ($num_results): ?>
				<?php $counter = 1; $page = 1;?>
				<?php foreach($offers as $k=>$post): ?>
					<?php if ($counter == $posts_per_page) {$page++; $counter = 1;} ?>
					<?php if ($counter == 1) echo "<tbody class='items page_{$page}'>"; ?>
					<tr class="<?php echo ($k%2 == 1)? 'tablerowbg':''; ?>">
						<?php $permalink = get_permalink( $post->ID );?>
						<td>
							<?php echo $post->post_title; ?><br><br>
							<a class="green" href="<?php echo $permalink ?>">详情</a>
						</td>
						<td><?php echo $post->meta['length'][0]; ?></td>
						<td class="stars"><?php echo get_stars($post->meta['stars'][0]); ?></td>
						<td><?php echo $post->taxonomies['company']->name; ?></td>
						<td> <?php echo $this->reviews->getAverageRatingForOfferId($post->ID); ?></td>
						<td><?php echo $post->meta['transports'][0]; ?></td>
						<td><?php echo $post->prices['double']; ?>镑</td>
					</tr>
					<?php if ($counter == $posts_per_page) echo "</tbody>"; ?>
					<?php $counter++; ?>
				<?php endforeach;?>
				</tbody>
			<?php else: ?>
				<tr>
					<td colspan="7"><?php _e("No results found", 'duckjoy_offers');?></td>
				</tr>
			<?php endif; ?>
		</table>
		<div class="pagination">
			<div class="pagination-pages element">
				<a href="#"><span class="page-prev"><</span></a>
				<span class="page-numbers"></span>
				<a href="#"><span class="page-next">></span></a>
			</div>
			<div class="element"><?php echo $num_results; _e(" Results", 'duckjoy_offers');?></div>
		</div>
	</div> <!-- content-side-wrapper -->
</div> <!-- left-side -->


<div id="right-side">
	<div id="filters">
		<form action="" method="post" border=>
			<input type="hidden" name="destination" value="<?php echo $_REQUEST['destination']; ?>">
			<input type="hidden" name="after" value="<?php echo $_REQUEST['after']; ?>">
			<input type="hidden" name="until" value="<?php echo $_REQUEST['until']; ?>">

			<table border="1">
				<tr>
					<td>旅行天数：</td> <!-- travel days -->
					<td></td>
					<td></td>
					<td><input <?php checked(@$_REQUEST['days'][1], 1, 'checked'); ?> type="checkbox" name="days[1]" value="1"> <?php _e("1-2 days", 'duckjoy_offers'); ?></td>
					<td><input <?php checked(@$_REQUEST['days'][3], 3, 'checked'); ?> type="checkbox" name="days[3]" value="3"> <?php _e("3-5 days", 'duckjoy_offers')?></td>
				</tr>
				<tr style="border-bottom: 1px solid white;">
					<td></td>
					<td></td>
					<td></td>
					<td><input <?php checked(@$_REQUEST['days'][6], 6, 'checked'); ?> type="checkbox" name="days[6]" value="6"> <?php _e("6-9 days", 'duckjoy_offers'); ?></td>
					<td><input <?php checked(@$_REQUEST['days'][10], 10, 'checked'); ?> type="checkbox" name="days[10]" value="10"> <?php _e("Over 10 days", 'duckjoy_offers');?></td>
				</tr>
				<tr style="border-bottom: 1px solid white;">
					<td>价格范围：</td> <!-- price -->
					<td></td>
					<td></td>
					<td><?php _e("Min", 'duckjoy_offers');?> <input type="text" size="2" name='min_price' value="<?php echo @$_REQUEST['min_price']; ?>"></td>
					<td><?php _e("Max", 'duckjoy_offers');?> <input type="text" size="2" name='max_price' value="<?php echo @$_REQUEST['max_price']; ?>"></td>
				</tr>
				<tr style="border-bottom: 1px solid white;">
					<td>出行方式：</td><!-- transports -->
					<td></td>
					<td><input <?php checked(@$_REQUEST['transports']['跟团游'], '跟团游', 'checked'); ?> type="checkbox" name="transports[跟团游]" value="跟团游">跟团游</td>
					<td><input <?php checked(@$_REQUEST['transports']['邮轮游'], '邮轮游', 'checked'); ?> type="checkbox" name="transports[邮轮游]" value="邮轮游">邮轮游</td>
					<td><input <?php checked(@$_REQUEST['transports']['自由行'], '自由行', 'checked'); ?> type="checkbox" name="transports[自由行]" value="自由行">自由行</td>
				</tr>
				<tr style="border-bottom: 1px solid white;">
					<td>旅游星级：</td><!-- stars -->
					<td></td>
					<td><input <?php checked(@$_REQUEST['stars'][3], 3, 'checked'); ?> type="checkbox" name="stars[3]" value="3"><span style="font-size: 9px;"><?php echo get_stars(3); ?></span></td>
					<td><input <?php checked(@$_REQUEST['stars'][4], 4, 'checked'); ?> type="checkbox" name="stars[4]" value="4"><span style="font-size: 9px;"><?php echo get_stars(4); ?></span></td>
					<td><input <?php checked(@$_REQUEST['stars'][5], 5, 'checked'); ?> type="checkbox" name="stars[5]" value="5"><span style="font-size: 9px;"><?php echo get_stars(5); ?></span></td>
				</tr>
				<tr>
					<td>客户评分：</td> <!-- guest rating -->
					<td></td>
					<td></td>
					<td colspan="2"><input <?php checked(@$_REQUEST['rating'], 6, 'checked'); ?> type="radio" name="rating" value="6"> 6分以上“好”</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2"><input <?php checked(@$_REQUEST['rating'], 7, 'checked'); ?> type="radio" name="rating" value="7"> 7分以上“满意的”</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2"><input <?php checked(@$_REQUEST['rating'], 8, 'checked'); ?> type="radio" name="rating" value="8"> 8分以上“非常好”</td>
				</tr>
				<tr style="border-bottom: 1px solid white;">
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2"><input <?php checked(@$_REQUEST['rating'], 9, 'checked'); ?> type="radio" name="rating" value="9"> 9分以上“好极了”</td>
				</tr>
				
				
				<tr>
					<td>旅游公司</td> <!-- company -->
				<?php $counter = 0; ?>
				<?php foreach ($companies as $company) :
					if ($counter >= 1 ) {
						echo "</tr><tr>"; 
						$counter = 0;
						echo "<td></td>";
					}
				?>	
					<td></td>
					<td></td>
					<td colspan="2"><input <?php checked(@$_REQUEST['company'][$company], $company, 'checked'); ?> type="checkbox" name="company[<?php echo $company; ?>]" value="<?php echo $company; ?>"> <?php echo $company; ?></td>
					<?php $counter++; ?>
				<?php endforeach; ?>
				</tr>
				<tr style="border-bottom: 1px solid white;">
					<td colspan="4"></td>
				</tr>
			</table>
			<div class=".selected_companies"></div>
			<p></p>
			<input class="searchbutton" type="submit" value="<?php _e('Filter', 'duckjoy_offers'); ?>">
		</form>
	</div>

</div>


<div class="address">
	Duckjoy Limited 2A Compton House, Guildford, GU1 4TX
	<br/>Tel: +44 (0)7410 429595
	<br/>@2013-2015 大脚鸭旅游网|ALL rights reserved
</div>
