<?php 

function dump($args){
	echo "<pre>"; 
	if (!is_array($args))
		print_r($args);
	else{
		foreach ($args as $arg){
			print_r($arg);
		}
	}
	echo "</pre>";
}


if ( !function_exists( 'get_stars' ) ) {
	function get_stars($number){
		$str = '';
		for($i = 1; $i <= $number; $i++){
			$str .= '&#9733; ';
		}

		return $str;
	}
}