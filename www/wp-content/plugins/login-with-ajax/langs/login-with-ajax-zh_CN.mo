��    !      $  /   ,      �     �  
   �     	     (     /     >     E     K     c     v     �     �     �  �   �  �   K  h   �  i   I     �     �      �  $   �                    "  !   .  "   P     s     �     �     �  
   �  %  �  #   �      	     ,	     <	     C	     V	     ]	     d	     �	     �	     �	     �	     �	  h   �	  h   2
  k   �
  k        s     z  !   �     �     �     �     �     �     �     �     
       	   0  l   :     �                                   	                            !                           
                                                                     > Lost your password > register An undefined error has ocurred Cancel Changes saved. E-mail Email Enter username or email Forgotten Password Get New Password Global Login Redirect Global Logout Redirect Hi If you would like a specific user role to be redirected to a custom URL upon login, place it here (blank value will default to the global redirect) If you would like a specific user role to be redirected to a custom URL upon logout, place it here (blank value will default to the global redirect) If you'd like to send the user to a specific URL after login, enter it here (e.g. http://wordpress.org/) If you'd like to send the user to a specific URL after logout, enter it here (e.g. http://wordpress.org/) Log In Log Out Login Successful, redirecting... Login widget with AJAX capabilities. Password Profile Register Remember Me Role-Based Custom Login Redirects Role-Based Custom Logout Redirects Save Changes Show profile link? Username We have sent you an email blog admin Project-Id-Version: Login With Ajax 2.0.4
Report-Msgid-Bugs-To: http://wordpress.org/tag/login-with-ajax
POT-Creation-Date: 2014-01-20 14:09-0000
PO-Revision-Date: 2014-01-20 14:11-0000
Last-Translator: Rodrigo Dias <Rodrigo@rodderscode.co.uk>
Language-Team: Simon Lau <adoresimon@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: _e;esc_html_e;__
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: /Users/rodrigodias/sites/duckjoy/plugins/login-with-ajax
 > 忘记密码？点此找回密码 > 还没有账号？立即注册 未知错误！ 取消 修改已保存。 邮箱 邮箱 请输入用户名或者Email 忘记密码 重置密码 全球登陆转向 全球注销转向 您好 如果您想自定义特定用户的登陆转向，请在此填写好(空白表示默认全球转向哦) 如果您想自定义特定用户的登陆注销，请在此填写好(空白表示默认全球注销哦) 如果您想使登陆用户转向特定的一个地址，请在此填写好(比如：http://wordpress.org/) 如果您想使注销用户转向特定的一个地址，请在此填写好(比如：http://wordpress.org/) 登陆 注销 登陆成功，正在转向中... AJAX登陆微件 密  码 个人资料 注册 记住密码 自定义登陆转向 自定义注销转向 修改已保存 显示资料链接？ 用户名 我们已经收到您重置密码的请求并向您发送了邮件，请您按照邮件提示重置密码。 博客后台 