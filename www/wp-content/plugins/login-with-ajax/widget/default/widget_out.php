<?php 
/*
 * This is the page users will see logged out. 
 * You can edit this, but for upgrade safety you should copy and modify this file into your template folder.
 * The location from within your template folder is plugins/login-with-ajax/ (create these directories if they don't exist)
*/
?>

	<div class="lwa lwa-default">
		<form class="lwa-form" action="<?php echo esc_attr(LoginWithAjax::$url_login); ?>" method="post">
			<div class="left">
				<span class="lwa-status"></span>
				<div class="column-1">
					<p><?php _e("Email", 'login-with-ajax');?></p>
					<p><?php _e("Password", 'login-with-ajax');?></p> 
					<p><?php esc_html_e( 'Remember Me','login-with-ajax' ) ?></p>
				</div>
				<div class="column-2">
					<p><input type="text" name="log" /></p>
					<p><input type="password" name="pwd" /></p>
					<p><input name="rememberme" type="checkbox" class="lwa-rememberme" value="forever" /></p>
					<input type="hidden" name="lwa_profile_link" value="<?php echo esc_attr($lwa_data['profile_link']); ?>" />
					<input type="hidden" name="login-with-ajax" value="login" />
					<input type="submit" name="wp-submit" id="lwa_wp-submit" value="<?php esc_attr_e('Log In', 'login-with-ajax'); ?>" tabindex="100" /><br>
				</div>
			</div>
			
			<div class="right">
				<br />
				<?php if ( get_option('users_can_register') && !empty($lwa_data['registration']) ) : ?>
					<a href="<?php echo home_url(); ?>/register" ><?php _e('> register','login-with-ajax')//register ?></a>
				<?php endif; ?>
				<br />	
				<?php if( !empty($lwa_data['remember']) ): ?>
					<a class="lwa-links-remember" href="<?php echo esc_attr(LoginWithAjax::$url_remember); ?>" title="<?php esc_attr_e('Password Lost and Found','login-with-ajax') ?>"><?php _e('> Lost your password','login-with-ajax') //lost your password? ?></a>
				<?php endif; ?>
			</div>
		</form>

		<div class="left">
			<?php if( !empty($lwa_data['remember']) ): ?>
				<form class="lwa-remember" action="<?php echo esc_attr(LoginWithAjax::$url_remember) ?>" method="post" style="display:none;">
					<span class="lwa-status"></span>
					<table>
						<tr>
							<td>
								<strong><?php esc_html_e("Forgotten Password", 'login-with-ajax'); ?></strong>         
							</td>
						</tr>
						<tr>
							<td class="lwa-remember-email">  
								<?php $msg = __("Enter username or email", 'login-with-ajax'); ?>
								<input type="text" name="user_login" class="lwa-user-remember" value="<?php echo esc_attr($msg); ?>" onfocus="if(this.value == '<?php echo esc_attr($msg); ?>'){this.value = '';}" onblur="if(this.value == ''){this.value = '<?php echo esc_attr($msg); ?>'}" />
								<?php do_action('lostpassword_form'); ?>
							</td>
						</tr>
						<tr>
							<td class="lwa-remember-buttons">
								<input type="submit" value="<?php _e("Get New Password", 'login-with-ajax'); ?>" class="lwa-button-remember" />
								<a href="#" class="lwa-links-remember-cancel"><?php _e("Cancel", 'login-with-ajax'); ?></a>
								<input type="hidden" name="login-with-ajax" value="remember" />
							</td>
						</tr>
					</table>
				</form>
			<?php endif; ?>
		</div>


		<?php if( get_option('users_can_register') && !empty($lwa_data['registration']) ): ?>
		<div class="lwa-register lwa-register-default lwa-modal" style="display:none;">
			<h4><?php esc_html_e('Register For This Site','login-with-ajax') ?></h4>
			<p><em class="lwa-register-tip"><?php esc_html_e('A password will be e-mailed to you.','login-with-ajax') ?></em></p>
			<form class="lwa-register-form" action="<?php echo esc_attr(LoginWithAjax::$url_register); ?>" method="post">
				<span class="lwa-status"></span>
				<p class="lwa-username">
					<label><?php esc_html_e('Username','login-with-ajax') ?><br />
					<input type="text" name="user_login" id="user_login" class="input" size="20" tabindex="10" /></label>
				</p>
				<p class="lwa-email">
					<label><?php esc_html_e('E-mail','login-with-ajax') ?><br />
					<input type="text" name="user_email" id="user_email" class="input" size="25" tabindex="20" /></label>
				</p>
				<?php do_action('register_form'); ?>
				<?php do_action('lwa_register_form'); ?>
				<p class="submit">
					<input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="<?php esc_attr_e('Register', 'login-with-ajax'); ?>" tabindex="100" />
				</p>
				<input type="hidden" name="login-with-ajax" value="register" />
			</form>
		</div>
		<?php endif; ?>
	</div>