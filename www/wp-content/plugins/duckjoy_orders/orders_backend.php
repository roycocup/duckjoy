<?php




function dj_bk_orders_plugin_menu() {
	add_menu_page( 'Duckjoy Orders', 'Duckjoy Orders', 'delete_pages', __FILE__, 'dj_bk_orders_render', null, 102 );
}
add_action( 'admin_menu', 'dj_bk_orders_plugin_menu');

function dj_bk_orders_enqueue($hook) {
	if($hook != "toplevel_page_duckjoy_orders/orders_backend") return false;
	wp_enqueue_script('bootstrap', 'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js', 'jquery');
	wp_enqueue_script('jquery-ui-dialog');
	wp_enqueue_script( 'pagination', get_stylesheet_directory_uri() . '/js/bootstrap-paginator.min.js', 'bootstrap');
	wp_enqueue_style('bootstrap_paginator', 'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css');
	wp_enqueue_style('jquery-ui-smooth', 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
}
add_action( 'admin_enqueue_scripts', 'dj_bk_orders_enqueue' );



function dj_bk_orders_render() {?>

<?php
//must check that the user has the required capability 
if (!current_user_can('delete_pages'))
{
	wp_die( __('You do not have sufficient permissions to access this page.', 'duckjoy_orders') );
}

//if we have a post
if( isset($_POST[ 'token' ]) && $_POST[ 'token' ] == 'token' ) {

	
}

$hidden_field_name = 'token';
$orders = new Orders_model();
$pickups = new Pickups_model();
$today = date('Y-m-d', time());
$all_orders = $orders->getAllUndeletedOrders();
?>

<style>
	td{ padding-left: 5px; padding-right: 5px;}
	.odd{background-color: #ccc;}
	.even{background-color: white;}
	.ui-dialog{ width: 800px;}
		.popups{display: none;}
	
	select{width:auto;}
</style>

<script>
	jQuery(document).ready(function($){

		$('td a').click(function(event) {
			var id = $(this).attr('id');

			var splited_id 	= id.split('_'); 
			var action 		= splited_id[0];
			var order_id 	= splited_id[1];
			

			if ( /details/i.test(action) ){
				popup_for_order(order_id);
			}

			if ( /confirm/i.test(action) ){
				var ok = confirm('<?php _e("Would you really want to send the email confirmation to the primary user?");?>');
				if(ok){
					//make an ajax call
					make_call('ajax_confirm_order', order_id);
					//on success show update
					show_update('<?php _e("Confirmation email sent");?>')
				}
			}

			if ( /resend/i.test(action) ){
				var ok = confirm('<?php _e("Would you really want to RESEND the email confirmation to the primary user?");?>');
				if(ok){
					//make an ajax call
					make_call('ajax_confirm_order', order_id);
					//on success show update
					show_update('<?php _e("RE-Confirmation email sent");?>')
				}
			}

			if ( /cancel/i.test(action) ){
				var ok = confirm('<?php _e("Would you really want to cancel this order");?>');
				if(ok){
					//make an ajax call
					make_call('ajax_cancel_order', order_id);
					//on success show update
					show_update('<?php _e("Order canceled.");?>')
				}
			}

			if ( /refund/i.test(action) ){
				var ok = confirm('<?php _e("Would you really want to refund this order?");?>');	
				if(ok){
					//make an ajax call
					make_call('ajax_refund_order', order_id);
					//on success show update
					show_update('<?php _e("Order refunded.");?>')
				}
			}

			function popup_for_order(order_id){
				$('#popup_'+order_id).dialog({
					modal: true,
					 width: 1024,
        			height: 500,
				});
			}

			function make_call(fname, params){
				var data = {function_name:fname, parameters:params};
				<?php $nonce = wp_create_nonce( 'duck_orders' ); ?>
				data.parameters = '<?php echo $nonce; ?>,' + order_id;
				data.action = 'duck_orders_ajax_getvars';
				$.post('/wp-admin/admin-ajax.php', data, function(response) {
					console.log(response);
					// this will make the array that I'm passing into an array again
					// response = JSON.parse(response);
				});
			}


			function show_update(msg){
				$('.wrap h2')
					.append("<div class='updated'><p>"+msg+"</p></div>")
					.fadeIn('slow')
					.delay(1000)
					.queue(function(next){
						$('.updated').fadeOut('slow').remove();
					})
			}

		}); //end of click order functions

	
		// pagination
		var options = {
			currentPage: 3,
			totalPages: 10,
			size: 'mini'
		}
		$('.pagination').bootstrapPaginator(options);

	});
</script>

<div class="wrap">
	<h2>Duckjoy Orders</h2>
	<div>
		<form method="post" action="">
			<input type="hidden" value="token" name="token">

			<table id='orders_table'>
				<thead>
					<td>Actions</td>
					<td>Order Id</td>
					<td>Order Code</td>
					<td>Source</td>
					<td>Primary Email</td>
					<td>Price</td>
					<td>Persons</td>
					<td># Rooms</td>
					<td># Single</td>
					<td># Double</td>
					<td>Departure</td>
					<td>Return</td>
					<td>Status</td>
					<td>Last Modified Date</td>
					<td>Refund</td>
				</thead>
				<?php  foreach ($all_orders as $key => $order) :?>
					<?php $details = $orders->getOrderDetails($order->order_id);?>

					<tr class="<?php echo ($key%2 == 0 ? 'odd' : 'even'); ?> ">
						<td>
							<a href="javascript:void(0)" id='details_<?php echo $order->order_id; ?>'>See Details</a><br>
							<?php if ($order->status!=Orders_model::STATUS_PENDING): ?>
								<?php if ($order->status!=Orders_model::STATUS_CONFIRMED && $order->status!=Orders_model::STATUS_COMPLETE): ?>
									<a href="javascript:void(0)" id='confirm_<?php echo $order->order_id; ?>'>Confirm</a><br>
								<?php endif;?>	
								<?php if ($order->status==Orders_model::STATUS_CONFIRMED || $order->status==Orders_model::STATUS_COMPLETE): ?>
									<a href="javascript:void(0)" id='resend_<?php echo $order->order_id; ?>'>Resend Confirmation</a><br>
								<?php endif;?>
								<a href="javascript:void(0)" id='refund_<?php echo $order->order_id; ?>'>Refund</a><br>
								<a href="javascript:void(0)" id='cancel_<?php echo $order->order_id; ?>'>Cancel</a><br>
							<?php endif; ?>
						</td>
						<td><?php echo $order->order_id; ?></td>
						<td><?php echo $order->order_code; ?></td>
						<td><?php echo $order->source; ?></td>
						<td><?php echo (!empty($details['primary']->email))? $details['primary']->email : _e("email not found"); ?></td>
						<td><?php echo $order->price; ?></td>
						<td><?php echo $details['order']->num_people; ?></td>
						<td><?php echo $details['order']->num_rooms; ?></td>
						<td><?php echo $details['order']->singles_number; ?></td>
						<td><?php echo $details['order']->doubles_number; ?></td>
						<td><?php echo date('Y-m-d', strtotime($order->departure_date)); ?></td>
						<td><?php echo date('Y-m-d', strtotime($order->return_date)); ?></td>
						<td><?php echo $order->status; ?></td>
						<td><?php echo $order->modified; ?></td>
						<td><?php echo ($order->refund_status)? ucwords($order->refund_status):''; ?></td>
					</tr>   
				<?php endforeach;?>
			</table>

			<!-- popups -->
			<?php  foreach ($all_orders as $key => $order) :?>
				<?php $details = $orders->getOrderDetails($order->order_id);?>
				<div class="popups" id="popup_<?php echo $order->order_id; ?>" title="Order Details">
					<div class="detail_table">
						<table border="1">
							<thead>
								<td>Order Id</td>
								<td>Transaction</td>
								<td>Offer Id</td>
								<td>Passengers</td>
								<td>People</td>
								<td>Couples</td>
								<td>Adults</td>
								<td>Students</td>
								<td>Children</td>
							</thead>
							<tr>
								<td><?php echo $order->order_id; ?></td>
								<td><?php echo (!empty($order->paypal_transaction_id))? $order->paypal_transaction_id:''; ?></td>
								<td><a href="<?php echo get_permalink( $details['order']->offer_id )?>" target="_blank"> <?php echo $details['order']->offer_id; ?></a></td>
								<td>
									<?php 
									$i = 1;
									foreach ($details['members'] as $member){
										echo ($member->primary == 1)? '(Primary) ':'';
										echo $member->first_name." ".$member->surname;
										if (count($details['members']) > 1 ) echo "</br>";
										$i++;
									}
									?>
								</td>
								<td><?php echo $details['order']->num_people; ?></td>
								<td><?php echo $order->num_couples; ?></td>
								<td><?php echo $order->doubles_adult + $order->singles_adult; ?></td>
								<td><?php echo $order->doubles_student + $order->singles_student; ?></td>
								<td><?php echo $order->doubles_children + $order->singles_children; ?></td>
							</tr>
						</table>
						<br>
						<?php if (!empty($order->pickup)) $pickup = $pickups->get_pickup_location_by_code($order->pickup); ?>
						<table border="1">
							<thead>
								<td>Visa</td>
								<td>Meeting Point</td>
							</thead>
							<tr>
								<td><?php echo ($order->visa == 'has_visa')? 'Has Visa': '<strong>No Visa<strong>'; ?></td>
								<td><?php echo (!empty($pickup))? $pickup->location." ".$pickup->time:'No Meeting Point'; ?></td>
							</tr>
						</table>
						<br>
						<table border="1">
							<thead>
								<td><strong>Primary Info</strong></td>
								<td>Email</td>
								<td>Phone</td>
								<td>Address</td>
								<td>Address 2</td>
								<td>Post Code</td>
								<td>Country</td>
							</thead>
							<tr>
								<td></td>
								<td><?php echo (!empty($details['primary']->email))? $details['primary']->email : _e("email not found"); ?></td>
								<td><?php echo (!empty($details['primary']->phone))? $details['primary']->phone : _e("phone not found"); ?></td>
								<td><?php echo (!empty($details['primary']->address1))? $details['primary']->address1 : _e("address not found"); ?></td>
								<td><?php echo (!empty($details['primary']->address2))? $details['primary']->address2 : '' ?></td>
								<td><?php echo (!empty($details['primary']->post_code))? $details['primary']->post_code : '' ?></td>
								<td><?php echo (!empty($details['primary']->country))? $details['primary']->country : '' ?></td>
							</tr>
						</table>
					</div>
				</div>
			<?php endforeach;?>

			<!-- <div class="pagination"></div> -->
			<?php //submit_button(); ?>
		</form> 
	</div>
</div>

<?php } 