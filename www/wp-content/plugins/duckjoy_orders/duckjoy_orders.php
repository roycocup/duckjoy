<?php

/**
*
* Plugin Name: Duckjoy Orders
* Provides: duck_db
* Description: A custom built plugin to enable orders to be put through to paypal
* Author: Rodrigo Dias
* Version: 1.0
* Author URI: http://rodderscode.co.uk
*
**/


require_once(dirname(__FILE__)."/models/orders.php");
require_once(dirname(__FILE__)."/helpers.php");
require_once(dirname(__FILE__)."/models/paypal.php");
require_once(dirname(__FILE__)."/models/emails.php");
require_once(dirname(__FILE__)."/models/pickups.php");
require_once(dirname(dirname(__FILE__))."/duckjoy_offers/models/offers.php");
require_once(dirname(dirname(__FILE__))."/duckjoy_orders/orders_backend.php");


add_action('init', 'duckorders_init');
function duckorders_init(){
	load_plugin_textdomain('duckjoy_orders', false, dirname( plugin_basename( __FILE__ ) ) . '/langs/');
	new DuckjoyOrders();
}

class DuckjoyOrders {

	public $post_type = 'duck_orders';
	const PLUGIN_NAME = 'duck_orders';

	public $confirm_page = 'orders/confirm';

	public function __construct(){
		$this->paypal = new Paypal();
		$this->orders = new Orders_model();
		$this->offers = new Offers_model();
		$this->emails = new Emails_model();
		$this->pickups = new Pickups_model();
		//start the session variables for multiple pages
		//if(!isset($_SESSION)) session_start();

		//page handling
		add_filter('the_content', array( $this, 'process_order'));
		add_filter('wp', array( $this, 'payup'));
		add_filter('wp', array( $this, 'paypal_return'));

		//ajax calls
		// if ( is_admin() ){
			add_action("wp_ajax_duck_orders_ajax_getvars", array($this, "duck_orders_ajax_getvars"));
			add_action("wp_ajax_nopriv_duck_orders_ajax_getvars", array($this, 'duck_orders_ajax_getvars'));
		// }
	}


	/**
	* Endpoint for all ajax calls
	*/
	public function duck_orders_ajax_getvars(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$function_name = @$_REQUEST['function_name'];
			$parameters = @$_REQUEST['parameters'];

			if (!empty($parameters)) {
				$result = $this->$function_name($parameters);	
			} else {
				$result = $this->$function_name();	
			}
			echo json_encode($result);
			exit();
		}
	}


	public function ajax_confirm_order($params){
		$params = explode(',', $params);
		$nonce = $params[0];
		$order_id = $params[1];
		if (wp_verify_nonce( $nonce, 'duck_orders')){
			//get the order info
			$primary = $this->orders->getPrimaryData($order_id);
			$order = $this->orders->getOrder($order_id);
			$offer = $this->offers->get_post_by_id($order->offer_id);
			$offer_details = $this->offers->add_data_to_post($offer);
			$details = $this->orders->getOrderDetails($order_id);
			$pickup_code = $details['order']->pickup;
			if (!empty($pickup_code)) $pickup = $this->pickups->get_pickup_location_by_code($pickup_code);
			$pickup = (!empty($pickup))? $pickup->location." @ ".$pickup->time : __('No Meeting Point', 'duckjoy_orders');
			$departure_date = date('Y-m-d', strtotime($order->departure_date));
			$return_date = date('Y-m-d', strtotime($order->return_date));
			

			//set the data
			$data = array(
				'name' 				=> $primary->surname." ".$primary->first_name,
				'price'				=> $order->price,
				'transaction_code' 	=> $order->paypal_transaction_id,
				'order_id'			=> $order->order_id,
				'order_code'		=> $order->order_code,
				'offer_code'		=> $offer_details->meta['offer_code'][0],
				'primary_passenger_email' => $primary->email,
				'travel_company'	=> $offer_details->taxonomies['company']->name,	
				'departure_time' 	=> $departure_date,
				'return_time'		=> $return_date,
				'pickup' 			=> $pickup,
				'total_price'		=> $order->price,
				'total_rooms'		=> $order->num_rooms,
				'num_doubles'		=> $details['order']->doubles_number,
				'num_singles'		=> $details['order']->singles_number,
				'members'			=> $details['members'],
				'other_requirement' => $order->notes,
			);
			//send email
			$this->emails->sendmail($primary->email, __('Confirmation', 'duckjoy_orders'), Emails_model::TEMPLATE_CONFIRM, $data);
			//change status to completed
			$this->orders->setStatus($order_id, Orders_model::STATUS_CONFIRMED);
			//send true
			return true;
		} 
		//if the nonce is not correct simply dont do anything. 
	}


	public function ajax_cancel_order($params){
		$params = explode(',', $params);
		$nonce = $params[0];
		$order_id = $params[1];
		if (wp_verify_nonce( $nonce, 'duck_orders')){
			//change status to canceled
			$this->orders->setStatus($order_id, Orders_model::STATUS_CANCELLED);
			//send true
			return true;
		} 
	}


	public function ajax_refund_order($params){
		$params = explode(',', $params);
		$nonce = $params[0];
		$order_id = $params[1];
		if (wp_verify_nonce( $nonce, 'duck_orders')){
			//get the order info
			$primary = $this->orders->getPrimaryData($order_id);
			$order = $this->orders->getOrder($order_id);
			$offer = $this->offers->get_post_by_id($order->offer_id);
			//set the data
			$data = array(
				'name' 			=> $primary->surname." ".$primary->first_name,
				'route_name'	=> $offer->post_title,
				'price'			=> $order->price,
				'transaction_code' => $order->paypal_transaction_id,
				'order_id'		=> $order->order_id,
				'order_code'	=> $order->order_code,
			);
			//send email
			$this->emails->sendmail($primary->email, __('Refund!', 'duckjoy_orders'), Emails_model::TEMPLATE_REFUND, $data);
			//change status to completed
			$this->orders->setRefundStatus($order_id, 'refunded');
			$this->orders->setStatus($order_id, Orders_model::STATUS_REFUNDED);
			//send true
			return true;
		} 
	}

	/**
	*
	* This will catch whats coming from the compare or group passenger info
	* All information should be in the session already. 
	* Lets just save the order and pass to paypal executions
	*
	**/
	public function process_order($content){
		global $pagename; 
		
		if (get_page_uri(get_the_ID()) == $this->confirm_page) {
			//the get comes from offers so that we know the action
			// this enables multiple actions from exterior modules
			// if he pushes back
			//go back and edit the passenger order
			//simple javascript back will do and catch the variables on the session


			//this must not execute if this is not the intended page
			// if it has confirm order or order id this is for paypal to take care of
			if (isset($_POST['confirm_order']) &&  isset($_POST['order_id']) ){
				return false;
			}

			//process the order
			$total_people = $_SESSION['order']['booking_details']['doubles_adult'] 
				+ $_SESSION['order']['booking_details']['doubles_student'] 
				+ $_SESSION['order']['booking_details']['doubles_children'] 
				+ $_SESSION['order']['booking_details']['singles_adult']
				+ $_SESSION['order']['booking_details']['singles_student']
				+ $_SESSION['order']['booking_details']['singles_children'];

			
			$passengers_data = $this->orders->process_passengers($_SESSION['order']['passenger_details']);

			$this->orders->offer_id 			= (int)$_SESSION['order']['booking_details']['post_id'];
			$this->orders->member_id 			= (int)$passengers_data['primary_member_id'];
			$this->orders->source 				= $_SESSION['order']['passenger_details']['source'];
			$this->orders->party_id 			= (int)$passengers_data['party_id'];
			$this->orders->price 				= (int)$_SESSION['order']['booking_details']['total_price'];
			$this->orders->num_people 			= (int)$total_people;
			$this->orders->num_rooms 			= (int)$_SESSION['order']['booking_details']['num_rooms'];
			$this->orders->doubles_adult 		= (int)$_SESSION['order']['booking_details']['doubles_adult'];
			$this->orders->doubles_student 		= (int)$_SESSION['order']['booking_details']['doubles_student'];
			$this->orders->doubles_children 	= (int)$_SESSION['order']['booking_details']['doubles_children'];
			$this->orders->singles_adult 		= (int)$_SESSION['order']['booking_details']['singles_adult'];
			$this->orders->singles_student 		= (int)$_SESSION['order']['booking_details']['singles_student'];
			$this->orders->singles_children 	= (int)$_SESSION['order']['booking_details']['singles_children'];
			$this->orders->doubles_number 		= (int)$_SESSION['order']['booking_details']['doubles_number'];
			$this->orders->singles_number 		= (int)$_SESSION['order']['booking_details']['singles_number'];
			$this->orders->num_couples 			= (int)$_SESSION['order']['passenger_details']['couples'];
			$this->orders->pickup 				= addslashes($_SESSION['order']['passenger_details']['pickup']);
			$this->orders->visa 				= addslashes($_SESSION['order']['passenger_details']['visa']);
			$this->orders->notes 				= addslashes($_SESSION['order']['passenger_details']['notes']);
			$this->orders->departure_date 		= $_SESSION['order']['booking_details']['departure_date'];
			$this->orders->return_date 			= $_SESSION['order']['booking_details']['return_date'];
			$this->orders->status 				= Orders_model::STATUS_PENDING;
			
			//save it
			//@fix me - Its saving the order every time it touches this page... so at least twice.
			if (empty($_GET)) {
				$order_id = $this->orders->save();

				//confirm
				$content = $this->get_content_for_confirm($order_id);
				
				//place a paypal button here via express checkout
				return $content;	
			}

			return false;

		}
	}

	/**
	*
	* Just returns the html for the confirmation page
	*
	**/
	public function get_content_for_confirm($order_id){


		$order = $this->orders->getOrder($order_id);
		$details = $this->orders->getOrderDetails($order->order_id);
		$visa = ($order->visa == 'has_visa')? __('Has Visa', 'duckjoy_orders'): '<strong>'.__("No Visa", 'duckjoy_orders').'<strong>';
		//pickup is actually meeting point. An error on the database naming... 
		$pickup_code = $details['order']->pickup;
		if (!empty($pickup_code)) $pickup = $this->pickups->get_pickup_location_by_code($pickup_code);
		$pickup = (!empty($pickup))? $pickup->location." @ ".$pickup->time : __('No Meeting Point', 'duckjoy_orders');
		$name = $details['primary']->surname." ".$details['primary']->first_name;
		$email = (!empty($details['primary']->email))? $details['primary']->email : _e("email not found", 'duckjoy_orders');
		$phone = (!empty($details['primary']->phone))? $details['primary']->phone : _e("phone not found", 'duckjoy_orders');
		$address1 = (!empty($details['primary']->address1))? $details['primary']->address1 : _e("address not found", 'duckjoy_orders');
		$address2 = (!empty($details['primary']->address2))? $details['primary']->address2 : '';
		$post_code = (!empty($details['primary']->post_code))? $details['primary']->post_code : '';
		$country = (!empty($details['primary']->country))? $details['primary']->country : '';
		$email = (!empty($details['primary']->email))? $details['primary']->email : _e("email not found", 'duckjoy_orders');
		$departure_date = date('Y-m-d', strtotime($order->departure_date));
		$return_date = date('Y-m-d', strtotime($order->return_date));
		$logo = get_stylesheet_directory_uri()."/img/logo_common.jpg";

		$output = "<style>
			thead{font-weight:bold;}
			<!-- main menu -->
			#login{font-size:16px; color:#fff; }
			#magnifier_glass{float:left;}
			.menu-logo{width:300px !important; height:40px !important;}
			.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url({$logo}) no-repeat center 0 !important;  }
			.menu-logo a:hover{border:none !important; display:block;}
			#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
			#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
			#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
			#login{margin-top:-50px;}
			.site-branding{margin-top:-20px !important;}


			<!-- button -->
			input[type='submit'].bootstrap-blue {
				background-color: hsl(206, 100%, 30%) !important;
				background-repeat: repeat-x;
				filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#0090ff', endColorstr='#005699');
				background-image: -khtml-gradient(linear, left top, left bottom, from(#0090ff), to(#005699));
				background-image: -moz-linear-gradient(top, #0090ff, #005699);
				background-image: -ms-linear-gradient(top, #0090ff, #005699);
				background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #0090ff), color-stop(100%, #005699));
				background-image: -webkit-linear-gradient(top, #0090ff, #005699);
				background-image: -o-linear-gradient(top, #0090ff, #005699);
				background-image: linear-gradient(#0090ff, #005699);
				border-color: #005699 #005699 hsl(206, 100%, 25%);
				color: #fff !important;
				text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.33);
				-webkit-font-smoothing: antialiased;
				padding:5px 10px;
			}
			
			
			td{border:2px solid #CCC; text-align:center;}
			h3{margin-left:10px;color: rgb(9,67,166);}
			
			
			
			
		</style>
		";


		$output .= "<h3>".__('Order', 'duckjoy_orders')."</h3>";

		$output .="
		<form action='' method='post'>
			<input type='hidden' name='confirm_order'>
			<input type='hidden' name='order_id' value='$order_id'>
			<input type='hidden' value='".wp_create_nonce( 'confirm' )."' name='nonce'>
			";

		$output .="
				<table>
					<thead>
						<td>".__('Order Code', 'duckjoy_orders')."</td>
						<td>".__('Price', 'duckjoy_orders')."</td>
						<td>".__('Persons', 'duckjoy_orders')."</td>
						<td>".__('# Rooms', 'duckjoy_orders')."</td>
						<td>".__('Singles', 'duckjoy_orders')."</td>
						<td>".__('Doubles', 'duckjoy_orders')."</td>
						<td>".__('Departure', 'duckjoy_orders')."</td>
						<td>".__('Return', 'duckjoy_orders')."</td>
					</thead>
					";


		$output .=" 
					<tr>
						<td>{$order->order_code}</td>
						<td>£{$order->price}</td>
						<td>{$details['order']->num_people}</td>
						<td>{$details['order']->num_rooms}</td>
						<td>{$details['order']->singles_number}</td>
						<td>{$details['order']->doubles_number}</td>
						<td>{$departure_date}</td>
						<td>{$return_date}</td>
					</tr>  
				</table>";

		
		$output .="
				<table>
					<thead>
						<td>".__('Visa', 'duckjoy_orders')."</td>
						<td>".__('Meeting Point', 'duckjoy_orders')."</td>
					</thead>
					<tr>
						<td>{$visa}</td>
						<td>$pickup</td>
					</tr>
				</table>
				";

			$output .= "<h3>".__('Primary Passenger', 'duckjoy_orders')."</h3>";
			$output .="
				<table>
					<thead>
						<td>".__('Name', 'duckjoy_orders')."</td>
						<td>".__('Email', 'duckjoy_orders')."</td>
						<td>".__('Phone', 'duckjoy_orders')."</td>
						<td>".__('Address', 'duckjoy_orders')."</td>
						<td>".__('Post Code', 'duckjoy_orders')."</td>
						<td>".__('Country', 'duckjoy_orders')."</td>
					</thead>
					<tr>
						<td>$name</td>
						<td>$email</td>
						<td>$phone</td>
						<td>$address1<br>$address2</td>
						<td>$post_code</td>
						<td>$country</td>
					</tr>
				</table>";

		if (count($details['members']) > 1){
			$output .= "<h3>".__('Passengers', 'duckjoy_orders')."</h3>";
			$output .="
					<table>
						<thead>
							<td>".__('Name', 'duckjoy_orders')."</td>						
							<td>".__('DOB', 'duckjoy_orders')."</td>
							<td>".__('Gender', 'duckjoy_orders')."</td>
						</thead>
						";

			foreach ($details['members'] as $member) {
				if (!empty($member->primary)) continue;
				$output .="
				<tr>
					<td>{$member->surname} {$member->first_name}</td>
					<td>{$member->dob}</td>
					<td>{$member->gender}</td>
				</tr>";
			}
						
			$output .="</table>";
		}

		// $output .= "<h3>Bookings</h3>";


		$backlink = get_permalink($details['offer']);
		$next_page = ($details['offer']->post_type == 'compares') ? 'compare_passenger_info' : 'group_passenger_info';
		
		$output .="
			<input type='submit' class='bootstrap-blue' value='".__('Submit', 'duckjoy_orders')."'>
		</form>
		<form action='{$backlink}' method='post'>
			<input type='hidden'  name='next_page' value='{$next_page}'>	
			<input type='submit' class='bootstrap-blue' value='".__('Back', 'duckjoy_orders')."'>
		</form>
		";

		return $output;
	}


	/**
	*
	* Catch the confirmation form from the confirmation time
	*
	**/
	public function payup(){	
		$cur_uri = get_page_uri(get_the_ID());
		if ($cur_uri == $this->confirm_page) {
			if (isset($_POST['confirm_order']) &&  isset($_POST['order_id']) ){

				if (!wp_verify_nonce( $_POST['nonce'], 'confirm')) {
					return false; 
				}

				//get the order
				$order = $this->orders->getOrder($_POST['order_id']);
				$price = $order->price;
				$offer_id = $order->offer_id;
				
				//get the offer
				$offer = $this->offers->get_post_by_id($this->orders->offer_id);
				$offer = $this->offers->add_data_to_post($offer);
				$title =  __("Holiday Package", 'duckjoy_orders');
				$description = $offer->post_title;
				$offer_code = $offer->meta['offer_code'][0];

				//specific to paypal
				$site_url = get_site_url();
				$return_url = "$site_url/{$cur_uri}/?paypal=accept&order_id=$order->order_id";
				$return_cancel_url = "$site_url/{$cur_uri}/?paypal=cancel&order_id=$order->order_id";


				//PAYPAL - make a new sale and get the token
				$response = $this->paypal->getSale();
				$paypal_orders_id = $this->orders->savePaypalToken($order->order_id, $response);

				//redirect the user to that	
				$token = $response->access_token;
				$response = $this->paypal->getRedirectLinks($token, $price, $title, $description, $offer_id, $return_url, $return_cancel_url);
				$this->orders->savePaypalResponse($order->order_id, $response, $paypal_orders_id); 

				$approval_link = $response->links[1]->href;
				wp_redirect( $approval_link );
				exit();
			}
		}
		
	}

	public function paypal_return(){

		//confirm that this is the correct page for this
		$cur_uri = get_page_uri(get_the_ID());
		if ($cur_uri == $this->confirm_page) {
			if(isset($_GET['paypal'])){
				if ($_GET['paypal'] == 'accept'){

					if (empty($_GET['token']) || empty($_GET['PayerID']) || empty($_GET['order_id']) ){
						setFlashMessage('error', __('There was a problem. Please contact us on our phone.', 'duckjoy_orders') );
						//debug_log("Major PROBLEM! One of these is empty: paypal_token: ".$_GET['token']." / payer_id: ".$_GET['PayerID']." / order_id: ".$_GET['order_id']."");
						wp_redirect('/');
						exit();
						return false;
					}

					//this is just approval
					$accept_token = $_GET['token'];
					$payer_id = $_GET['PayerID'];
					$order_id = $_GET['order_id'];
					$ok = $this->orders->savePaypalAcceptance($order_id, $accept_token, $payer_id);
					
					
					
					//execute the payment now
					$url = $this->orders->get_paypal_execution_url($order_id);
					$sale_token = $this->orders->get_token($order_id);
					
					$response = $this->paypal->execute($sale_token, $payer_id, $url);

					//@TODO
					if (!empty($response->name) && $response->name == "PAYMENT_STATE_INVALID"){
						//do something because this is fake call
						echo 'There was a problem with your transaction. Please contact us directly on '.get_bloginfo('admin_email');
						exit();
						// wp_safe_redirect('/error=paypal');
					} else {
						$ok = $this->orders->savePaypalSale($order_id, $response->links[0]->href, $response);
						//close the offer before  sending the final email (2step protection)... 
						$this->orders->setStatus($order_id, Orders_model::STATUS_UNCONFIRMED);
						// just send the thank you email
						//get the primary email and offer data
						$primary = $this->orders->getPrimaryData($order_id);
						$order = $this->orders->getOrder($order_id);
						$offer = $this->offers->get_post_by_id($order->offer_id);
						

						$data = array(
							'title'			=> $primary->title,
							'name' 			=> $primary->first_name." ".$primary->surname,
							'route_name'	=> $offer->post_title,
							'price'			=> $order->price,
							'transaction_code' => $order->paypal_transaction_id,
							'order_id'		=> $order->order_id,
							'order_code'	=> $order->order_code,
						);
						
						$this->emails->sendmail($primary->email, __('Thank you!', 'duckjoy_orders'), Emails_model::TEMPLATE_THANK_YOU, $data);
						//also send to agency
						$this->emails->sendmail(get_bloginfo('admin_email'), __('New Order in', 'duckjoy_orders'), Emails_model::TEMPLATE_ORDER_IN, $data);


						//go to thank you page
						$ty_page_url = get_site_url().'/thank-you';
						if ($ok) wp_redirect( $ty_page_url );
						exit();
					}
					
				} 


				if ($_GET['paypal'] == 'cancel'){
					//do something about the canceling
					//return to homepage
					setFlashMessage('error', __('Your paypal order was canceled.', 'duckjoy_orders') );
					wp_redirect('/');
				}
				
				//anithing else
				wp_redirect('/');

			}
		}
	}


} //end of class





