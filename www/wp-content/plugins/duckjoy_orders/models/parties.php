<?php

require_once ('db.php');

class Parties_model extends db{

	public $table = 'wp_duck_parties';
	

	public function Parties_model(){
		parent::__construct();
	}

	public function createParty($email){
		$data['party_name'] = $email;
		return $this->insert($data);
	}

	public function get_parties_for_member_id($member_id){
		//$party_id = $this->wpdb->get_results("SELECT party_id FROM {$this->table} WHERE member_id = {$member_id}");
		//return $party_id;
	}


	public function associateMembersToParty($party_id, $members = array()){
		foreach($members as $member_id){
			$data['party_id'] = $party_id;
			$data['member_id'] = $member_id;
			$this->insert($data, $this->party_members_table); 
		}

		return true;
	}

}