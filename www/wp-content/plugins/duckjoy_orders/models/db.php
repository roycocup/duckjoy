<?php

class db {

	public $table = '';
	public $wpdb;

	public $paypal_table 			= 'wp_duck_paypal_orders';
	public $members_table 			= 'wp_duck_members';
	public $party_members_table 	= 'wp_duck_party_members';
	public $parties_table 			= 'wp_duck_parties';
	public $templates_table 		= 'wp_duck_email_templates';
	public $cron_table 				= 'wp_duck_cron';
	public $reviews_table			= 'wp_duck_reviews';

	//offers
	public $itinerary_table 		= 'wp_duck_itinerary';
	public $countries_table 		= 'wp_duck_countries';
	public $availability_table 		= 'wp_duck_availability';
	public $cities_table			= 'wp_duck_cities';
	public $pickup_locations_table	= 'wp_duck_pickup_locations';
	public $posts_table				= 'wp_posts';


	//group and stuff
	public $group_itinerary_table 	= 'wp_duck_group_itinerary';
	public $group_countries_table 	= 'wp_duck_group_countries';
	public $group_cities_table		= 'wp_duck_group_cities';
	public $group_availability_table 		= 'wp_duck_availability';
	public $group_pickup_locations_table	= 'wp_duck_pickup_locations';




	public function __construct(){
		global $wpdb;
		$this->wpdb = $wpdb; 
	}


	/**
	*
	* The order of the parameters is inverted so that table does not have to specified
	* when inside an object as $this->table will be assumed by default
	*
	**/
	public function insert($data, $table = ''){
		$table = (empty($table)) ? $this->table : $table;
		$now = date('Y-m-d H:i:s', time());
		$modified_exists = $this->fieldExists($table, 'modified');
		$created_exists = $this->fieldExists($table, 'created');

		if ($created_exists) $data['created'] = $now;
		if ($modified_exists) $data['modified'] = $now;
		$ok = $this->wpdb->insert($table, $data);
		if ($ok) return $this->wpdb->insert_id;
		return $ok;
	}

	/**
	*
	* The order of the parameters is inverted so that table does not have to specified
	* when inside an object as $this->table will be assumed by default
	*
	**/
	public function update($data, $where, $table = ''){
		$table = (empty($table)) ? $this->table : $table;
		$now = date('Y-m-d H:i:s', time());
		$modified_exists = $this->fieldExists($table, 'modified');
		if ($modified_exists) $data['modified'] = $now;

		$ok = $this->wpdb->update($table, $data, $where);
		return $ok;
	}


	public function fieldExists($table, $field){
		$sql = "SELECT TABLE_NAME 
				FROM information_schema.COLUMNS 
				WHERE 
				TABLE_NAME = '{$table}' 
				AND COLUMN_NAME = '{$field}'";
		return $this->wpdb->get_var($sql);
	}

}