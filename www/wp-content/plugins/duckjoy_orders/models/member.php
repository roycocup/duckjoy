<?php 

require_once ('db.php');
include('parties.php');

class Members_model extends db{
	
	public $table = 'wp_duck_members';


	public function Members_model(){
		parent::__construct();
		$this->parties = new Parties_model(); 
	}



	public function process_primary($primary){
		//deal with the password
		if (!empty($primary['password']) 
			&& !empty($primary['cpassword']) 
			&& !empty($primary['email']) 
			&& !empty($primary['confirm']) ) {
			
			//he is passing a password
			//if he exists as a user
			$user_id = $this->checkUserExists($primary['email']);
			if ( !empty($user_id) ){
				//if he is a user, he is surely a member
				if ( $member_id = $this->getMemberByEmail($primary['email']) ){
					return $member_id;
				}
			//if user does not exist
			} else {
				debug_log("User is passing password and is already a member with id $user_id".__FILE__."::".__LINE__);
				//This is a new user and member
				$username = $primary['email'];
				$password = $primary['password'];
				$email = $primary['email'];
				wp_create_user( $username, $password, $email );
			}
		}

		

		//if we just created a user or not, he may already have been a member. Check it.
		if ( $member_id = $this->getMemberByEmail($primary['email']) ){
			debug_log("Member already exists and making new process information".__FILE__."::".__LINE__);
			return $member_id;
		}
		

		//Save this guy as primary 
		$primary = (object)$primary;
		$data['title'] 				= '';
		$data['first_name'] 		= $primary->first_name;
		$data['surname'] 			= $primary->surname;
		$y = $primary->dob['year'];
		$m = $primary->dob['month'];
		$d = $primary->dob['day'];
		$data['dob'] 				= date('Y-m-d',strtotime($y.'-'.$m.'-'.$d));
		$data['email'] 				= $primary->email;
		$data['type_passenger'] 	= $primary->type_passenger;
		$data['gender'] 			= $primary->gender;
		$data['country_phone_code'] = $primary->country_phone_code;
		$data['phone'] 				= $primary->phone;
		$data['address1'] 			= $primary->address1;
		$data['address2'] 			= $primary->address2;
		$data['post_code'] 			= $primary->post_code;
		$data['country'] 			= $primary->country;
		$data['primary'] 			= 1;

		$member_id = $this->insert($data);
		//maybe the member already exists
		if (!$member_id){
			debug_log('This member already exists when we just tried to create him...');
			$member_id = $this->wpdb->get_var("SELECT member_id FROM $this->table WHERE email = '{$primary->email}' ");	
		} 

		return $member_id;
	}


	public function process_passengers($members){

		foreach ($members as $member){
			$names = explode(' ', trim($member['name']));
			$data['first_name']		= $names[0];
			$data['surname'] = '';
			if (count($names) > 1){
				for ($i=1; $i < count($names); $i++) {
					$data['surname'] .= $names[$i]." ";
				}
			}
			$data['gender'] 		= $member['gender'];
			$data['type_passenger'] = $member['type_passenger'];
			$data['dob'] 			= date('Y-m-d', strtotime($member['dob']) );

			$id = $this->insert($data);
			$member_ids[] = $id;
		}
				
		return $member_ids;
	}



	public function updateMember($member_id, $data, $where){
		$where = array('member_id' => $member_id);
		$ok = $this->update($data, $where, $this->table);

		if($ok){
			return $this->insert_id();
		}else{
			//something has gone bad
			return $ok;	
		}
		

	}


	/**
	*
	* gets the user from the users table. Not members table.
	*
	**/
	public function checkUserExists($email){
		$id = $this->wpdb->get_var("SELECT ID FROM wp_users WHERE user_email = '{$email}'");
		return $id;
	}


	/**
	*
	* gets the member from the members table
	*
	**/
	public function getMemberByEmail($email){
		$id = $this->wpdb->get_var("SELECT member_id FROM {$this->table} WHERE email = '{$email}'");
		return $id;
	}


}