<?php

require_once('db.php');
require_once('member.php');
require_once('parties.php');
require_once('reviews.php');



class Orders_model extends db{


	const STATUS_PENDING 			= 'pending'; //right after creation but not payed
	const STATUS_UNCONFIRMED	 	= 'unconfirmed'; // The user has payed for this but its pending confirmation email.
	const STATUS_CONFIRMED	 		= 'confirmed'; // The user has payed, we have manually confirmed (final email sent) the order but the return date has not passed.
	const STATUS_COMPLETE 			= 'complete'; // User has payed, manually confirmed (email sent), the return date has past.
	const STATUS_FAILED 			= 'failed'; // Something went wrong 
	const STATUS_CANCELLED 			= 'cancelled'; // This order was cancelled by the agency (user can not cancel orders)
	const STATUS_REFUNDED			= 'refunded'; // Same as cancelled and the money was returned (agency only).
	const STATUS_DELETED			= 'deleted'; // Deleted is generally done as a soft delete foe cron about pending orders that need to be cleaned up

	public $table = 'wp_duck_orders';
	
	
	public $member;

	public $offer_id;
	public $member_id;
	public $source;
	public $party_id;
	public $price;
	public $num_people;
	public $doubles_adult;
	public $doubles_student;
	public $doubles_children;
	public $singles_adult;
	public $singles_student;
	public $singles_children;
	public $doubles_number;
	public $singles_number;
	public $num_couples;
	public $pickup; //pickup is actually meeting point. An error on the database naming... 
	public $visa;
	public $notes;
	public $departure_date;
	public $return_date;
	public $paypal_accept_token;
	public $paypal_payer_id;
	public $status;



	public function Orders_model(){
		parent::__construct();
		$this->member = new Members_model();
		$this->parties = new Parties_model();
		$this->offers = new Offers_model();
	}


	public function process_passengers($passenger_details){
		
		//they are ALL members but we need to handle them differently
		$primary_member_id = $this->member->process_primary($passenger_details['primary']);
		
		$member_ids = array();
		if (!empty($passenger_details['passenger'])){
			$member_ids = $this->member->process_passengers($passenger_details['passenger']);	
		}

		//create a party for this first and get the id number
		//add the primary id to the list of members
		$all_members_ids = $member_ids;
		$all_members_ids[] = $primary_member_id; 

		$party_id = $this->parties->createParty($passenger_details['primary']['email']);
		//there wont be any ids in here. All there is is ok from the insert
		$result = $this->parties->associateMembersToParty($party_id, $all_members_ids);
		
		if (!$result) return false ;
		return array(
			'member_ids'=>$member_ids,
			'primary_member_id'=>$primary_member_id,
			'party_id'=>$party_id
		); 
	}

	


	public function save(){

		$data['order_code'] 		= strtoupper(substr(md5(time()), 0,5)."-".date('Y-m', time()));
		$data['offer_id'] 			= $this->offer_id;
		$data['source'] 			= $this->source;
		$data['member_id'] 			= $this->member_id;
		$data['party_id'] 			= $this->party_id;
		$data['price'] 				= $this->price;
		$data['num_people'] 		= $this->num_people;
		$data['num_rooms'] 			= $this->num_rooms;
		$data['doubles_adult'] 		= $this->doubles_adult;
		$data['doubles_student'] 	= $this->doubles_student;
		$data['doubles_children'] 	= $this->doubles_children;
		$data['singles_adult'] 		= $this->singles_adult;
		$data['singles_student'] 	= $this->singles_student;
		$data['singles_children'] 	= $this->singles_children;
		$data['doubles_number'] 	= $this->doubles_number;
		$data['singles_number'] 	= $this->singles_number;
		$data['num_couples'] 		= $this->num_couples;
		$data['pickup'] 			= $this->pickup; //pickup is actually meeting point. An error on the database naming... 
		$data['visa'] 				= $this->visa;
		$data['notes'] 				= $this->notes;
		$data['departure_date'] 	= $this->departure_date;
		$data['return_date'] 		= $this->return_date;
		$data['status'] 			= $this->status;

		$id = $this->insert($data);
		return $id;

	}

	public function getOrder($order_id){
		$sql = "SELECT * FROM {$this->table} WHERE order_id = {$order_id}";
		$order = $this->wpdb->get_row($sql);
		return $order;
	}

	/**
	*
	* Get all orders
	* @param - from is the date from which to get the orders from
	*
	**/
	public function getAllOrders($from = ''){
		$sql = "SELECT * FROM {$this->table} WHERE 1";
		if (!empty($from)) $sql .= " AND created > '$from' ";
		$sql .= " ORDER BY order_id DESC ";

		$orders = $this->wpdb->get_results($sql);
		return $orders;
	}

	/**
	*
	* Get all orders that are not deleted
	*
	**/
	public function getAllUndeletedOrders(){
		$deleted = self::STATUS_DELETED;
		$sql = "SELECT * FROM {$this->table} WHERE 1";
		if (!empty($from)) $sql .= " AND created > '$from' ";
		$sql .= " AND `status` != '{$deleted}' ";
		$sql .= " ORDER BY order_id DESC ";

		$orders = $this->wpdb->get_results($sql);
		return $orders;	
	}


	public function getOrdersForMemberId($member_id){
		$sql = "SELECT * FROM {$this->table} WHERE member_id = $member_id ORDER BY order_id DESC";
		return $this->wpdb->get_results($sql);
	}

	public function getOrderDetails($order_id){
		$res = array();
		//get the full order
		$res['order'] = $this->getOrder($order_id);
		//get the members
		$res['members'] = $this->getMembers($order_id);
		//get the primary
		$res['primary'] = $this->getPrimaryData($order_id);
		//get the offer details
		if (!empty($res['order']->offer_id)){
			$res['offer'] = $this->getOfferData($res['order']->offer_id);
		}

		return $res;
	}

	public function getOfferData($offer_id){
		$post = $this->offers->get_post_by_id($offer_id); 
		return $post;
	}

	public function getOrdersIdForPost($post_id){
		$sql = "SELECT *
				FROM wp_duck_orders o
				WHERE o.offer_id = {$post_id}";
		return $this->wpdb->get_results($sql);
	}

	/**
	*
	* How many orders where bought for post id
	*
	**/
	public function getBoughtOrdersForPostId($post_id){
		$unconfirmed = self::STATUS_UNCONFIRMED;
		$confirmed = self::STATUS_CONFIRMED;
		$complete = self::STATUS_COMPLETE;

		$sql = "SELECT count(*) FROM {$this->table} WHERE offer_id = {$post_id} 
		AND source = 'group' AND status in ('{$unconfirmed}', '{$confirmed}') ";
		return $this->wpdb->get_var($sql);
	}

	/**
	*
	* How many party members bought this post_id
	*
	**/
	public function getBoughtPartyMembersForPostId($post_id){
		$unconfirmed = self::STATUS_UNCONFIRMED;
		$confirmed = self::STATUS_CONFIRMED;
		$complete = self::STATUS_COMPLETE;

		$sql = "SELECT COUNT(*)
				FROM wp_duck_orders o
				JOIN wp_duck_party_members pm ON o.party_id = pm.party_id
				WHERE offer_id = {$post_id} AND source = 'group' AND STATUS IN ('unconfirmed', 'confirmed') ";
		return $this->wpdb->get_var($sql);
	}
	
	
	public function setStatus($id, $status){
		$data['status'] = $status;
		$where['order_id'] = $id;
		$this->update($data, $where);
	}

	public function getPrimaryData($order_id){
		$sql = "SELECT t2.* FROM {$this->table} t1
		inner join {$this->members_table} t2 on t2.member_id = t1.member_id
		WHERE order_id = {$order_id} ";
		$result = $this->wpdb->get_row($sql);
		return $result;
	}	

	public function getMembers($order_id){
		$sql = "SELECT m.* FROM {$this->table} o
			JOIN {$this->parties_table} p ON p.party_id = o.party_id
			JOIN {$this->party_members_table} pm ON pm.party_id = p.party_id
			JOIN {$this->members_table} m ON m.member_id IN (pm.member_id)
			WHERE o.order_id = {$order_id}";
		$result = $this->wpdb->get_results($sql);
		return $result;
	}

	public function getPickup($pickup_code){
		$sql = "SELECT * FROM {$this->pickup_locations_table} t1 WHERE pickup_code = {$pickup_code}";
		$result = $this->wpdb->get_row($sql);
		return $result;
	}


	/**
	*
	* Saves all the information for the sale token request
	*
	**/
	public function savePaypalToken($order_id, $response){
		$data = array(
			'order_id' 			=> $order_id,
			'sale_token' 		=> $response->access_token,
			'token_response' 	=> json_encode($response)
		);
		$id = $this->insert($data, $this->paypal_table );
		return $id;
	}


	public function savePaypalResponse($order_id, $response){
		$data = array(
			'sale_app_link' 	=> $response->links[1]->href,
			'sale_execute_link' => $response->links[2]->href, 
			'sale_response' => json_encode($response)
		);
		$where = array('order_id' => $order_id); 
		$id = $this->update($data, $where, $this->paypal_table);
		return $id;
	}

	public function savePaypalAcceptance($order_id, $accept_token, $payer_id){
		$data = array(
			'accept_token' 	=> $accept_token,
			'payer_id' => $payer_id
		);
		$where = array('order_id' => $order_id); 
		$id = $this->update($data, $where, $this->paypal_table);
		return $id;	
	}


	public function savePaypalSale($order_id, $sale_response_url, $response){
		$data = array(
			'sale_response_url' 	=> $sale_response_url,
			'accept_response' 		=> json_encode($response),
			'transaction_id'		=> $response->transactions[0]->related_resources[0]->sale->id,
		);
		//update the orders table with the transaction_id
		$this->update(array('paypal_transaction_id'=>$data['transaction_id']), array('order_id' => $order_id), $this->table);
		//delete the token from paypal table
		$this->update(array('sale_token'=>'', 'token_response'=>''), array('order_id'=>$order_id), $this->paypal_table);
		//update the paypal orders
		$where = array('order_id' => $order_id); 
		$id = $this->update($data, $where, $this->paypal_table);
		return $id;
	}


	public function get_paypal_execution_url($order_id){
		$sql = "SELECT sale_execute_link FROM $this->paypal_table WHERE order_id = $order_id";
		$var = $this->wpdb->get_var($sql);
		return $var; 
	}

	public function get_token($order_id){
		$sql = "SELECT sale_token FROM $this->paypal_table WHERE order_id = $order_id";
		$var = $this->wpdb->get_var($sql);
		return $var; 	
	}

	public function setRefundStatus($id, $status){
		$data['refund_status'] = $status;
		$where['order_id'] = $id;
		$this->update($data, $where);
	}


	


}