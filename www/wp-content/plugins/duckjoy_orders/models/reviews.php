<?php 

require_once('db.php');
require_once('orders.php');


class Reviews_model extends db{


	public $table = 'wp_duck_reviews';
	
	
	public function Reviews_model(){
		parent::__construct();
		$this->orders = new Orders_model();
	}


	public function getReviewsForOrderId($order_id){
		$sql = "SELECT * FROM {$this->table} WHERE order_id = {$order_id}";
		$reviews = $this->wpdb->get_results($sql);
		return $reviews;
	}


	public function getReviewsForOrderByUserId($order_id, $user_id){
		$sql = "SELECT * FROM {$this->table} WHERE order_id = {$order_id} AND user_id = {$user_id}";
		$review = $this->wpdb->get_row($sql);
		return $review;	
	}

	public function save($order_id, $user_id, $rate, $review){
		$data['order_id'] 	= $order_id;
		$order = $this->orders->getOrder($order_id);
		$data['offer_id']	= $order->offer_id;
		$data['user_id'] 	= $user_id;
		$data['rate'] 		= $rate;
		$data['review'] 	= $review;

		$exists = $this->getReviewsForOrderByUserId($order_id, $user_id);

		if ($exists){
			$where = array('order_id'=>$order_id, 'user_id'=>$user_id);
			$this->update($data, $where, $this->table);
		} else {
			$this->insert($data, $this->table);
		}
	}


	public function getReviewsForOfferId($offer_id){
		$sql = "SELECT * FROM {$this->table} WHERE offer_id = {$offer_id}";
		return $this->wpdb->get_results($sql);
	}

	public function getAverageRatingForOfferId($offer_id){
		$rs = $this->getReviewsForOfferId($offer_id);
		foreach ($rs as $review) {
			$ratings[] = $review->rate;
		}
		if (!empty($ratings)){
			$num_ratings = count($ratings);
			$result =  array_sum($ratings) / $num_ratings;	
		} else {
			$result = '--';
		}
		
		return $result;

	}


	

}

?>