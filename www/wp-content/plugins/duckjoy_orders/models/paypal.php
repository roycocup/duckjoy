<?php

require_once('db.php');

class Paypal extends db{



	//mine
	public $user = "";
	public $secret = "";

	public $table = 'wp_duck_paypal_orders';
	public $credentials_file = '';

	public function Paypal(){
		$this->credentials_file = dirname(__FILE__)."/paypal.ini";		
		if (file_exists($this->credentials_file)){
			$cred = parse_ini_file($this->credentials_file, true);	
			$this->user = $cred['mine']['user'];
			$this->secret = $cred['mine']['secret'];
		}else{
			$this->user = 'ATc9zBDBgkA-SqBMTSfoJVFuXjCgqoFpcnRSpM5cSJCcjLxY-YSmXjG4jmvj';
			$this->secret = 'EGQa1BAGpgJZmcRGwSEqmTZN3zhtNVOJD7carHXxWcN1OliKZ9TDxScR-Upb';
		}
	}

	public function getSale(){
		$domain = "https://api.sandbox.paypal.com/v1/oauth2/token";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, "Accept: application/json");
		curl_setopt($ch, CURLOPT_HEADER, "Accept-Language: en_US");
		curl_setopt($ch, CURLOPT_USERPWD, $this->user.":".$this->secret);
		curl_setopt($ch, CURLOPT_URL, $domain); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

		$response = json_decode(curl_exec($ch));
		curl_close($ch);

		return $response;
	}


	public function getRedirectLinks($token, $price, $title, $description, $offer_id, $return_url, $return_cancel_url){
		$domain = "https://api.sandbox.paypal.com/v1/payments/payment";

		$data = '{
					"intent": "sale",
					"payer": {
						"payment_method": "paypal"
					},
					"redirect_urls": {
						"return_url": "'.$return_url.'",
						"cancel_url": "'.$return_cancel_url.'"
					},
					"transactions": [
						{
							"amount": {
								"total": "'.$price.'",
								"currency": "GBP",
								"details": {
									"subtotal": "'.$price.'",
									"tax": "0.00",
									"shipping": "0.00"
								}
							},
							"description": "'.$description.'",
							"item_list": { 
								"items":[
									{
										"quantity":"1", 
										"name":"'.$title.'", 
										"price":"'.$price.'",  
										"sku":"'.$offer_id.'", 
										"currency":"GBP"
									}
								]
							}
						}
					]
				}';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $domain); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(  
		  "Content-Type:application/json",
		  "Authorization:Bearer ".$token)
		);
		$response = json_decode(curl_exec($ch));
		curl_close($ch);

		return $response;

	}


	public function execute($token, $payer_id, $domain){
		
		// $data['payer_id'] = $payer_id;		
		$data = '{"payer_id":"'.$payer_id.'"}';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $domain); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(  
		  "Content-Type:application/json",
		  "Authorization:Bearer ".$token)
		);
		$response = json_decode(curl_exec($ch));
		curl_close($ch);

		return $response;

	}



	public function confirmPayment(){
		$data = array(
			'user' => ''
		);
		//https://developer.paypal.com/webapps/developer/docs/integration/web/accept-paypal-payment/
		//https://developer.paypal.com/webapps/developer/docs/api/#look-up-a-sale
	}



}

