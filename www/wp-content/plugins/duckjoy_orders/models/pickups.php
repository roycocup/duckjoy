<?php


require_once('db.php');


class Pickups_model extends db{


	
	public $table = 'wp_duck_pickup_locations';
	

	public function __contruct(){
		parent::__construct();
	}


	public function get_pickup_location_by_code($code){
		$sql = "SELECT * FROM {$this->table} WHERE pickup_code = {$code}";
		$result = $this->wpdb->get_row($sql);

		return $result;
	}

}
