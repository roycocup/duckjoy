<?php 
	/**
	*	
	* Template for this is group list
	*
	**/
	?>

	<style>
		
		#login{font-size:16px !important; color:#fff !important;}
		#magnifier_glass{float:left;}
		.menu-logo{width:300px !important; height:40px !important;}
		.menu-logo a{width:300px !important; height:40px !important; text-indent:-999em !important;  display:block;  background:url(<?php bloginfo( 'stylesheet_directory' ); ?>/img/logo_common.jpg) no-repeat center 0 !important;  }
		.menu-logo a:hover{border:none !important; display:block;}
		#masthead.site-header .navbar .main-navigation .nav-menu li {display: inline;float: left;}
		#masthead.site-header .navbar .main-navigation .nav-menu li a {padding: 10px 16px !important;}
		#searchbox input {height: 22px !important;width: 145px !important;float:left;border: 2px #fff !important;color:#000 !important;background:none; margin:0;}
		


		
	
		/*ribbon*/		
		.page-template-group-template-php .ribbon:before {content: ' ';position: absolute;width: 2px;height: 0;top: 45px;border-top-width: 5px;
		border-right-width: 24px;border-bottom-width: 12px;border-left-width: 24px;border-top-style: solid;border-right-style: solid;border-bottom-style: solid;
		border-left-style: solid;border-top-color: rgb(234,122,106);border-right-color: rgb(234,122,106);border-bottom-color: transparent;border-left-color: rgb(234,122,106);left:0;}
		.page-template-group-template-php .ribbon:after {content: ' ';position: absolute;width: 0;height: 0;left: -10px;
		top: 0px;border-top-width: 10px;border-right-width: 5px;border-bottom-width: 10px;border-left-width: 5px;border-top-style: solid;border-right-style: solid;
		border-bottom-style: solid;border-left-style: solid;border-top-color: transparent;border-right-color: rgb(129,37,40);border-bottom-color: rgb(129,37,40);border-left-color: transparent;}
		.page-template-group-template-php .ribbon {position: relative;width: 50px;
		background-color: rgb(237,124,108);-webkit-box-shadow: 0px 2px 4px #888;-moz-box-shadow: 0px 2px 4px #888;box-shadow: 0px 2px 4px #888;margin-top: -20px;
		margin-left: 10px;height: 45px;float: left;position: relative;display: inline;margin-right: 261px;margin-bottom:0 !important;
		z-index: 9999999;font-size:17px !important;color: rgb(255,255,255) !important;text-align: center;vertical-align: middle;padding-top: 16px;font-weight:bold;}
		.page-template-group-template-php #ribbonwrap {margin-top: -700px;position: absolute;}


		.page-template-group-template-php .infobox h2 {margin-left: 75px;font-size: 16px;margin-top: -20px;margin-bottom: 0 !important;float: left;font-weight: lighter;}
		.page-template-group-template-php .infobox h2 a {color:#fff; text-decoration:none;}
		.page-template-group-template-php .infoboxpcolor {color: rgb(237,124,108) !important;}
		.page-template-group-template-php .price_save {margin-top: 0;margin-left: 77px;font-size: 12px;color: #fff;float: left;}
		.page-template-group-template-php .arrowcolor{float:right; margin:-7px 10px  0;}
		.page-template-group-template-php .arrowcolor a {color: rgb(108,197,186) !important;text-decoration: none;font-size: 20px !important;font-weight: bold !important;}
		.page-template-group-template-php .arrowcolor a:hover {color: rgb(108,197,186) !important;text-decoration:underline;}
		.page-template-group-template-php .price_old {margin-top:0;margin-left:20px;font-size: 12px;color:#fff;float:left;}
		.page-template-group-template-php .content-side-wrapper {overflow:hidden !important;}

		
		/*content elements*/
		.blue{color: rgb(123,190,196) }
		.page-title{margin-top:35px; font-size: 24px; color: rgb(51,51,51);font-weight: bold; width: 100%; font-family: "Yuppy SC Regular"}
		.content-side-wrapper{padding-left: 40px;}
			.deal{position:relative; float:left; width: 300px; height: 300px; background-color: #ccc; margin: 30px 20px 0 0; }
			.deal_link {position: absolute;top: 40px;left: 0;width: 100%;height: 212px;}
				.timeend{float: left;width: 300px;height: 50px;position: relative;text-align: center; padding-top: 5px;
							font-size: 24px;font-weight: bold;color: rgb(255,255,255); display: inline;background-color: rgba(123,190,196,1);
							-moz-box-shadow: -5px -5px 5px #888;-webkit-box-shadow: -5px -5px 5px #888;
							box-shadow: -5px -5px 5px #888;-moz-border-radius: 10px 10px / 10px 10px;border-radius: 10px 10px / 10px 10px;
							margin: -10px 21px 0 0}
				.infobox{float: left;width: 300px;height: 50px; padding-bottom: 30px; display: inline;background-color: rgba(80,80,80,1);
							z-index: 999999;color: rgba(255,255,255,1);font-size: 14px;margin:211px 21px 0 0 }
				.ribbon{width: 50px;background-color: rgb(237,124,108);-webkit-box-shadow: 0px 2px 4px #888;-moz-box-shadow: 0px 2px 4px #888;
							box-shadow: 0px 2px 4px #888;margin-top: -20px;margin-left: 10px;height: 45px;float: left;position: relative;
							display: inline;margin-right: 261px;margin-bottom: 295px;z-index: 9999999;color: rgba(255,255,255,1);font-size: 14px;}
		.pagination{float: left; clear: both; font-size: 16px; margin-top: 30px;}

	</style>

	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/group.js"></script>

	<?php $model = new Groups_model();?>

	<div id="left-side">
		<div class="content-side-wrapper">
			<div class="page-title">
				<span class="blue">团购：</span>
				<span>来一次说走就走的旅行</span>
			</div>
			
			<?php 
			$paged = get_query_var('paged') ? get_query_var('paged') : 1;
			$args = array('posts_per_page'=> 9,'offset'=> 0,'orderby'=> 'post_date','order'=> 'ASC','post_type'=> 'groups','post_status'=> 'publish', 'paged'=>$paged);
			$the_query = new WP_Query( $args ); 
			?>

			<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php $post = $the_query->post; ?>
					
					<?php 
						
						$all_meta = get_post_custom(get_the_ID());
						$prices = unserialize(unserialize($all_meta['prices'][0]));
						$best_price = $prices['double'];
						$used_to_be_price = get_field('used_to_be_price'); 
						$diff = abs($best_price - $used_to_be_price);

						if (!empty($best_price) && !empty($used_to_be_price) ){
							$diff_percentage = round(abs((($best_price * 100) / $used_to_be_price)-100)) ;
						} else {
							$diff_percentage = '0';
						}
					?>	

					<div class="deal" style="background-image:url('<?php echo the_field('list_image'); ?>')" >
						<div class="timeend" value="<?php echo format_js_date(get_post_meta( $post->ID, 'end_date', true )); ?>"></div> <br>
						<a href="<?php the_permalink(); ?>" class="deal_link"> &nbsp; </a>
                        <div class="infobox">
							<div class="ribbon"><?php echo $best_price; ?>镑</div>
							<h2><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h2>
							<div class="priceinfobox"> 
								<span class="price_save">节省: <span class="infoboxpcolor"><?php echo $diff."磅"; ?></span> </span>
								<span class="price_old">原价: <?php echo $used_to_be_price; ?>磅</span>
								<span class="arrowcolor"><a href="<?php the_permalink(); ?>">&gt;</a></span>
							</div>
						</div>
					</div>


				<?php endwhile; ?>

				<div class="pagination">
					<?php
						$big = 999999999; // need an unlikely integer

						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'prev_text'    => __('« Previous', 'duckjoy'),
							'next_text'    => __('Next »', 'duckjoy'),
							'total' => $the_query->max_num_pages
						) );
					?>
				</div>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>
			
		</div> <!-- content-side-wrapper -->
	</div> <!-- left-side -->

		<div class="clearfix"></div>
