<?php

/**
*
* Plugin Name: Duckjoy Group
* Depends: Duckjoy-Sys, duck_db
* Description: A custom built plugin to enable Group Holiday Packages to be created and displayed as a list of own entities in wordpress
* Author: Rodrigo Dias
* Version: 1.0
* Author URI: http://rodderscode.co.uk
*
**/

//require_once(dirname(__FILE__)."/helpers.php");
require_once(dirname(__FILE__)."/models/groups.php");
//require_once(dirname(dirname(__FILE__))."/duckjoy_orders/models/orders.php");

add_action('init', 'duckgroups_init');
function duckgroups_init(){
	load_plugin_textdomain('duckjoy_group', false, dirname( plugin_basename( __FILE__ ) ) . '/langs/');
	new DuckjoyGroups();
}

class DuckjoyGroups {

	public $post_type = 'groups';
	const PLUGIN_NAME = 'groups';

	public $itinerary_table 			= 'wp_duck_group_itinerary';
	public $countries_table 			= 'wp_duck_group_countries';
	public $cities_table				= 'wp_duck_group_cities';
	public $availability_table 			= 'wp_duck_availability';
	public $pickup_locations_table		= 'wp_duck_pickup_locations';

	//pages
	public $passenger_info = 'passenger_info';

	public function __construct(){
		//start the session variables for multiple pages
		//if(!isset($_SESSION)) session_start();

		//taxonomies and metaboxes
		$this->register_post_type();
		// $this->taxonomies();
		add_action( 'add_meta_boxes', array( $this, 'meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save' ) );
		add_action( 'admin_menu', array( $this, 'remove_metaboxes' ));

		

		//page handling
		add_filter('wp', array( $this, 'process_order_info'));
		add_filter('wp', array( $this, 'process_passenger_info'));
		add_filter('the_content', array( $this, 'duck_group_list'));

		
		$this->model = new Groups_model();
	}




	/**
	*
	* Processes the search and results pages
	*
	**/
	public function duck_group_list($content){
		if (!is_page_template('group-template.php')){
			return $content;
		}

		include('views/list-page.php');
		return $content; 	
		
			
	}


	/*==================================================
	=            Forms and pages Processing            =
	==================================================*/
	
	
	public function process_passenger_info($content){

		if ( !is_single() ){
			return false;  
		}

		/**
		*
		* This is for passenger info
		*
		**/
		//this form coming back?
		if (isset($_POST['group_passenger_info'])){
			
			//validate the nonce from passenger info
			if (!wp_verify_nonce($_POST['nonce'], 'group_passenger_info')){
				error_log(__FILE__ . ": Failed the nonce.". __LINE__);
				debug_log('failed nonce on '. __FILE__.":".__LINE__);
				die();
			}
			
			//validate the form posts
			if (!is_user_logged_in()){
				//the passwords only have to be validated if the user decides to cretae a pasword.
				//Users can buy without being registered as members
				if (!empty($_POST['primary']['password']) ||  !empty($_POST['primary']['cpassword'])) {
					//passwords must match
					if ($_POST['primary']['password'] !== $_POST['primary']['cpassword']){
						$_POST['messages'][] = __("Passwords do not match.", 'duckjoy_group');	
					}
					//passwords must be bigger than 6 characters
					if (strlen($_POST['primary']['password']) < 6){
						$_POST['messages'][] = __("Password should be at least 6 characters long.", 'duckjoy_group');	
					}
				}
			}
			
			//primary title must not be empty
			
			//primary surname must not be empty
			if (empty($_POST['primary']['surname']) ){
				$_POST['messages'][] = __("Primary passenger surname must not be empty.", 'duckjoy_group');	
			}
			//primary first_name must not be empty
			if (empty($_POST['primary']['first_name']) ){
				$_POST['messages'][] = __("Primary passenger name must not be empty.", 'duckjoy_group');	
			}
			//primary dob must not be empty
			//primary email must not be empty
			//primary email must be valid email
			if (!is_email($_POST['primary']['email']) ){
				$_POST['messages'][] = __("Primary passenger does not have a valid email.", 'duckjoy_group');		
			}

			//confirm email must not be empty
			if (empty($_POST['primary']['confirm'])){
				$_POST['messages'][] = __("Primary passenger needs to confirm email.", 'duckjoy_group');		
			}

			//primary email must match 
			if ($_POST['primary']['email'] !== $_POST['primary']['confirm']){
				$_POST['messages'][] = __("Emails do not match.", 'duckjoy_group');		
			}

			//primary type_passenger must not be empty
			//primary address1 must not be empty
			if (empty($_POST['primary']['address1']) ){
				$_POST['messages'][] = __("Please fill your first line of address.", 'duckjoy_group');
			}
			//primary country must not be empty
			if (empty($_POST['primary']['country'])){
				$_POST['messages'][] = __("Please fill your country of residence.", 'duckjoy_group');	
			}
			//primary phone must not be empty
			if (empty($_POST['primary']['phone'])){
				$_POST['messages'][] = __("A phone number is required.", 'duckjoy_group');	
			}
			//primary phone must be a number
			if (!is_numeric($_POST['primary']['phone'])){
				$_POST['messages'][] = __("Primary phone number must be a number.", 'duckjoy_group');	
			}
			

            //number of filled passengers should match total_people -1 

			//number of couples should not be greater than total_people/2 
			if (!empty($_POST['passenger'])){
				foreach ($_POST['passenger'] as $passenger){
					if (empty($passenger['name']) || empty($passenger['dob']) ){
						$_POST['messages'][] = __("Other passengers need a name and a date of birth.", 'duckjoy_group');	
					}
				}
			}
			
			//the number of passengers should be correct

			//the passengers should have all fields filled
			//we should be expecting x number of adults, y number of children, z number of students

            //visa
			if (empty($_POST['visa'])){
				$_POST['messages'][] = __("Please select a visa option.", 'duckjoy_group');		
			}

			//t&c aggreement
			if (empty($_POST['t&c'])){
				$_POST['messages'][] = __("Must agree with the terms and conditions.", 'duckjoy_group');		
			}

			//passengers
			$total_rooms = $_SESSION['group_form_vars']['total_rooms'];
			$total_people = $_SESSION['group_form_vars']['doubles_adult'] + 
					$_SESSION['group_form_vars']['doubles_student'] +
					$_SESSION['group_form_vars']['doubles_children'] +
					$_SESSION['group_form_vars']['singles_adult'] +
					$_SESSION['group_form_vars']['singles_student'] +
					$_SESSION['group_form_vars']['singles_children'] ;

			$single_rooms = $_SESSION['group_form_vars']['singles_number'];
			$double_rooms = $_SESSION['group_form_vars']['doubles_number'];
			$num_adults = $_SESSION['group_form_vars']['doubles_adult'] + $_SESSION['group_form_vars']['singles_adult'];
			$num_children = $_SESSION['group_form_vars']['doubles_children'] + $_SESSION['group_form_vars']['singles_children'] ;
			$num_students = $_SESSION['group_form_vars']['singles_student'] + $_SESSION['group_form_vars']['doubles_student'];


			// $_POST['messages'][] = __("This failed for a reason or another.", 'duckjoy_group');	

			if (!empty($_POST['messages'])){
				// if the post invalid, return back to form with default values
				$_POST['next_page'] = 'group_passenger_info';
				return;
			} else {
				//if the form is all valid 
				//store in session variables from the booking AND from passenger info.
				if (empty($_POST['couples'])) $_POST['couples'] = 0;
				$_SESSION['order']['passenger_details'] = $_POST;
				$_SESSION['order']['booking_details'] = $_SESSION['group_form_vars'];
				$_SESSION['order']['booking_details']['num_rooms'] = $total_rooms;
				$_SESSION['order']['booking_details']['total_people'] = $total_people;
				$_SESSION['order']['booking_details']['single_rooms'] = $single_rooms;
				$_SESSION['order']['booking_details']['double_rooms'] = $double_rooms;
				$_SESSION['order']['booking_details']['num_adults'] = $num_adults;
				$_SESSION['order']['booking_details']['num_children'] = $num_children;
				$_SESSION['order']['booking_details']['num_students'] = $num_students;
				
				//go to orders confirm
				wp_redirect('/orders/confirm');
			}
			
		}

		
	}


	/**
	*
	* This is for room booking
	*
	**/
	public function process_order_info($content){
		global $wpdb, $post;

		if ( !is_single() ){
			return false;  
		}

		//these are not the droids you are looking for
		if (empty($_POST) && !isset($_POST['orderbutton']) || !isset($_POST['group_detail']) ){
			return false;
		}

		// verify the code
		if (!isset($_REQUEST['nonce']) || !wp_verify_nonce( $_REQUEST['nonce'], 'orderinfo_'.$post->ID )){
			return false;
		}

		
		// setting the variables we are going to work with later
		$traveldates 		= isset($_POST['traveldates']) ? intval($_POST['traveldates']) : 0;
		$doubles_number 	= isset($_POST['doubles_number']) ? intval($_POST['doubles_number']) : 0; 
		$singles_number 	= isset($_POST['singles_number']) ? intval($_POST['singles_number']) : 0; 
		$doubles_adult 		= isset($_POST['doubles_adult']) ? intval($_POST['doubles_adult']) : 0; 
		$doubles_children 	= isset($_POST['doubles_children']) ? intval($_POST['doubles_children']) : 0;
		$doubles_student 	= isset($_POST['doubles_student']) ? intval($_POST['doubles_student']) : 0;
		$singles_adult 		= isset($_POST['singles_adult']) ? intval($_POST['singles_adult']) : 0;
		$singles_children 	= isset($_POST['singles_children']) ? intval($_POST['singles_children']) : 0;
		$singles_student 	= isset($_POST['singles_student']) ? intval($_POST['singles_student']) : 0;
		$orderbutton 		= isset($_POST['orderbutton']) ? sanitize_text_field($_POST['orderbutton']) : 0;

		$route_name 		= isset($_POST['route_name']) ? sanitize_text_field($_POST['route_name']) : 0;
		$offer_code 		= isset($_POST['offer_code']) ? sanitize_text_field($_POST['offer_code']) : 0;
		$post_id 			= isset($_POST['post_id']) ? sanitize_text_field($_POST['post_id']) : 0;
		$stars 				= isset($_POST['stars']) ? sanitize_text_field($_POST['stars']) : 0;
		$availability_code	= isset($_POST['traveldates']) ? sanitize_text_field($_POST['traveldates']) : 0;
		$departure_date = $this->model->get_departure_date_for_availability_code($availability_code);
		$return_date = $this->model->get_return_date_for_availability_code($availability_code);


		$all_meta = get_post_custom(get_the_ID()); 
		$all_tax = get_terms( array('company'));
		foreach($all_tax as $tax){
			$taxonomies[$tax->taxonomy] = $tax;
		}
		
		$availability = $wpdb->get_results("select * from $this->availability_table where offer_id = ".$post->ID);
		$pickup_locations = $wpdb->get_results("select * from $this->pickup_locations_table where offer_id = ".$post->ID);
		$prices = unserialize(unserialize($all_meta['prices'][0])); 

		$cost_doubles_adult = ($prices['double'] * $doubles_adult);
		$cost_doubles_student = ($prices['double_student'] * $doubles_student);
		$cost_doubles_children = ($prices['children'] * $doubles_children);

		$cost_singles_adult = ($prices['single'] * $singles_adult);
		$cost_singles_student = ($prices['single_student'] * $singles_student);
		$cost_singles_children = ($prices['children'] * $singles_children);


		$total_price = $cost_doubles_adult 
			+ $cost_singles_student 
			+ $cost_singles_children 
			+ $cost_singles_adult 
			+ $cost_singles_student
			+ $cost_singles_children;
		$total_rooms = $doubles_number + $singles_number;

		
		//rules & validation
		$_POST['order_info'] = false;
		//Must have at least one room selected
		if (empty($doubles_number) && empty($singles_number)){
			$_POST['messages'][] = __("No room selected. Please select at least one room.", 'duckjoy_group');
		}

		if (empty($doubles_adult) && empty($doubles_student) && empty($singles_adult) && empty($single_student)){
			if (!empty($doubles_children) || !empty($singles_children)){
				$_POST['messages'][] = __("Children can not be without at least one adult.", 'duckjoy_group');	
			}
			$_POST['messages'][] = __("At least one adult or student needs to be selected.", 'duckjoy_group');
		}

		//rule for cant have more people than twice the rooms
		$total_doubles_people = $doubles_adult + $doubles_student;
		if ($total_doubles_people > (2 * $doubles_number)){
			$_POST['messages'][] = __("Too many people for the number of double rooms. Each double room can only have 1 or 2 people.", 'duckjoy_group');	
		}

		//we cant have more people than singles rooms
		$total_singles_people = $singles_adult + $singles_student;
		if ($total_singles_people > $singles_number){
			$_POST['messages'][] = __("Too many people for the number of single rooms. Each single room can only have 1 person.", 'duckjoy_group');	
		}

		// rules for too many rooms for less people
		if ($total_singles_people < $singles_number){
			$_POST['messages'][] = __("You have selected $singles_number single rooms for just $total_singles_people people. ", 'duckjoy_group');	
		}

		//cant have more double rooms than the double of the people. This enables the 2 people for 1 double room
		// number of people can only be double the room OR double -1
		$test_1 = true;
		$test_2 = true;
		if (  $total_doubles_people != $doubles_number*2 ){
			$test_1 = false;
		}

		if ($total_doubles_people != $doubles_number*2-1){
			$test_2 = false;
		}

		//if there are more people than the double of rooms, there is another rule above that takes care of that
		if ($test_1 == false && $test_2 == false && $total_doubles_people < $doubles_number*2){
			$_POST['messages'][] = __("You have selected $doubles_number double rooms for just $total_doubles_people people. ", 'duckjoy_group');	
		}

		//if there is an idication to go back
		if (!empty($_POST['messages'])){
			$_POST['next_page'] = 'group_detail';	
			return false;
		} else {
			//Going to passenger info page
			if (isset($_SESSION['group_form_vars'])) unset($_SESSION['form_vars']);

			$data = array(
				//number of rooms
				'doubles_number'=>$doubles_number,
				'singles_number'=>$singles_number,
				//number of people
				'doubles_adult'=>$doubles_adult,
				'doubles_student'=>$doubles_student,
				'doubles_children'=>$doubles_children,
				'singles_adult'=>$singles_adult,
				'singles_student'=>$singles_student,
				'singles_children'=>$singles_children,
				//totals
				'total_price'=>$total_price,
				'total_rooms'=>$total_rooms,
			);

			foreach ($data as $k=>$v){
				$_SESSION['group_form_vars'][$k] = $v;	
			}
			
			$_SESSION['group_form_vars']['total_price'] 	= $total_price;
			$_SESSION['group_form_vars']['total_rooms'] 	= $total_rooms;
			$_SESSION['group_form_vars']['locations'] 		= $pickup_locations;
			$_SESSION['group_form_vars']['route_name'] 		= $route_name;
			$_SESSION['group_form_vars']['post_id'] 		= $post_id;
			$_SESSION['group_form_vars']['offer_code'] 		= $offer_code;
			$_SESSION['group_form_vars']['stars']		 	= $stars;
			$_SESSION['group_form_vars']['return_date']		= $return_date;
			$_SESSION['group_form_vars']['departure_date']	= $departure_date;

			
			//Proceed with order
			$_POST['next_page'] = 'group_passenger_info';	
			return;
		}

		
	
	/*==================================
	=  Post Type definition            =
	==================================*/




	}

	public function register_post_type(){
		$args = array(
			'labels' => array(
				'name'              => 'Group Deal',
				'singular_name'     => 'Group Deal',
				'menu_name'         => 'Group Deals',
				'all_items'         => 'Group Deals',
				'add_new'			=> 'Add New',
				'add_new_item'		=> 'Add New Group Deal',
				'edit_item'			=> 'Edit Group Deal',
				'new_item'			=> 'New Group Deal',
				'view_item'			=> 'View Group Deal',
				'search_items'		=> 'Search Group Deal',
				'not_found'			=> 'No group deals found',
				'not_found_in_trash'=> 'No group deals in trash',
				'parent_item_colon'	=> '',
				),
			'public' => true, 
			'exclude_from_search' => true,
			//'menu_icon' => admin_url() . '/images/wpspin_light.gif',
			'supports' => array(
				'title',
				'thumbnail',
				),
			'menu_position' => '30',
			);

		register_post_type($this->post_type, $args);

	}

	

	/*=================================
	=            Metaboxes            =
	=================================*/
	
	
	/**
	*
	* initial endpoint for all metadata
	* Place - normal, advanced, side
	* priority - high, core, low 
	*
	**/
	public function meta_boxes(){
		$metaboxes = array();

		//priority high
		

		//priority core
		$metaboxes['availability'] = array(
			'slug' => $this->post_type.'_availability',
			'title' => 'Available Dates',
			'callback' => array($this, 'render_availability'),
			'place' => 'normal',
			'priority' => 'core'
			);
		$metaboxes['company'] = array(
			'slug' => $this->post_type.'_company',
			'title' => 'Company',
			'callback' => array($this, 'render_company'),
			'place' => 'side',
			'priority' => 'core',
			);
		$metaboxes['offer_code'] = array(
			'slug' => $this->post_type.'_offer_code',
			'title' => 'Package Code',
			'callback' => array($this, 'render_offer_code'),
			'place' => 'side',
			'priority' => 'core',
			);

		//priority low
		$metaboxes['pickup_locations'] = array(
			'slug' => $this->post_type.'_pickup_locations',
			'title' => 'Pickup Locations and Times',
			'callback' => array($this, 'render_pickup_locations'),
			'place' => 'normal',
			'priority' => 'core'
			);
		$metaboxes['stars'] = array(
			'slug' => $this->post_type.'_stars',
			'title' => 'Stars',
			'callback' => array($this, 'render_stars'),
			'place' => 'side',
			'priority' => 'low'
			);
		$metaboxes['transports'] = array(
			'slug' => $this->post_type.'_transports',
			'title' => 'Transports',
			'callback' => array($this, 'render_transports'),
			'place' => 'side',
			'priority' => 'low'
			);
		$metaboxes['length'] = array(
			'slug' => $this->post_type.'_length',
			'title' => 'Length',
			'callback' => array($this, 'render_length'),
			'place' => 'side',
			'priority' => 'low'
			);
		// $metaboxes['start'] = array(
		// 	'slug' => $this->post_type.'_start_date',
		// 	'title' => 'Start Date',
		// 	'callback' => array($this, 'render_start_date'),
		// 	'place' => 'side',
		// 	'priority' => 'low'
		// 	);
		$metaboxes['end'] = array(
			'slug' => $this->post_type.'_end_date',
			'title' => 'End Date',
			'callback' => array($this, 'render_end_date'),
			'place' => 'side',
			'priority' => 'low'
			);
		$metaboxes['itinerary'] = array(
			'slug' => $this->post_type.'_itinerary',
			'title' => 'Itinerary',
			'callback' => array($this, 'render_itinerary'),
			'place' => 'normal',
			'priority' => 'low'
			);
		$metaboxes['prices'] = array(
			'slug' => $this->post_type.'_prices',
			'title' => 'Prices',
			'callback' => array($this, 'render_prices'),
			'place' => 'normal',
			'priority' => 'low'
			);

		$this->add_metaboxes($metaboxes);
	}

	/*
	* Adds all metaboxes at once
	* Place - normal, advanced, side
	* priority - high, core, low 
	*/
	public function add_metaboxes($metaboxes){
		foreach($metaboxes as $metabox){
			$this->add_meta_box(
				$metabox['slug'], 
				$metabox['title'], 
				$metabox['callback'], 
				$metabox['place'], //normal, advanced, side
				$metabox['priority'] //high, core, low
				);
		}
	}

	
	/*
	* Adds one metabox only
	*/
	public function add_meta_box($slug, $title, $callback, $place = 'side', $priority = 'core') {
		add_meta_box(
			$slug //slug
			,$title //title
			,$callback
			,$this->post_type,
			$place,
			$priority
			);
	}


	/**
	* Custom metaboxes
	**/
	public function remove_metaboxes(){
		remove_meta_box('companydiv', $this->post_type, 'side');
	}



	/*==========  meta renders  ==========*/
	

	public function render_offer_route_name(){
		global $post; 
		$meta_name = 'offer_route_name';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo '<input name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'" size="50">';

	}

	public function render_offer_code(){
		global $post; 
		$meta_name = 'offer_code';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo '<input name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'">';

	}
	

	public function render_transports(){
		global $post; 
		$meta_name = 'transports';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo "Used means of transport (comma separated): ";
		echo '<input name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'">';
	}

	public function render_start_date(){
		global $post; 
		$meta_name = 'start_date';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo "Date offer is available (yyyy-mm-dd hh:mm:ss): ";
		echo '<input class="datetimepicker" name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'">';
	}

	public function render_end_date(){
		global $post; 
		$meta_name = 'end_date';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		echo "Date offer ends (yyyy-mm-dd hh:mm:ss): ";
		echo '<input class="datetimepicker" name="'.$meta_name.'" type="text" value="'.esc_attr($saved_value).'">';
	}


	public function render_availability(){
		global $post, $wpdb; 
		$meta_name = 'availability';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_values = $wpdb->get_results("SELECT * FROM {$this->availability_table} tb1 WHERE offer_id = {$post->ID} ORDER BY departure ASC");
		$previous_dates = "";
		
		if (empty($saved_values)){
			$previous_dates = __("No previous dates entered yet.", 'duckjoy_group');
		} else {
			foreach ($saved_values as $value) {
				$departure = date('Y-m-d',strtotime($value->departure));
				$return = date('Y-m-d',strtotime($value->return));
				$status = (empty($value->status))? '' : 'checked'; 
				$previous_dates .='<tr><td><input type="text" class="datepicker" name="availability_departure['.$value->availability_code.']" value="'.$departure.'"></td>';
				$previous_dates .='<td><input type="text" class="datepicker" name="availability_return['.$value->availability_code.']" value="'.$return.'"></td>';
				$previous_dates .='<td><input type="checkbox" name="status['.$value->availability_code.']" '.$status.' ></td></tr>';
			}
		}
		?>
		<table id="availability_table" style="text-align:center">
			<thead>
				<td>Departure Date</td>
				<td>Return Date</td>
				<td>Active</td>
			</thead>
			<tbody>
				<?php echo $previous_dates; ?>
				<?php $time_now = time(); ?>
				<?php if (empty($saved_values)) : ?>
					<td><input type="text" class="datepicker" name="availability_departure[<?php echo $time_now; ?>]"></td>
					<td><input type="text" class="datepicker" name="availability_return[<?php echo $time_now; ?>]"></td>
					<td><input type="checkbox" name="status[<?php echo $time_now; ?>]" checked='true'></td>
				<?php endif; ?>
			</tbody>
		</table>
		<!-- <div><a href="javascript:void(0)" id="add_availability"> + Add another date</a></div> -->
		<?php
	}

	public function render_pickup_locations(){
		global $post, $wpdb; 
		$meta_name = 'pickup_locations';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_values = $wpdb->get_results("SELECT * FROM $this->pickup_locations_table tb1 WHERE offer_id = {$post->ID}");
		$previous_dates = "";
		
		if (empty($saved_values)){
			$previous_dates = __("No previous locations entered yet.");
		} else {
			foreach ($saved_values as $value) {
				$time = date('H:i',strtotime($value->time));
				$previous_dates .='<tr><td><input type="text" size="50" name="pickup_location['.$value->pickup_code.']" value="'.$value->location.'"></td>';
				$previous_dates .='<td><input type="text" class="timepicker" name="pickup_time['.$value->pickup_code.']" value="'.$time.'"></td></tr>';
			}
		}
		?>
		<table id="pickup_table" style="text-align:center">
			<thead>
				<td>Location</td>
				<td>Time</td>
			</thead>
			<tbody>
				<?php echo $previous_dates; ?>
				<?php $time_now = time(); ?>
				<td><input type="text" size="50" name="pickup_location[<?php echo $time_now; ?>]"></td>
				<td><input type="text" class="timepicker" name="pickup_time[<?php echo $time_now; ?>]"></td>
			</tbody>
		</table>
		<div><a href="javascript:void(0)" id="add_location"> + Add another location & time</a></div>
		<?php
	}


	public function render_prices(){
		global $post; 
		$meta_name = 'prices';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_values = unserialize(get_post_meta( $post->ID, $meta_name, true));
		
		echo "<p>Please insert prices without the currency, example - 399</p>";
		echo "<label>Price for Single Room: ";
		echo '<input name="'.$meta_name.'[single]" type="text" value="'.esc_attr($saved_values['single']).'"></label><br/>';
		echo "<label>Price for Double Room: ";
		echo '<input name="'.$meta_name.'[double]" type="text" value="'.esc_attr($saved_values['double']).'"></label><br/>';
		echo "<label>Price for Single Room Student: ";
		echo '<input name="'.$meta_name.'[single_student]" type="text" value="'.esc_attr($saved_values['single_student']).'"></label><br/>';
		echo "<label>Price for Double Room Student: ";
		echo '<input name="'.$meta_name.'[double_student]" type="text" value="'.esc_attr($saved_values['double_student']).'"></label><br/>';
		echo "<label>Price for Children: ";
		echo '<input name="'.$meta_name.'[children]" type="text" value="'.esc_attr($saved_values['children']).'"></label><br/>';
		echo "<label>Price for Pick Up: ";
		echo '<input name="'.$meta_name.'[pickup]" type="text" value="'.esc_attr($saved_values['pickup']).'"></label><br/>';
	}

	public function render_info_boxes(){
		global $post; 
		$meta_name = 'info_boxes';
		$meta_title = ucwords(str_replace("_", " ", $meta_name));
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_values = json_decode(get_post_meta( $post->ID, $meta_name, true));
		$message = "";
		$fare_description_str = $message;
		$day_by_day_str = $message;
		$visa_str = $message;
		$precautions_str = $message;
		if (is_object($saved_values)){
			$fare_description_str 	= esc_attr($saved_values->fare_description);
			$day_by_day_str 		= esc_attr($saved_values->day_by_day);
			$visa_str 				= esc_attr($saved_values->visa);
			$precautions_str 		= esc_attr($saved_values->precautions);
		}

		echo "<h3>Fare Description</h3>";
		$settings = array('textarea_name'=>$meta_name.'[fare_description]', 'teeny'=>false,'media_buttons' => false,'editor_height' => 50);
		wp_editor($fare_description_str, 'fare_description', $settings);

		echo "<br><h3>Day-by-Day</h3>";
		$settings['textarea_name'] = $meta_name.'[day_by_day]';
		wp_editor($day_by_day_str, 'day_by_day', $settings);

		echo "<br><h3>Visa Information </h3>";
		$settings['textarea_name'] = $meta_name.'[visa]';
		wp_editor($visa_str, 'visa', $settings);

		echo "<br><h3>Precautions </h3>";
		$settings['textarea_name'] = $meta_name.'[precautions]';
		wp_editor($precautions_str, 'precautions', $settings);
	}


	/**
	* Company render - Custom Taxonomy metabox
	**/
	public function render_company(){
		global $post; 
		$tax_name = 'company';
		$tax_title = ucwords($tax_name);
		$tax_slug = $this->post_type.'_'.$tax_name; 
		$tax = get_taxonomy($tax_name);
		//The name of the form
		$name = 'tax_input[' . $tax_name . ']';
		//Get all the terms for this taxonomy
		$terms = get_terms($tax_name,array('hide_empty' => 0));
		$postterms = get_the_terms( $post->ID,$tax_name );
		$current_term = ($postterms ? array_pop($postterms) : false);
		?>
		<!-- Display taxonomy terms -->
		<div id="<?php echo $tax_name; ?>-all" class="tabs-panel">
			<select name="<?php echo $name; ?>">
				<?php foreach($terms as $term){
					$selected = ($term->term_id == $current_term->term_id)? true : false;
					if ($selected){
						echo '<option value="'.$term->term_id.'" selected>'.$term->name.'</option>';	
					} else {
						echo '<option value="'.$term->term_id.'">'.$term->name.'</option>';
					}
				}?>
			</select>
		</div>
		<?php
	}


	
	public function render_itinerary(){
		global $post; 
		$meta_name = 'itinerary';
		$meta_title = ucwords($meta_name);
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		?>
		<p>Please insert the names of all the cities this journey separated by comma,<br/> 
			e.g., London, Frankfurt, Brussels (Belgium), Porto (portugal)<br/>
			The first and the last cities will be the origin and the destination for the tour.
		</p>
		<textarea name="<?php echo $meta_name; ?>" id="" cols="100" rows="5"><?php echo $saved_value; ?></textarea>
		<?php
	}


	public function render_stars(){
		global $post; 
		$meta_name = 'stars';
		$meta_title = 'Stars';
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		$stars = array(1,2,3,4,5); 
		?>
		<select name="<?php echo $meta_name; ?>" >
			<?php
			foreach($stars as $star){
				$selected = ($star == $saved_value)? true : false;
				if ($selected){
					echo '<option value="'.$star.'" selected>'.$star.'</option>';	
				} else {
					echo '<option value="'.$star.'">'.$star.'</option>';
				}
			}
			?>
		</select>

		<?php
	}


	public function render_length(){
		global $post; 
		$meta_name = 'length';
		$meta_title = ucwords($meta_name);
		$meta_slug = $this->post_type.'_'.$meta_name; 
		$saved_value = get_post_meta( $post->ID, $meta_name, true );
		$days = range(1,30); 
		?>
		<select name="<?php echo $meta_name; ?>" >
			<?php
			foreach($days as $day){
				$selected = ($day == $saved_value)? true : false;
				if ($selected){
					echo '<option value="'.$day.'" selected>'.$day.' days</option>';	
				} else {
					echo '<option value="'.$day.'">'.$day.' days</option>';
				}
			}
			?>
		</select>
		<?php
	}

	/*===================================
	=            Plugin CRUD            =
	===================================*/

	public function save( $post_id ) {
		global $wpdb;

		// Check if our nonce is set.
		// if ( ! isset( $_POST[$this->post_type.'_company_name'] ) )
		// 	return $post_id;
		// $nonce = $_POST['duck_offers_nonce'];

		// // Verify that the nonce is valid.
		// if ( ! wp_verify_nonce( $nonce, 'duck_offers_nonce_field' ) )
		// 	return $post_id;

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( ! current_user_can( 'edit_page', $post_id ) )
			return $post_id;

		// save metas
		if (empty($_POST['offer_route_name'])) $_POST['offer_route_name'] = null;
		$offer_route_name = sanitize_text_field( $_POST['offer_route_name'] );
		$this->save_meta( $post_id, 'offer_route_name', $offer_route_name );

		if (empty($_POST['offer_code'])) $_POST['offer_code'] = '--';
		$offer_code = sanitize_text_field( $_POST['offer_code'] );
		$this->save_meta( $post_id, 'offer_code', $offer_code );

		if (empty($_POST['stars'])) $_POST['stars'] = 3;
		$stars = sanitize_text_field( $_POST['stars'] );
		$this->save_meta( $post_id, 'stars', $stars );


		if (empty($_POST['transports'])) $_POST['transports'] = null;
		$transports	 = $this->sanitize_text_field( $_POST['transports'] );
		$this->save_meta( $post_id, 'transports', $transports );

		if (empty($_POST['length'])) $_POST['length'] = null;
		$length	= sanitize_text_field( $_POST['length'] );
		$this->save_meta( $post_id, 'length', $length );
		
		
		if (empty($_POST['start_date'])) $_POST['start_date'] = null;
		$start_date	= $this->sanitize_text_field( $_POST['start_date'] );
		$this->save_meta($post_id, 'start_date', $start_date);

		if (empty($_POST['end_date'])) $_POST['end_date'] = null;
		$end_date = $this->sanitize_text_field( $_POST['end_date'] );
		$this->save_meta( $post_id, 'end_date', $end_date );

		//itinerary saves to 2 places. To the meta and to its own table.
		//the metabox is the one for display
		//its own table serves only so to index all cities later
		$itinerary = sanitize_text_field( @$_POST['itinerary'] );
		$this->save_meta($post_id, 'itinerary', $itinerary);
		$this->model->save_itinerary($itinerary);

		//prices is an array of values
		$this->save_prices($post_id);


		//info boxes
		if (!empty($_POST['info_boxes'])){
			foreach ($_POST['info_boxes'] as &$value) {
				$value = sanitize_text_field($value);
			}
			$info_boxes = json_encode(($_POST['info_boxes']));
			update_post_meta( $post_id, 'info_boxes', $info_boxes );			
		}

		$this->save_availability_boxes($post_id);

		$this->save_pickup_boxes($post_id);

	}

	public function sanitize_text_field($text_field){
		return sanitize_text_field($text_field);
	}

	public function save_meta($post_id, $key, $post_field){
		return update_post_meta( $post_id, $key, $post_field );
	}

	public function save_prices($post_id){
		if (!empty($_POST['prices'])){
			foreach($_POST['prices'] as &$price){
				//TODO: get only the numbers in here - This didnt work for some reason... 
				// preg_match('/([0-9]*)/', $price, $match);
				//convert this to a number no matter what it is.
				$price = ($price)? $price : (int) 0;
			}
			$prices = serialize($_POST['prices']);
			$this->save_meta( $post_id, 'prices', $prices );
		}
	}

	public function save_availability_boxes($post_id){
		global $wpdb;
		//If I have ANY value on all the departure dates
		// so initial page will not go into this code
		if (!empty($_POST['availability_departure'])){
			foreach ($_POST['availability_departure'] as $key => $value) {
				$key = strip_tags($key); 
				$departure = $value;
				$return = $_POST['availability_return'][$key];
				$status = (!empty($_POST['status'][$key])) ? true : false ;
				
				//if there is a missing return date, ignore this entry
				if (empty($departure)){
					//we need to delete
					$wpdb->delete($this->availability_table, array('availability_code'=>$key));
					continue;
				} 
				
				$departure = date('Y-m-d 00:00:00', strtotime($departure));
				$return = date('Y-m-d 00:00:00', strtotime($return));
				$data = array(
					'availability_code'	=>	$key,
					'offer_id'			=>	$post_id,
					'departure'			=>	$departure,
					'return'			=>	$return,
					'status'			=>	$status
					);
				
				$wpdb->replace( $this->availability_table, $data );
			}
		} 
	}

	public function save_pickup_boxes($post_id){
		global $wpdb;
		//If I have ANY value on all the departure dates
		// so initial page will not go into this code
		if (!empty($_POST['pickup_location'])){
			foreach ($_POST['pickup_location'] as $key => $value) {
				$key = strip_tags($key); 
				$location = sanitize_text_field($value);
				$time = date('H:i:s', strtotime($_POST['pickup_time'][$key]) );

				//if there is no location just delete or skip
				if (empty($location)){
					//we need to delete
					$wpdb->delete($this->pickup_locations_table, array('pickup_code'=>$key));
					continue;
				} 

				$data = array(
					'pickup_code'		=>	$key,
					'offer_id'			=>	$post_id,
					'location'			=>	$location,
					'time'				=>	$time
					);
				$format = array( 
					'%d',
					'%d', 
					'%s',
					'%s',
					); 

				$wpdb->replace( $this->pickup_locations_table, $data, $format );
			}
		} 
	}


} //end of class





