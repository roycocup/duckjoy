��          �      |      �     �  3     /   A     q  )   �     �  "   �  2   �  1   %  .   W     �  &   �  '   �     �  .   
  )   9  )   c  ,   �  &   �  ]   �  X   ?  �  �     \  !   {  -   �     �  !   �          "  !   >  9   `     �     �     �     �     �  $   	     @	     V	     r	     �	  /   �	  (   �	     
                                                	                                            A phone number is required. At least one adult or student needs to be selected. Children can not be without at least one adult. Emails do not match. Must agree with the terms and conditions. No previous dates entered yet. No previous locations entered yet. No room selected. Please select at least one room. Other passengers need a name and a date of birth. Password should be at least 6 characters long. Passwords do not match. Please fill your country of residence. Please fill your first line of address. Please select a visa option. Primary passenger does not have a valid email. Primary passenger name must not be empty. Primary passenger needs to confirm email. Primary passenger surname must not be empty. Primary phone number must be a number. Too many people for the number of double rooms. Each double room can only have 1 or 2 people. Too many people for the number of single rooms. Each single room can only have 1 person. Project-Id-Version: 
POT-Creation-Date: 2014-01-09 12:41-0000
PO-Revision-Date: 2014-01-09 15:04-0000
Last-Translator: Rodrigo Dias <Rodrigo@rodderscode.co.uk>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.3
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
Language: zh_CN
X-Poedit-SearchPath-0: /Users/rodrigodias/sites/duckjoy/www/wp-content/plugins/duckjoy_group
 请输入您的电话号码。 请您至少选择一名成人。 儿童应至少由一名成人陪同参团。 邮箱地址不一致。 您需要同意本站的条款。 之前没有输入日期。 之前没有输入地点。 请您至少选择一间客房。 请您完整填写每个旅客的姓名和出生日期。 密码至少应为6位数。 密码不一致。 请输入您的居住地。 请输入您的地址。 请您选择签证选项。 请您输入正确的邮箱地址。 请输入您的名。 请您确认邮箱地址。 请输入您的姓。	 电话号码必须为数字。 每间双人房只能入住1名或2名旅客。 每间单人房只能入住1名旅客。 