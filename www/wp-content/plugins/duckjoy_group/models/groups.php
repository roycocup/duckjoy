<?php 

require_once(dirname(dirname(dirname(__FILE__)))."/duckjoy_orders/models/db.php");

class Groups_model extends db{

	public $table = '';
	public $orders_model;

	public function __construct(){
		parent::__construct();
		$this->orders_model = new Orders_model();
	}


	/**
	*
	* Get a post by id
	*
	**/
	public function get_post_by_id($post_id){
		$post = get_post($post_id, 'object');
		return $post; 
	}


	public function add_data_to_post($post, $terms = array('company') ){
		$taxonomies = self::get_taxonomies_for_post($post, $terms);
		$post->meta = self::get_meta_for_post($post);
		$post->taxonomies = $taxonomies;
		$post->availability = $this->get_availability_for_post($post);
		$post->prices = unserialize(unserialize($post->meta['prices'][0]));
		
		return $post;
	}

	public static function get_meta_for_post( $post ){
		$all_meta = get_post_custom($post->ID);
		return $all_meta;
	}


	public static function get_taxonomies_for_post($post, $terms){
		$all_tax = get_terms( $terms );
		foreach($all_tax as $tax){
			$taxonomies[$tax->taxonomy] = $tax;
		}

		return $taxonomies;
	}

	public function get_availability_for_post($post, $after_today = false){
		$sql = "SELECT * FROM $this->group_availability_table WHERE offer_id = ".$post->ID;
		if ($after_today){
			$sql .= " AND departure > now()"; 
		}
		$results = $this->wpdb->get_results($sql);
		return $results;
	}


	public function get_availability_for_post_id($post_id, $after_today = false){
		$sql = "SELECT * FROM $this->group_availability_table WHERE offer_id = ".$post_id;
		if ($after_today){
			$sql .= " AND departure > now()"; 
		}
		$results = $this->wpdb->get_results($sql);
		return $results;
	}


	public function get_cities( $extra_args = array() ){

		$sql = "SELECT * FROM $this->group_cities_table t1";

		if(!empty($extra_args)){
			foreach ($extra_args as $key => $value) {
				$sql .=  " ".$value;
			}
		}

		$results = $this->wpdb->get_results($sql);

		return $results;
	}


	public function get_city_on_itinerary_by_name($city_name){
		$sql = "SELECT * FROM $this->group_itinerary_table WHERE place_name = '{$city_name}'";
		$results = $this->wpdb->get_results($sql);

		return $results;
	}


	public function save_itinerary($itinerary){
		global $post, $wpdb;
		
		mb_regex_encoding('UTF-8');
      	mb_internal_encoding("UTF-8"); 
		$itinerary = mb_split( '[，,]+' , $itinerary );
		//$itinerary = explode(',', $itinerary);

		
		$origin = strtolower(trim($itinerary[0])); 
		$destination = strtolower(trim($itinerary[count($itinerary)-1]));
		
		
		//dont do anything unless this is a full post with an ID. 
		if (isset($post->ID)) {

			$previous_itinerary = $wpdb->query("DELETE FROM $this->group_itinerary_table WHERE post_id = $post->ID");

			foreach ($itinerary as $place_name) {
				$place_name = strtolower(trim($place_name));
				if (empty($place_name)) continue;

				$data = array(
					'post_id' => $post->ID,
					'place_name'=>$place_name
				);
				$data['origin'] = ($place_name == $origin) ? 1 : 0;
				$data['destination'] = ($place_name == $destination) ? 1 : 0; 

				$wpdb->replace( $this->group_itinerary_table, $data );	
			}
			
			$this->reindex_countries(); 
		}
		return true;
		
	}


	private function reindex_countries(){
		global $wpdb;

		//get the list from itenerary
		$sql = "SELECT DISTINCT(place_name) FROM $this->group_itinerary_table";
		$cities = $wpdb->get_results($sql);

		$countries_found = $this->find_country_in_cities($cities);
		asort($countries_found);
		foreach ($countries_found as $country) {
			//check that exists there first
			$country_exists = $this->find_country_in_db($country);
			//if it could not find anything
			if (!$country_exists){
				$wpdb->insert($this->group_countries_table, array('name'=>$country) );
			}
		}
		$this->reindex_cities();
	}


	private function reindex_cities(){
		global $wpdb;

		$sql = "SELECT DISTINCT(place_name) FROM $this->group_itinerary_table";
		$cities = $wpdb->get_results($sql);

		$sql = "TRUNCATE TABLE $this->group_cities_table";
		$wpdb->query($sql);
		
		//many hours of work to sync cities and countries, turns out chinese caracters and regex dont get on well
		foreach($cities as $city){
			$country = $this->find_country_in_city($city);
			$country = $this->find_country_in_db($country);
			$data = array(
				'name'=>$city->place_name
			);
			if (!empty($country)){
				$data['country_id'] = $country[0]->country_id;
			} 
			
			//check that the city is an origin for ANY itineary
			$city_in_itinerary = $this->get_city_on_itinerary_by_name($city->place_name);
			foreach ($city_in_itinerary as $value) {
				//assign the data to the query to be updated
				if ($value->origin) $data['origin'] = 1;
				if ($value->destination) $data['destination'] = 1;
			}
			//update the table
			$wpdb->insert($this->group_cities_table, $data );
		}
		
	}


	private function find_country_in_cities($cities){
		foreach($cities as $city){
			$country = $this->find_country_in_city($city);
			if (!empty($country)){
				$result[] = $country;
			}
		}
		return $result;
	}

	private function find_country_in_city($city){
		//$pattern = '/.*?\(([a-z]*)/u';
		$pattern = '/[\x{3400}-\x{4DB5}]*';
		//$pattern .= '[\x{3200}-\x{9FFF}]*';
		$pattern .= '\(([a-z]*)/u';
			preg_match($pattern, $city->place_name, $match);
			if (!empty($match[1])){
				return $match[1];
			}
	}


	private function find_country_in_db($country){
		global $wpdb;
		$country = $wpdb->get_results("SELECT * FROM $this->group_countries_table WHERE name = '{$country}'");
		if (empty($country[0]->name)){
			$country = false;
		}
		return $country;
	}




	/**
	*
	* get all the posts there are for this city
	*
	**/
	public function get_posts_by_city($post_type, $city = ''){

		$selected_posts = array();
		$args = array(
			'posts_per_page'   => -1,
			'post_type' => array($post_type),
			'orderby'=>'date',
			'order'=>'DESC',
			'post_status'=>'publish',
		);
		$posts = get_posts($args);


		//check for city in the itinerary
		foreach($posts as $post){
			$all_meta = self::get_meta_for_post($post);
			if (stripos($all_meta['itinerary'][0], $city) === false) continue;
			$selected_posts[] = $post;
		}
		return $selected_posts;
	}



	public function get_posts_with_origin_and_destination($post_type, $origin, $destination){
		//get all the posts with the origin
		$origin_posts_ids = $this->get_posts_ids_for_origin($origin);
		//search for the destination inside each one of these posts
		$posts = $this->get_city_present_in_post($origin_posts_ids, $destination);

		return $posts;
	}

	/*
	* Origin posts is an array of objects with post->post_id
	* Needle city is just a name
	*/
	public function get_city_present_in_post($origin_posts, $needle_city){
		$selected_posts = array();
		foreach ($origin_posts as $post){
			$post = $this->get_post_by_id($post->post_id);
			$all_meta = self::get_meta_for_post($post);
			if (stripos($all_meta['itinerary'][0], $needle_city) === false) continue;
			$selected_posts[] = $post;
		}
		
		return $selected_posts;
	}


	/**
	*
	* Get the post ids for the posts that have this city as their origin
	* @returns array of objects
	* 
	**/
	public function get_posts_ids_for_origin($city_name){		
		$sql = "SELECT post_id from $this->group_itinerary_table where place_name = '{$city_name}' and origin = 1";
		$results = $this->wpdb->get_results($sql);
		return $results;
	}

	/**
	*
	* Get the posts for a city that is the origin of the itinerary of that post.
	* Same as get_posts_ids_for_origin but then gets the actual posts for each one and returns them
	* @returns array of POST object
	*
	**/
	public function get_posts_ids_for_origin_city($city_name){
		$post_ids = $this->get_posts_ids_for_origin($city_name);
		foreach ($post_ids as $key => $value) {
			$posts[] = $this->get_post_by_id($value->post_id);
		}

		return $posts;
	}


	/**
	*
	* AJAX is going to call
	* this at multiple times so better to have empty results a lot
	*
	**/
	
	public function get_available_destinations_for_origin($origin_value){
		$cities = array();
		if (empty($origin_value)) return $cities;

		//get the post ids where the origin is on
		$sql = "SELECT post_id FROM {$this->group_itinerary_table} WHERE place_name = '{$origin_value}' AND origin = 1";
		$post_ids = $this->wpdb->get_results($sql);


		//get all the cities in between 
		if (empty($post_ids)) return $cities;
		$str_post_ids = '';
		$i = 1; 
		$num_posts = count($post_ids);
		foreach ($post_ids as $key => $post) {
			$str_post_ids .= $post->post_id;
			if ($i < $num_posts) $str_post_ids .= ', ';
			$i++;
		}

			$sql = "SELECT place_name FROM {$this->group_itinerary_table} WHERE post_id in ({$str_post_ids}) AND origin = 0 ";
			$cities[] = $this->wpdb->get_results($sql);	
			return $cities;
	}


	public function get_departure_date_for_availability_code($availability_code){
		$sql = "SELECT departure FROM $this->group_availability_table WHERE availability_code = {$availability_code} limit 1";
		$result = $this->wpdb->get_var($sql);
		return $result; 
	}


	public function get_return_date_for_availability_code($availability_code){
		$sql = "SELECT `return` FROM $this->group_availability_table WHERE availability_code = {$availability_code} limit 1";
		$result = $this->wpdb->get_var($sql);
		return $result; 
	}

	public function get_availability_for_availability_code($availability_code){
		$sql = "SELECT status FROM $this->group_availability_table WHERE availability_code = {$availability_code} limit 1";
		$result = $this->wpdb->get_var($sql);
		return $result; 
	}



	/**
	*
	* Has the minimum number of people been attained
	*
	**/
	public function is_minimum_attained($post_id){
		$minimum_target = $this->get_minimum($post_id); 
		//$num_people_bought = $this->orders_model->getBoughtOrdersForPostId($post_id);
		$num_people_bought = $this->orders_model->getBoughtPartyMembersForPostId($post_id);
		
		//if the minimum target is still higher than the num of people who bought... 
		if ($minimum_target > $num_people_bought){
			return false;
		}

		return true;
	}

	/**
	*
	* Get the minimum number of people for this group deal
	*
	**/
	public function get_minimum($post_id){
		//get the minimum for this post
		$sql = "SELECT pm.meta_value
				FROM wp_posts p
				JOIN wp_postmeta pm ON pm.post_id = p.ID
				WHERE p.post_type = 'groups' AND pm.meta_key = 'minimum_number_of_buyers' 
				AND p.ID = {$post_id}
				";
		$minimum_num = $this->wpdb->get_var($sql);
		return $minimum_num;
	} 


}


	



?>