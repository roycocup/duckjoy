<?php


class Itineraty_model{

	public $table = 'wp_duck_group_itinerary';
	public $wpdb; 

	public function __construct(){
		global $wpdb;
		$this->wpdb = $wpdb;
	}


	public function get_itinerary_for_post_id($post_id){
		$sql = "SELECT * FROM {$this->table} WHERE post_id = $post_id";
		return $this->wpdb->get_results($sql);
	}


	public function get_origin_and_destination_for_post_id($post_id){
		$results = $this->get_itinerary_for_post_id($post_id);
		$origin = '';
		$destination = '';
		foreach ($results as $result){
			if ($result->origin) $origin = $result;
			if ($result->destination) $destination = $result;
		}
		return array ('origin'=>$origin, 'destination'=>$destination);
	}

}


?>